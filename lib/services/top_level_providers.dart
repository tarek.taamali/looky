import 'package:applooki/favoris/favoris_view_model.dart';
import 'package:applooki/home/home_view_model.dart';
import 'package:applooki/enter_page/enter_view_model.dart';
import 'package:applooki/panier/panier_view_model.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final enterPageViewModel = ChangeNotifierProvider<EnterViewModel>(
  (ref) => EnterViewModel(),
);

final menuViewModel = ChangeNotifierProvider.autoDispose<MenuViewModel>(
  (ref) => MenuViewModel(),
);
final panierViewModel = ChangeNotifierProvider<PanierViewModel>(
  (ref) => PanierViewModel(),
);

final favorisViewModel = ChangeNotifierProvider<FavorisViewModel>(
  (ref) => FavorisViewModel(),
);
