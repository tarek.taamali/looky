import 'package:flutter/material.dart';
import 'colors.dart';
import 'fonts.dart';

class CustomPasswordField extends StatelessWidget {
  final FormFieldValidator<String> validator;
  final String labelText;
  final TextEditingController controller;
  final TextInputType keyboardType;
  final bool obscureText;
  final Widget suffixIcon;
  final double width;
  final Widget prefixIcon;
  final String? initialValue;
  final FocusNode? focusNode;
  final Function(String)? onFieldSubmitted;

  const CustomPasswordField({
    Key? key,
    required this.labelText,
    required this.validator,
    required this.controller,
    this.keyboardType: TextInputType.text,
    this.obscureText = false,
    required this.suffixIcon,
    required this.width,
    required this.prefixIcon,
    this.initialValue,
    this.focusNode,
    this.onFieldSubmitted,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      constraints: BoxConstraints(
        minHeight: 55.0,
      ),
      child: TextFormField(
        focusNode: focusNode,
        initialValue: initialValue,
        keyboardType: keyboardType,
        obscureText: obscureText,
        obscuringCharacter: '*',
        controller: controller,
        cursorColor: primary_blue,
        onFieldSubmitted: onFieldSubmitted,
        decoration: InputDecoration(
          hintText: labelText,
          suffixIcon: suffixIcon,
          prefixIcon: prefixIcon,
          labelStyle: TextStyle(
              color: placeholder_grey,
              fontFamily: fontMedium,
              fontSize: 13.0,
              height: 2.0),
          focusColor: primary_black,
          hoverColor: primary_black,
          floatingLabelBehavior: FloatingLabelBehavior.auto,
          border: new OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(7),
            ),
            borderSide: BorderSide(
              color: primary_blue,
            ),
          ),
          alignLabelWithHint: true,
          filled: true,
          fillColor: primary_white,
          focusedBorder: new OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(10.0),
            ),
            borderSide: BorderSide(
              color: primary_blue,
            ),
          ),
        ),
        validator: validator,
      ),
    );
  }
}
