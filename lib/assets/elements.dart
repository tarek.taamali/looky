import 'dart:ui';

import 'package:applooki/assets/fonts.dart';
import 'package:applooki/assets/images_links.dart';
import 'package:flutter/material.dart';
import 'package:loading_indicator/loading_indicator.dart';

import 'colors.dart';

Widget LoaderWidget(context) {
  return Center(
    child: SizedBox(
      width: MediaQuery.of(context).size.width * 0.15,
      child: LoadingIndicator(
        colors: [primary_blue],
        indicatorType: Indicator.ballPulse,
      ),
    ),
  );
}

BackdropFilter alertWithTwoButton(
  context,
  String title,
  String content,
  String txtYesButton,
  String txtNoButton,
  Function onPressYes,
  Function onPressNo,
) {
  return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
      child: Container(
        alignment: Alignment.center,
        color: background_blue.withOpacity(0.2),
        child: AlertDialog(
          scrollable: true,
          backgroundColor: primary_white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          content: Container(
              constraints: BoxConstraints(minHeight: 200.0),
              width: 0.85 * MediaQuery.of(context).size.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.only(top: 8),
                    child: Text(
                      title,
                      style: TextStyle(
                          fontSize: 18.0,
                          fontFamily: fontBold,
                          color: primary_blue),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      top: 20,
                    ),
                    child: Text(
                      content,
                      style: TextStyle(
                          color: primary_black,
                          fontSize: 13.0,
                          fontFamily: fontRegular),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 50),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height: 38.0,
                          width:
                              0.35 * (MediaQuery.of(context).size.width * 0.85),
                          decoration: BoxDecoration(
                            border: Border.all(
                              width: 1,
                              color: primary_blue,
                            ),
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          child: RaisedButton(
                            elevation: 0,
                            color: primary_white,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            onPressed: () {
                              onPressNo();
                            },
                            child: Text(
                              txtNoButton,
                              style: TextStyle(
                                color: primary_blue,
                                fontFamily: fontSemiBold,
                                fontSize: 13.0,
                              ),
                            ),
                          ),
                        ),
                        Spacer(),
                        Container(
                          padding: EdgeInsets.only(left: 10),
                          height: 38.0,
                          width:
                              0.35 * (MediaQuery.of(context).size.width * 0.85),
                          child: RaisedButton(
                            elevation: 0,
                            color: primary_blue,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                            onPressed: () {
                              onPressYes();
                            },
                            child: Text(
                              txtYesButton,
                              style: TextStyle(
                                color: primary_white,
                                fontFamily: fontSemiBold,
                                fontSize: 13.0,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              )),
        ),
      ));
}

BackdropFilter PopupEchec(
    context, titleEchec, contentEcehec, Function onPress) {
  return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
      child: Container(
        alignment: Alignment.center,
        //padding: EdgeInsets.only(bottom: 230),
        color: background_blue.withOpacity(0.5),
        child: AlertDialog(
          scrollable: true,
          backgroundColor: primary_white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          content: Container(
              constraints: BoxConstraints(minHeight: 200.0),
              //height: 200,
              width: 0.85 * MediaQuery.of(context).size.width,
              //padding: ,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.only(top: 8),
                    child: Column(
                      children: [
                        Text(
                          titleEchec,
                          style: TextStyle(
                              fontSize: textSizeLarge, fontFamily: fontBold),
                        ),
                        Padding(padding: EdgeInsets.only(top: 10)),
                        Image.asset(
                          logo_alert,
                          scale: 1.0,
                          height: 120,
                          color: Colors.redAccent,
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      top: 20,
                    ),
                    child: Text(
                      contentEcehec,
                      style: TextStyle(
                          color: primary_black,
                          fontSize: textSizeLargeMedium,
                          fontFamily: fontMedium),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 50),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height: 50,
                          width:
                              0.35 * (MediaQuery.of(context).size.width * 0.85),
                          //margin: EdgeInsets.only(bottom: 20),
                          child: RaisedButton(
                            elevation: 5.0,
                            color: primary_blue,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            onPressed: () {
                              onPress;
                              Navigator.pop(context);
                              //FadeTransition(opacity: , child: child);
                            },
                            child: Text(
                              "OK",
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: fontBold,
                                fontSize: textSizeMedium,
                                // fontSize: MediaQuery.of(context).size.height / 40,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )),
        ),
      ));
}

BackdropFilter PopupSuccess(
    context, titleSucces, contentSuccess, Function onPress) {
  return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
      child: Container(
        alignment: Alignment.center,
        //padding: EdgeInsets.only(bottom: 230),
        color: primary_blue.withOpacity(0.2),
        child: AlertDialog(
          scrollable: true,
          backgroundColor: primary_white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          content: Container(
              constraints: BoxConstraints(minHeight: 200.0),
              //height: 200,
              width: 0.85 * MediaQuery.of(context).size.width,
              //padding: ,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.only(top: 8),
                    child: Column(
                      children: [
                        Text(
                          titleSucces,
                          style: TextStyle(
                              fontSize: textSizeLarge, fontFamily: fontBold),
                        ),
                        Padding(padding: EdgeInsets.only(top: 10)),
                        Image.asset(
                          logo_succes,
                          scale: 1.0,
                          height: 100,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                      top: 20,
                    ),
                    child: Text(
                      contentSuccess,
                      style: TextStyle(
                          color: primary_black,
                          fontSize: textSizeLargeMedium,
                          fontFamily: fontMedium),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 50),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height: 50,
                          width:
                              0.35 * (MediaQuery.of(context).size.width * 0.85),
                          //margin: EdgeInsets.only(bottom: 20),
                          child: RaisedButton(
                            elevation: 5.0,
                            color: primary_blue,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            onPressed: () {
                              //Navigator.pop(context);
                              onPress();
                              Navigator.pop(context);
                              //FadeTransition(opacity: , child: child);
                            },
                            child: Text(
                              "OK",
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: fontBold,
                                fontSize: textSizeMedium,
                                // fontSize: MediaQuery.of(context).size.height / 40,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )),
        ),
      ));
}

//decoration des bouttons
BoxDecoration boxDecorationBtnCnx(
    {double radius = 10,
    Color color = primary_blue,
    required Color bgColor,
    bool showShadow = false}) {
  return BoxDecoration(
      color: bgColor,
      //gradient: LinearGradient(colors: [bgColor, whiteColor]),
      boxShadow: showShadow
          ? [
              BoxShadow(
                  color: txt_grey,
                  blurRadius: 2,
                  spreadRadius: 2,
                  offset: Offset(0, 1))
            ]
          : [BoxShadow(color: Colors.transparent)],
      border: Border.all(color: Colors.transparent),
      borderRadius: BorderRadius.all(Radius.circular(radius)));
}

BoxDecoration boxDecorationBtnFB(
    {double radius = 10,
    Color color = primary_white,
    Color bgColor = primary_blue,
    bool showShadow = false}) {
  return BoxDecoration(
      color: bgColor,
      //gradient: LinearGradient(colors: [bgColor, whiteColor]),
      boxShadow: showShadow
          ? [
              BoxShadow(
                  color: txt_grey,
                  blurRadius: 5,
                  spreadRadius: 2,
                  offset: Offset(0, 2))
            ]
          : [BoxShadow(color: Colors.transparent)],
      border: Border.all(color: Colors.transparent),
      borderRadius: BorderRadius.all(Radius.circular(radius)));
}

BoxDecoration boxDecorationBtnSignin(
    {double radius = 10,
    Color color = primary_blue,
    Color bgColor = primary_white,
    bool showShadow = false}) {
  return BoxDecoration(
      color: bgColor,
      //gradient: LinearGradient(colors: [bgColor, whiteColor]),
      boxShadow: showShadow
          ? [
              BoxShadow(
                  color: txt_grey,
                  blurRadius: 5,
                  spreadRadius: 2,
                  offset: Offset(0, 2))
            ]
          : [BoxShadow(color: Colors.transparent)],
      border: Border.all(color: color),
      borderRadius: BorderRadius.all(Radius.circular(radius)));
}
