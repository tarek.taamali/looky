import 'package:flutter/material.dart';
import 'colors.dart';

class CustomFormField extends StatelessWidget {
  final bool autofocus;
  final FormFieldValidator<String> validator;
  final String hintText;
  final String? initialValue;
  final TextEditingController controller;
  final TextInputType keyboardType;
  final String? prefixText;
  final double width;
  final Widget? suffixIcon;
  final bool enabled;
  final bool? readOnly;
  final bool obscuretext;
  final Widget? prefixIcon;
  Function(String?)? onChange;
  Function(String?)? onSaved;
  final FocusNode? focusNode;
  Function(String)? onFieldSubmitted;

  CustomFormField({
    Key? key,
    required this.validator,
    required this.controller,
    this.keyboardType: TextInputType.text,
    this.prefixText,
    required this.width,
    this.onChange,
    this.readOnly = false,
    this.obscuretext = false,
    this.suffixIcon,
    this.hintText = "",
    this.onSaved,
    this.prefixIcon,
    this.initialValue,
    this.enabled = true,
    this.autofocus = false,
    this.focusNode,
    this.onFieldSubmitted,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      constraints: BoxConstraints(
        maxHeight: 100.0,
      ),
      child: Container(
        constraints: BoxConstraints(
          minHeight: 60.0,
        ),
        child: TextFormField(
          autofocus: autofocus,
          focusNode: focusNode,
          initialValue: initialValue,
          keyboardType: keyboardType,
          controller: controller,
          cursorColor: primary_black,
          onChanged: onChange,
          enabled: enabled,
          onSaved: onSaved,
          decoration: InputDecoration(
            prefixText: prefixText,
            prefixIcon: prefixIcon,
            suffixIcon: suffixIcon,
            hintText: hintText,
            focusColor: primary_black,
            hoverColor: primary_black,
            border: OutlineInputBorder(
              borderRadius: const BorderRadius.all(
                const Radius.circular(7),
              ),
              borderSide: BorderSide(
                color: Colors.grey.withOpacity(0.5),
              ),
            ),
            filled: true,
            fillColor: primary_white,
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: primary_blue,
              ),
              borderRadius: const BorderRadius.all(
                const Radius.circular(7.0),
              ),
            ),
          ),
          validator: validator,
          onFieldSubmitted: onFieldSubmitted,
        ),
      ),
    );
  }
}
