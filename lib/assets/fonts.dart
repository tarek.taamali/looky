/*Intel fonts*/
const fontBlack = 'Black';
const fontBold = 'Bold';
const fontExtraBold = 'ExtraBold';
const fontExtraLight = 'ExtraLight';
const fontLight = 'Light';
const fontMedium = 'Medium';
const fontRegular = 'Regular';
const fontSemiBold = 'SemiBold';
const fontThin = 'Thin';
const robotoBold = 'RobotoBold';

/* font sizes*/
const textSizeSmall = 12.0;
const textSizeXSMedium = 13.0;
const textSizeSMedium = 14.0;
const textSizeMMedium = 15.0;
const textSizeMedium = 16.0;
const textSizeLargeMedium = 17.0;
const textSizeXLargeMedium = 18.0;
const textSizeSNormal = 19.0;
const textSizeNormal = 20.0;
const textSizeNormalMedium = 21.0;
const textSizeNormalLarge = 22.0;
const textSizeSLarge = 23.0;
const textSizeLarge = 24.0;
const textSizeTwentySix = 26.0;
const textSizeXLarge = 28.0;
const textSizeXXLarge = 30.0;
