import 'dart:ui';
import 'package:flutter/material.dart';

const primary_blue = Color(0xFFB1CB2BC);
const background_blue = Color(0xFFBCC1F4);
const primary_black = Colors.black;
const placeholder_grey = Color(0xFFAAAAAA);
const txt_grey = Color(0xFFB7B7B7);
const border_input_grey = Color(0xFFE3E3E3);
const background_article_grey = Color(0xFFF6F6F7);
const blue_article_name =Color(0xFF112637);
const primary_white = Colors.white;
const txt_dark = Color(0xFF112637);
const back_input_search = Color(0xFFF7F7F8);
const red_favoris = Color(0xFFFF2B2B);
const background_recentFilter = Color(0xFFF2F2F2);
const grey_descrptioProd = Color(0xFFC6C6C6);
const inactive_tab = Color(0xFFCBCBCB);
const txt_adresse = Color(0xFF757575);