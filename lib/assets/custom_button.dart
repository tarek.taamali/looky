import 'package:flutter/material.dart';
import 'fonts.dart';

class CustomButton extends StatelessWidget {
  final Function() onPressed;
  final String txtButton;
  final Color color;
  final Color backcolor;
  final BoxDecoration boxDecoration;
  final bool sideColor;
  final double? width;
  final Color BorderColor;
  const CustomButton(
      {Key? key,
      required this.onPressed,
      required this.txtButton,
      required this.color,
      required this.backcolor,
      required this.boxDecoration,
      this.sideColor = false,
      required this.BorderColor,
      this.width})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      child: RaisedButton(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.0),
          side: BorderSide(
            color: sideColor ? BorderColor : backcolor,
          ),
        ),
        padding: EdgeInsets.all(0.0),
        onPressed: onPressed,
        child: Container(
          height: 50.0,
          width: MediaQuery.of(context).size.width * 0.85,
          child: Center(
            child: Text(
              txtButton,
              style: TextStyle(
                fontFamily: robotoBold,
                fontSize: textSizeMedium,
                color: color,
              ),
            ),
          ),
          decoration: boxDecoration,
        ),
      ),
    );
  }
}
