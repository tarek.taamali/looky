// To parse this JSON data, do
//
//     final products = productsFromJson(jsonString);

import 'dart:convert';

import 'package:applooki/constant.dart';
import 'package:applooki/core/utf_helper.dart';

Products productsFromJson(String str) => Products.fromJson(json.decode(str));

class Products {
  Products({
    required this.products,
  });

  List<Product> products;

  factory Products.fromJson(Map<String, dynamic> json) => Products(
        products: List<Product>.from(
            json["products"].map((x) => Product.fromJson(x))),
      );
  Map<String, dynamic> toJson() => {
        "products": List<dynamic>.from(products.map((x) => x.toJson())),
      };
}

class Product {
  Product({
    required this.id,
    required this.idCategoryDefault,
    required this.idDefaultImage,
    required this.name,
    required this.description,
    required this.descriptionShort,
    required this.price1,
    required this.condition,
    required this.images,
    required this.idSeller,
    required this.dateAdd,
    required this.productsOptions,
    required this.combinations,
    required this.reducedPrice,
  });

  int id;
  String idCategoryDefault;
  String idDefaultImage;
  String name;
  String description;
  String descriptionShort;
  String price1;
  String reducedPrice;
  String condition;
  List<String> images;

  String idSeller;
  DateTime dateAdd;
  List<ProductOptionValue> productsOptions;
  List<String> combinations;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
      id: json["id"],
      idCategoryDefault: json["id_category_default"],
      idDefaultImage: json["id_default_image"] == null ||
              json["id_default_image"].toString().isEmpty
          ? ''
          : PathImage +
              json["id"].toString() +
              '/' +
              json["id_default_image"].toString(),
      name: toDecodedString(json["name"]),
      description: toDecodedString(json["description"]),
      descriptionShort: json["description_short"],
      price1: json["price"],
      reducedPrice: json["reduced_price"].toString(),
      condition: json["condition"],
      images: json["associations"]["images"] == null
          ? []
          : List<String>.from(
              json["associations"]["images"]?.map((x) {
                return PathImage + json["id"].toString() + '/' + x["id"];
              }),
            ),
      idSeller: json["id_seller"] is bool ? '' : json["id_seller"],
      dateAdd: DateTime.parse(json["date_add"]),
      productsOptions: json["associations"] != null &&
              json["associations"]["product_option_values"] != null
          ? List<ProductOptionValue>.from(
              json["associations"]["product_option_values"].map((e) =>
                  ProductOptionValue.fromJson(e as Map<String, dynamic>)))
          : [],
      combinations: json["associations"] != null &&
              json["associations"]["combinations"] != null
          ? List<String>.from(
              json["associations"]["combinations"].map((e) => e["id"]),
            )
          : []);

  factory Product.fromJson2(Map<String, dynamic> json) => Product(
      id: json["id_product"],
      idCategoryDefault: '',
      idDefaultImage: json["id_default_image"] == null ||
              json["id_default_image"].toString().isEmpty
          ? ''
          : PathImage +
              json["id_product"].toString() +
              '/' +
              json["id_default_image"].toString(),
      name: toDecodedString(json["name"]),
      description: toDecodedString(json["description"]),
      descriptionShort: json["description"],
      condition: '',
      images: [],
      idSeller: "0",
      dateAdd: DateTime.now(),
      price1: json['price'],
      reducedPrice: json['price'],
      productsOptions: [],
      combinations: []);

  Map<String, dynamic> toJson() => {
        "id": id,
        "idCategoryDefault": idCategoryDefault,
        "idDefaultImage": idDefaultImage,
        "name": name,
        "description": description,
        "descriptionShort": descriptionShort,
        "price1": price1,
        "reducedPrice": reducedPrice,
        "condition": condition,
        "images": List<dynamic>.from(images.map((x) => x)),
        "idSeller": idSeller,
        "dateAdd": dateAdd.toString(),
        "productsOptions":
            List<dynamic>.from(productsOptions.map((x) => x.toJson())),
        "combinations": List<dynamic>.from(combinations.map((x) => x))
      };

  factory Product.fromJsonLocal(Map<String, dynamic> json) => Product(
      id: json["id"],
      idCategoryDefault: json["idCategoryDefault"],
      idDefaultImage: json["idDefaultImage"],
      name: json["name"],
      description: json["description"],
      descriptionShort: json["descriptionShort"],
      price1: json["price1"],
      reducedPrice: json["reducedPrice"].toString(),
      condition: json["condition"],
      images: List<String>.from(json["images"].map((x) => x)),
      idSeller: json["idSeller"],
      dateAdd: DateTime.parse(json["dateAdd"]),
      productsOptions: List<ProductOptionValue>.from(
          json["productsOptions"].map((x) => ProductOptionValue.fromJson(x))),
      combinations: List<String>.from(json["combinations"].map((x) => x)));
}

class ProductOptionValue {
  ProductOptionValue({
    this.id = '',
    this.name = '',
    this.parent = '',
    this.groupType = '',
  });

  String id;
  String name;
  String parent;
  //String idParent;
  String groupType;

  factory ProductOptionValue.fromJson(Map<String, dynamic> json) =>
      ProductOptionValue(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? '' : json["name"],
        parent: json["parent"] == null ? '' : json["parent"],
        // idParent: json["id_parent"] == null ? null : json["id_parent"],
        groupType: json["group_type"] == null ? null : json["group_type"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "parent": parent,
        "group_type": groupType,
      };
}
