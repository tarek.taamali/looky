import 'dart:convert';

class Categories {
  Categories({
    required this.categories,
  });

  List<Category> categories;

  factory Categories.fromJson(Map<String, dynamic> json) => Categories(
        categories: List<Category>.from(
            json["categories"].map((x) => Category.fromJson(x))).toList(),
      );
}

class Category {
  Category({
    required this.id,
    required this.name,
    required this.levelDepth,
    required this.idParent,
  });

  int id;
  String name;
  String levelDepth;
  String idParent;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json["id"],
        name: json["name"],
        levelDepth: json["level_depth"],
        idParent: json["id_parent"],
      );

  factory Category.empty() => Category(
        id: 0,
        name: '',
        levelDepth: '',
        idParent: '',
      );
}
