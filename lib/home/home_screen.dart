import 'dart:ui';
import 'package:applooki/Utils/app_sizes.dart';
import 'package:applooki/Utils/utils.dart';
import 'package:applooki/assets/elements.dart';
import 'package:applooki/home/home_view_model.dart';
import 'package:applooki/home/products_response.dart';
import 'package:applooki/assets/fonts.dart';
import 'package:applooki/constant.dart';
import 'package:applooki/detailsproduit/detailsproduit_screen.dart';
import 'package:applooki/filtre_recherche/filtre_options_list.dart';
import 'package:applooki/filtre_recherche/filtre_recherche_screen.dart';
import 'package:applooki/services/top_level_providers.dart';
import 'package:applooki/shared/categories_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:applooki/assets/colors.dart';
import 'component/empty_list.dart';
import 'component/loading_list.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with AutomaticKeepAliveClientMixin {
  FiltreOption? filtreOption = listOptions[1];
  var textSearch = TextEditingController();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      loadCategories();
      loadProduct(textSearch.text, showLoader: false);
    });
  }

  loadCategories() {
    final viewModel = context.read(menuViewModel);
    viewModel.loadCategories();
  }

  loadProduct(String search, {bool showLoader: true}) async {
    final viewModel = context.read(menuViewModel);
    viewModel.productListLoaded(
      filtreOption!.label,
      search,
      showLoader: showLoader,
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    Size size = MediaQuery.of(context).size;
    final double itemHeight = 189.0;
    final double itemWidth = 146.0;
    return Scaffold(
      backgroundColor: primary_white,
      body: SafeArea(
        child: ProviderListener<MenuViewModel>(
          provider: menuViewModel,
          onChange: (context, _menuView) {
            if (_menuView.showLoader) {
              Loader.show(
                context,
                progressIndicator: LoaderWidget(context),
              );
            } else {
              Loader.hide();
            }
          },
          child: Center(
            child: Container(
                width: size.width * 0.85,
                child: Consumer(builder: (context, watch, child) {
                  final viewModel = watch(menuViewModel);
                  return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        _buildTopBar(context),
                        gapW4,
                        _buildTextCategory(size),
                        CategoriesBarWidget(
                          onPress: (e) {
                            viewModel.selectCategory(
                                e, filtreOption!.label, textSearch.text);
                          },
                        ),
                        buildTrendBar(size),
                        Expanded(
                          child: viewModel.listMenu.length == 0 &&
                                  viewModel.loadData == Status.loading
                              ? LoadingList()
                              : Container(
                                  child: viewModel.listMenu.length == 0 &&
                                          viewModel.loadData == Status.loaded
                                      ? EmptyList()
                                      : GridView.builder(
                                          padding: EdgeInsets.only(
                                              top: 5, bottom: 10),
                                          gridDelegate:
                                              SliverGridDelegateWithFixedCrossAxisCount(
                                                  childAspectRatio:
                                                      (itemWidth / itemHeight),
                                                  crossAxisCount: 2,
                                                  crossAxisSpacing: 10,
                                                  mainAxisSpacing: 20),
                                          shrinkWrap: true,
                                          itemCount: viewModel.listMenu.length,
                                          itemBuilder: (context, index) {
                                            return buildItemCard(
                                                context,
                                                viewModel.listMenu[index],
                                                size);
                                          },
                                        ),
                                ),
                        ),
                      ]);
                })),
          ),
        ),
      ),
    );
  }

  Widget buildTrendBar(Size size) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            'Produits populaire',
            style: TextStyle(
              fontSize: 15,
              fontFamily: fontBold,
              color: primary_black,
            ),
          ),
          GestureDetector(
            onTap: () {
              //setState(() {});
            },
            child: Text(
              '',
              style: TextStyle(
                fontSize: 13,
                fontFamily: fontRegular,
                color: primary_blue,
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildTextCategory(Size size) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 5),
      child: Text(
        "Catégorie",
        style: TextStyle(
            fontSize: textSizeMMedium,
            fontFamily: fontBold,
            color: primary_black),
      ),
    );
  }

  Widget _buildTopBar(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.03,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.65,
            height: 40,
            decoration: BoxDecoration(
                color: back_input_search,
                borderRadius: BorderRadius.circular(8)),
            child: TextField(
              controller: textSearch,
              textInputAction: TextInputAction.search,
              onSubmitted: (value) {
                if (value.isNotEmpty) {
                  loadProduct(value);
                }
              },
              onChanged: (value) {
                if (value.isEmpty) {
                  loadProduct(value);
                }
              },
              decoration: InputDecoration(
                  prefixIcon: Icon(
                    Icons.search,
                    color: primary_blue,
                  ),
                  hintText: 'Ce que vous vouler',
                  hintStyle: TextStyle(
                    fontFamily: fontRegular,
                    fontSize: 15.0,
                    color: txt_dark,
                  ),
                  border: InputBorder.none),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.15,
            height: 40,
            decoration: BoxDecoration(
                color: back_input_search,
                borderRadius: BorderRadius.circular(8)),
            child: IconButton(
              icon: Icon(
                Icons.tune,
                color: primary_blue,
                size: 20.0,
              ),
              iconSize: 15,
              color: back_input_search,
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => RechercheFiltreScreen(
                            filtreOption: filtreOption,
                            onSelect: (sfiltreOption) {
                              setState(() {
                                filtreOption = sfiltreOption;
                              });
                              loadProduct(textSearch.text);
                            })));
              },
            ),
          ),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

Widget buildItemCard(BuildContext context, Product product, Size size) {
  return GestureDetector(
    onTap: () {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => DetailsProduitScreen(
                  product: product,
                )),
      );
    },
    child: Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: background_article_grey,
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 131.0,
              width: 230,
              child: Image.network(
                product.idDefaultImage,
                headers: kheaders,
                fit: BoxFit.fitWidth,
                errorBuilder: (context, error, stackTrace) {
                  return Image.asset(
                    'images/no-image.png',
                    fit: BoxFit.fitWidth,
                  );
                },
              ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin:
                            EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                        child: SizedBox(
                          width: size.width * 0.25,
                          child: Text(
                            product.name,
                            overflow: TextOverflow.ellipsis,
                            softWrap: false,
                            style: TextStyle(
                                fontSize: 13,
                                fontFamily: fontSemiBold,
                                color: txt_dark),
                          ),
                        ),
                      ),
                      Container(
                        margin:
                            EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                        child: Text(convertToStringPrice(product.reducedPrice),
                            style: TextStyle(
                                fontSize: 13,
                                fontFamily: fontSemiBold,
                                color: primary_blue),
                            textAlign: TextAlign.left),
                      ),
                    ],
                  ),
                  GestureDetector(
                    onTap: () {
                      final viewModel = context.read(favorisViewModel);
                      viewModel.addOrRemoveProductToFavoris(context, product);
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: 5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12),
                        color: primary_white,
                      ),
                      constraints:
                          BoxConstraints(minHeight: 35.0, minWidth: 35.0),
                      child: Consumer(builder: (context, watch, child) {
                        final viewModel = watch(favorisViewModel);
                        return Icon(
                          viewModel.isFavoris(product)
                              ? Icons.favorite
                              : Icons.favorite_border,
                          color: red_favoris,
                        );
                      }),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    ),
  );
}
