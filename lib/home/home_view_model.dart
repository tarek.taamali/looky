import 'dart:convert';
import 'package:applooki/Utils/utils.dart';
import 'package:applooki/home/categories_responses.dart' as Rs;
import 'package:applooki/home/products_response.dart';
import 'package:applooki/constant.dart';
import 'package:applooki/models/customers.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class MenuViewModel extends ChangeNotifier {
  List<Product> listMenu = [];
  List<Rs.Category> listCategories = [], filterCategoris = [];
  Rs.Category selectedCategory = Rs.Category.empty();
  Status loadData = Status.none;
  bool showLoader = false;
  List<Customer> customers = [];

  MenuViewModel() {
    loadCategories();
    loadCustomers();
  }

  loadCategories() async {
    final response = await http.get(
        Uri.parse(
          BASE_URL + "categories?output_format=JSON&display=full",
        ),
        headers: kheaders);
    var resp = json.decode(response.body);
    if (response.statusCode == 200) {
      listCategories = Rs.Categories.fromJson(resp).categories;
      filterCategoris =
          listCategories.where((element) => element.levelDepth == "2").toList();
    }
    notifyListeners();
  }

  loadCustomers() async {
    final response = await http.get(
        Uri.parse(BASE_URL + "customers?output_format=JSON&display=full"),
        headers: kheaders);
    if (response.statusCode == 200) {
      var customersResp = Customers.fromJson(json.decode(response.body));
      customers = customersResp.customers;
      //print("LoadCustomeres= ${customersResp.customers.length}");
    }
    notifyListeners();
  }

  String getCustomerById(String id) {
    return customers.firstWhere((element) => element.id == id).firstname +
        " " +
        customers.firstWhere((element) => element.id == id).lastname;
  }

  productListLoaded(String? sort, String filtreName,
      {bool showLoader = true}) async {
    loadData = Status.loading;
    this.showLoader = showLoader;
    notifyListeners();
    print(Uri.parse(
      PRODUCTS_URL + _buildQuery(sort, filtreName),
    ));
    final response = await http.get(
      Uri.parse(
        PRODUCTS_URL + _buildQuery(sort, filtreName),
      ),
      headers: kheaders,
    );
    print(_buildQuery(sort, filtreName));

    if (response.statusCode == 200) {
      var _resp = json.decode(response.body);

      listMenu = [];
      if (_resp is Map<String, dynamic>)
        listMenu = Products.fromJson(_resp).products;
    }
    this.showLoader = false;
    loadData = Status.loaded;
    notifyListeners();
  }

  selectCategory(Rs.Category category, String? sort, String searchText) {
    selectedCategory =
        selectedCategory.id != category.id ? category : Rs.Category.empty();
    productListLoaded(sort, searchText);
  }

  String _buildQuery(String? sort, String filtreName) {
    String _sortParam = sort == null || sort.isEmpty ? "" : "&sort=$sort";
    String _filtreN = filtreName.isEmpty
        ? "&filter[active]=1"
        : "&filter[name]=%[$filtreName]%&filter[active]=1";
    String _queryParam = selectedCategory.id == 0
        ? "?output_format=JSON&display=full" + _sortParam + _filtreN
        : "?output_format=JSON&display=full&filter[id_category_default]=${selectedCategory.id.toString()}&filter[active]=1" +
            _sortParam +
            _filtreN;
    return _queryParam;
  }
}
