import 'package:applooki/assets/colors.dart';
import 'package:applooki/assets/fonts.dart';
import 'package:applooki/constant.dart';
import 'package:flutter/material.dart';
import 'package:skeleton_loader/skeleton_loader.dart';

class LoadingList extends StatelessWidget {
  const LoadingList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final double itemHeight = 189.0;
    final double itemWidth = 146.0;
    
    return Container(
      padding: EdgeInsets.only(top: 5),
      child: SingleChildScrollView(
        child: SkeletonLoader(
            builder: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              childAspectRatio: (itemWidth / itemHeight),
              crossAxisCount: 2,
              crossAxisSpacing: 10,
              mainAxisSpacing: 20),
          shrinkWrap: true,
          itemCount: 6,
          itemBuilder: (context, index) {
            return _buildEmptyItemCard(size);
          },
        )),
      ),
    );
  }

  Widget _buildEmptyItemCard(Size size) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: background_article_grey,
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 131.0,
                width: 230,
                child: Image.network(
                  '',
                  headers: kheaders,
                  fit: BoxFit.fitWidth,
                  errorBuilder: (context, error, stackTrace) {
                    return Image.asset(
                      'images/no-image.png',
                      fit: BoxFit.fitWidth,
                    );
                  },
                ),
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin:
                              EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                          child: SizedBox(
                            width: size.width * 0.25,
                            child: Text(
                              '',
                              overflow: TextOverflow.ellipsis,
                              softWrap: false,
                              style: TextStyle(
                                  fontSize: 13,
                                  fontFamily: fontSemiBold,
                                  color: txt_dark),
                            ),
                          ),
                        ),
                        Container(
                          margin:
                              EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                          child: Text('',
                              style: TextStyle(
                                  fontSize: 13,
                                  fontFamily: fontSemiBold,
                                  color: primary_blue),
                              textAlign: TextAlign.left),
                        ),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 5),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12),
                        color: primary_white,
                      ),
                      constraints:
                          BoxConstraints(minHeight: 35.0, minWidth: 35.0),
                      child: Icon(
                        Icons.favorite_border,
                        color: red_favoris,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
