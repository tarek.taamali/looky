import 'package:flutter/material.dart';

class EmptyList extends StatelessWidget {
  const EmptyList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset("images/carte_vide.png"),
        SizedBox(
          height: 25,
        ),
        Text("Aucune annonce", style: TextStyle(fontWeight: FontWeight.bold))
      ],
    );
  }
}
