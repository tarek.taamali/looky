import 'dart:convert';

const URL_FORGET_PASSWORD = "https://looki.ma/";
const BASE_URL = "https://looki.ma/api/";
const CUSTOMERS_URL = BASE_URL + "customers";
const PRODUCTS_URL = BASE_URL + "products";
const COMBINAISON_URL = BASE_URL + "combinations";
const KSELLERS_PRODUCTS_URL = BASE_URL + "kbsellerproducts";
const KSELLER_URL = BASE_URL + "kbsellers";
const PathImage = BASE_URL + "images/products/";
var kAuthorization =
    'Basic ' + base64Encode(utf8.encode('PB9W7VFGI1ZZXRJPQPC4XC3P9QMJ79UD:'));
Map<String, String>  kheaders = {'authorization': kAuthorization};
const CARTS_URL = BASE_URL + "carts";
