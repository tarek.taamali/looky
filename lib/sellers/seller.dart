class SellerResponse {
  Seller seller;
  SellerResponse({required this.seller});
  factory SellerResponse.fromJson(Map<String, dynamic> json) =>
      SellerResponse(seller: Seller.fromJson(json["seller"]));
}

class Seller {
  String title = "";
  String phone = "";
  String businessEmail = "";
  Seller({
    required this.title,
    required this.phone,
    required this.businessEmail,
  });

  factory Seller.fromJson(Map<String, dynamic> json) => Seller(
        title: json["title"],
        phone: json["phone_number"],
        businessEmail: json['business_email'],
      );
}
