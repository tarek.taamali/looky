class CategoryResponse {
  Category category;
  CategoryResponse({required this.category});
  factory CategoryResponse.fromJson(Map<String, dynamic> json) =>
      CategoryResponse(
          category: json["categories"] == null
              ? Category(name: "")
              : List<Category>.from(
                  json["categories"].map((x) => Category.fromJson(x))).first);
}

class Category {
  String name = "";
  Category({required this.name});

  factory Category.fromJson(Map<String, dynamic> json) =>
      Category(name: json["name"] ?? "");
}
