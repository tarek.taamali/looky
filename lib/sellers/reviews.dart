class ReviewResponse {
  List<Review> reviews;
  ReviewResponse({required this.reviews});
  factory ReviewResponse.fromJson(Map<String, dynamic> json) => ReviewResponse(
      reviews: List<Review>.from(
          json["kbsellerreviews"].map((x) => Review.fromJson(x))));
}

class Review {
  String comment = "";
  int idCcustomer = -1;
  int approved;

  Review({
    required this.comment,
    required this.idCcustomer,
    required this.approved,
  });

  factory Review.fromJson(Map<String, dynamic> json) => Review(
      comment: json["comment"] ?? "",
      idCcustomer: int.parse(json["id_customer"]),
      approved: int.parse(json["approved"]));
}
