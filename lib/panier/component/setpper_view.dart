import 'package:flutter/material.dart';

class StepProgressView extends StatelessWidget {
  final double _width;
  final List<IconData> _icons;
  final List<String> _titles;
  final int _curStep;
  final Color _activeColor;
  final double lineWidth = 1.0;
  StepProgressView({
    Key? key,
    required List<IconData> icons,
    required int curStep,
    required List<String> titles,
    required double width,
    required Color color,
  })  : _icons = icons,
        _titles = titles,
        _curStep = curStep,
        _width = width,
        _activeColor = color,
        assert(curStep > 0 == true && curStep <= icons.length),
        assert(width > 0),
        super(key: key);

  Widget build(BuildContext context) {
    return Container(
        width: this._width,
        child: Column(
          children: <Widget>[
            Row(
              children: _iconViews(),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: _titleViews(),
            ),
          ],
        ));
  }

  List<Widget> _iconViews() {
    var list = <Widget>[];
    _icons.asMap().forEach((i, icon) {
      list.add(
        Container(
          width: 30.0,
          height: 30.0,
          padding: EdgeInsets.all(0),
          child: Center(
            child: //(i == 0 || _curStep > i + 1)
                i == 0 || i < _curStep
                    ? Container(
                        decoration: new BoxDecoration(
                            color: _activeColor,
                            borderRadius: new BorderRadius.all(
                                new Radius.circular(25.0))),
                        width: 20.0,
                        height: 20.0,
                      )
                    : Container(),
          ),
          decoration: new BoxDecoration(
            borderRadius: new BorderRadius.all(new Radius.circular(25.0)),
            border: new Border.all(
              color: _activeColor,
              width: 1.0,
            ),
          ),
        ),
      );

      //line between icons
      if (i != _icons.length - 1) {
        list.add(Expanded(
            child: Container(
          height: lineWidth,
          color: _activeColor,
        )));
      }
    });

    return list;
  }

  List<Widget> _titleViews() {
    var list = <Widget>[];
    _titles.asMap().forEach((i, text) {
      list.add(Text(text));
    });
    return list;
  }
}
