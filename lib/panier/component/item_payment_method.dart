import 'package:applooki/assets/colors.dart';
import 'package:flutter/material.dart';

class ItemMethodPaymentMethod extends StatelessWidget {
  final Widget icon;
  final String text;
  final bool checked;
  final Function? onTap;
  const ItemMethodPaymentMethod({
    Key? key,
    required this.icon,
    required this.text,
    this.checked: false,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onTap!();
      },
      child: Container(
        height: 60.0,
        padding: EdgeInsets.all(15.0),
        decoration: BoxDecoration(
          color: background_article_grey,
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                icon,
                SizedBox(
                  width: 10,
                ),
                Text(text),
              ],
            ),
            checked
                ? Container(
                    margin: EdgeInsets.only(right: 5),
                    width: 20.0,
                    height: 20.0,
                    padding: EdgeInsets.all(0),
                    child: Center(
                        child: Container(
                      child: Icon(
                        Icons.check,
                        color: Colors.white,
                        size: 10,
                      ),
                      width: 10.0,
                      height: 10.0,
                    )),
                    decoration: new BoxDecoration(
                        borderRadius:
                            new BorderRadius.all(new Radius.circular(25.0)),
                        color: primary_blue),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}
