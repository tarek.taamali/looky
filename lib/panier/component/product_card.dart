import 'package:applooki/Utils/utils.dart';
import 'package:applooki/assets/colors.dart';
import 'package:applooki/assets/fonts.dart';
import 'package:applooki/assets/images_links.dart';
import 'package:applooki/constant.dart';
import 'package:applooki/detailsproduit/Components/badge.dart';
import 'package:applooki/home/products_response.dart';
import 'package:applooki/services/top_level_providers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';

class ProductCard extends StatelessWidget {
  final Product product;

  ProductCard({Key? key, required this.product}) : super(key: key);
  void doNothing(BuildContext context) {
    final viewModel = context.read(panierViewModel);
    viewModel.removeProduct(context, product);
  }

  @override
  Widget build(BuildContext context) {
    var inputFormat = DateFormat('dd-MM-yyyy');

    return Slidable(
      key: Key(product.id.toString()),
      endActionPane: ActionPane(
        motion: const ScrollMotion(),
        children: [
          SlidableAction(
            onPressed: doNothing,
            foregroundColor: Colors.red,
            icon: Icons.delete,
            label: 'Supprimer',
          ),
        ],
      ),
      child: Container(
        height: 115.0,
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.only(top: 5, bottom: 5),
        decoration: BoxDecoration(
          color: primary_white,
          borderRadius: BorderRadius.circular(15.0),
          border: Border.all(color: primary_blue.withOpacity(0.59), width: 1.5),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              flex: 1,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(7),
                  image: DecorationImage(
                    image: NetworkImage(
                        product.images.length > 0 ? product.images[0] : '',
                        headers: kheaders),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 20,
            ),
            Expanded(
              flex: 3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Text(
                          product.name,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          softWrap: false,
                          style: TextStyle(
                            fontSize: 14.0,
                            fontFamily: fontSemiBold,
                            color: primary_black,
                          ),
                        ),
                      ),
                      Text(
                        convertToStringPrice(product.reducedPrice),
                        style: TextStyle(
                          fontSize: 15.0,
                          fontFamily: fontSemiBold,
                          color: primary_blue,
                        ),
                      ),
                    ],
                  ),
                  Text(
                    'Publier par ',
                    style: TextStyle(
                      fontSize: 9.0,
                      fontFamily: fontRegular,
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Wrap(
                    spacing: 2.0,
                    runSpacing: 2,
                    children: [
                      SmallBadgeProduct(
                        title: "Date de dépôt",
                        content: inputFormat.format(product.dateAdd).toString(),
                        icon: iconeDateDepot,
                        widthBadge: 80,
                      ),
                      Visibility(
                        visible: product.productsOptions.length > 0 &&
                            product.productsOptions
                                    .where((element) =>
                                        element.parent == 'Couleur')
                                    .length >
                                0,
                        child: SmallBadgeProduct(
                          title: 'Couleur',
                          icon: iconeColor,
                          content: product.productsOptions.length > 0
                              ? product.productsOptions
                                  .firstWhere(
                                      (element) => element.parent == 'Couleur',
                                      orElse: () => ProductOptionValue())
                                  .name
                              : '',
                          widthBadge: 50,
                        ),
                      ),
                      /*SmallBadgeProduct(
                        title: 'Etat',
                        icon: iconeEtat,
                        content: "${product.condition}",
                        widthBadge: 50,
                      ),*/
                      Visibility(
                        visible: product.productsOptions.length > 0 &&
                            product.productsOptions
                                    .where(
                                        (element) => element.parent == 'Taille')
                                    .length >
                                0,
                        child: SmallBadgeProduct(
                          title: 'Taille',
                          icon: iconeTaille,
                          content: product.productsOptions.length > 0
                              ? product.productsOptions
                                          .where((element) =>
                                              element.parent == 'Taille')
                                          .length >
                                      0
                                  ? product.productsOptions
                                      .where(
                                        (element) => element.parent == 'Taille',
                                      )
                                      .first
                                      .name
                                  : ''
                              : '',
                          widthBadge: 50,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
