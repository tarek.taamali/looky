import 'package:applooki/assets/colors.dart';
import 'package:applooki/assets/fonts.dart';
import 'package:applooki/profile/adresses/adresses.dart';
import 'package:flutter/material.dart';

class ItemAdresseWidget extends StatelessWidget {
  final Address address;
  final Address? defaultAddress;
  final Function(Address) onSelect;
  const ItemAdresseWidget(
      {Key? key,
      required this.address,
      this.defaultAddress,
      required this.onSelect})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 120.0,
      margin: EdgeInsets.only(top: 10, bottom: 10),
      padding: EdgeInsets.all(15.0),
      decoration: BoxDecoration(
        color: background_article_grey,
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GestureDetector(
            onTap: () {
              onSelect(address);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 5),
                        width: 20.0,
                        height: 20.0,
                        padding: EdgeInsets.all(0),
                        child: Center(
                            child: Container(
                          decoration: new BoxDecoration(
                            color: defaultAddress != null &&
                                    defaultAddress == address
                                ? primary_blue
                                : Colors.transparent,
                            borderRadius: new BorderRadius.all(
                              new Radius.circular(25.0),
                            ),
                          ),
                          width: 10.0,
                          height: 10.0,
                        )),
                        decoration: new BoxDecoration(
                          borderRadius:
                              new BorderRadius.all(new Radius.circular(25.0)),
                          border: new Border.all(
                            color: Colors.grey,
                            width: 1.0,
                          ),
                        ),
                      ),
                      Text(
                        address.alias,
                        style: TextStyle(
                          fontFamily: fontSemiBold,
                          fontSize: 16.0,
                          color: primary_black,
                        ),
                      ),
                    ],
                  ),
                ),
                GestureDetector(
                  child: Icon(
                    Icons.edit_rounded,
                    color: primary_blue,
                  ),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 20),
            child: Text(
              address.phoneMobile,
              style: TextStyle(
                fontSize: 13.0,
                fontFamily: fontRegular,
                color: txt_adresse,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 20),
            child: Text(
              address.address1,
              style: TextStyle(
                fontSize: 13.0,
                fontFamily: fontRegular,
                color: txt_adresse,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
