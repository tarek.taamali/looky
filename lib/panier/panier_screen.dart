import 'package:applooki/Utils/local_storage.dart';
import 'package:applooki/Utils/utils.dart';
import 'package:applooki/assets/custom_button.dart';
import 'package:applooki/assets/elements.dart';
import 'package:applooki/assets/images_links.dart';
import 'package:applooki/auth/sign_in_screen.dart';
import 'package:applooki/services/top_level_providers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/svg.dart';
import '../assets/fonts.dart';
import 'package:applooki/assets/colors.dart';
import 'component/dotted.dart';
import 'component/product_card.dart';
import '../shared/top_bar_widget.dart';
import 'delivery_screen.dart';

class PanierScreen extends StatefulWidget {
  @override
  _PanierScreenState createState() => _PanierScreenState();
}

class _PanierScreenState extends State<PanierScreen> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: primary_white,
      body: SafeArea(
        child: Container(
          width: size.width,
          child: Consumer(
            builder: (context, watch, child) {
              final viewModel = watch(panierViewModel);
              return Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: size.width * 0.075),
                    child: TopBarWidget(
                      title: 'Panier',
                    ),
                  ),
                  Expanded(
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: size.width * 0.075),
                      child: SingleChildScrollView(
                          child: Column(
                        children: viewModel.products.length == 0
                            ? _buildEmptyPanier(size)
                            : viewModel.products
                                .map((e) => ProductCard(
                                      product: e,
                                    ))
                                .toList(),
                      )),
                    ),
                  ),
                  Visibility(
                    visible: viewModel.products.length == 0 ? false : true,
                    child: Container(
                      height: 180,
                      child: Container(
                        padding: EdgeInsets.all(5),
                        width: size.width,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(25),
                            topRight: Radius.circular(25),
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset: Offset(0, 3),
                            ),
                          ],
                        ),
                        child: Center(
                          child: Container(
                            width: size.width * 0.85,
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text("Prix"),
                                    Text(convertToStringPrice(
                                        viewModel.prix.toString())),
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Livraison",
                                      style: TextStyle(
                                        fontSize: 12,
                                        fontFamily: fontRegular,
                                      ),
                                    ),
                                    Text(
                                      "0.0",
                                      style: TextStyle(
                                        fontSize: 12,
                                        fontFamily: fontRegular,
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                DotWidget(
                                  dashColor: primary_blue,
                                  dashHeight: 2,
                                  dashWidth: 4,
                                  totalWidth: size.width * 0.85,
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Livraison",
                                      style: TextStyle(
                                        fontSize: 12,
                                        fontFamily: fontRegular,
                                      ),
                                    ),
                                    Text(
                                      convertToStringPrice(
                                          viewModel.prix.toString()),
                                      style: TextStyle(
                                        fontSize: 12,
                                        fontFamily: fontRegular,
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                CustomButton(
                                  onPressed: () {
                                    String isAuth =
                                        StorageUtil.getString("connected");
                                    if (isAuth == 'connected') {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              DeliveryScreen(),
                                        ),
                                      );
                                    } else {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => SignInScreen(),
                                        ),
                                      );
                                    }
                                  },
                                  txtButton: "Suivant",
                                  color: primary_white,
                                  backcolor: primary_blue,
                                  boxDecoration: boxDecorationBtnCnx(
                                    bgColor: primary_blue,
                                  ),
                                  sideColor: false,
                                  BorderColor: primary_blue,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  List<Widget> _buildEmptyPanier(Size size) {
    return [
      Container(
        width: size.width * 0.5,
        margin: EdgeInsets.only(
            top: size.height * 0.05, bottom: size.height * 0.075),
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: SvgPicture.asset("images/ic_looki.svg"),
      ),
      Container(
        width: size.width * 0.85,
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Column(
          children: [
            SvgPicture.asset('images/empty_panier.svg'),
            SizedBox(
              height: size.height * 0.03,
            ),
            Text(
              'Votre panier est vide',
              style: TextStyle(
                fontSize: 18.0,
                fontFamily: fontSemiBold,
                color: primary_black,
              ),
            ),
          ],
        ),
      )
    ];
  }
}
