import 'dart:convert';

import 'package:another_flushbar/flushbar.dart';
import 'package:applooki/Utils/local_storage.dart';
import 'package:applooki/Utils/xml_helpers.dart';
import 'package:applooki/assets/elements.dart';
import 'package:applooki/constant.dart';
import 'package:applooki/home/products_response.dart';
import 'package:applooki/models/customers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:http/http.dart' as http;

class PanierViewModel extends ChangeNotifier {
  List<Product> products = [];
  double prix = 0.0;
  double prixttc = 0.0;
  String cartPath = BASE_URL + "carts?output_format=JSON";

  Future<double> calculatePriceFromProducts(List<Product> products) async {
    double price = 0.0;
    await Future.forEach(
        products, (Product pr) => price += double.parse(pr.reducedPrice));
    return price;
  }

  addProduct(BuildContext context, Product product,
      {bool navigate = false}) async {
    //prix = prix + double.parse(product.price);
    double priceTemp = await calculatePriceFromProducts(products);
    if (!products.contains(product)) {
      Loader.show(
        context,
        progressIndicator: LoaderWidget(context),
      );
      prix = priceTemp + double.parse(product.reducedPrice);
      prixttc = priceTemp + double.parse(product.reducedPrice);

      products.add(product);
      await Future.delayed(Duration(milliseconds: 500));
      Loader.hide();
      Flushbar(
        isDismissible: true,
        margin: EdgeInsets.only(top: 20),
        backgroundColor: Color(0xFF28a745),
        forwardAnimationCurve: Curves.decelerate,
        reverseAnimationCurve: Curves.easeOut,
        animationDuration: Duration(milliseconds: 400),
        duration: Duration(seconds: 2),
        flushbarPosition: FlushbarPosition.TOP,
        message: "${product.name} a été ajouté à votre panier.",
      )..show(context);
    }
    notifyListeners();
  }

  removeProduct(BuildContext context, Product product) async {
    prix = prix - double.parse(product.reducedPrice);
    prixttc = prix;
    if (products.contains(product)) {
      products.remove(product);
      Flushbar(
        isDismissible: true,
        margin: EdgeInsets.only(top: 20),
        backgroundColor: Colors.red,
        forwardAnimationCurve: Curves.decelerate,
        reverseAnimationCurve: Curves.easeOut,
        animationDuration: Duration(milliseconds: 400),
        duration: Duration(seconds: 2),
        flushbarPosition: FlushbarPosition.TOP,
        message: "${product.name} a été supprimé de votre panier.",
      )..show(context);
    }
    notifyListeners();
  }

  reset() {
    products = [];
    prix = 0.0;
    prixttc = 0.0;
    notifyListeners();
  }

  Future<String> createCart(BuildContext context, String idAddress) async {
    Map<String, dynamic> cashedCustomer =
        jsonDecode(StorageUtil.getString('customer'));
    String idCustomer = Customer.fromJson(cashedCustomer).id;
    Loader.show(
      context,
      progressIndicator: LoaderWidget(context),
    );
    try {
      var response = await http.post(Uri.parse(cartPath),
          headers: kheaders,
          body: buildCreateCart(
            products: products,
            idCustomer: idCustomer,
            idAddress: idAddress,
          ));

      Map<String, dynamic> result = json.decode(response.body);
      print(response.body);
      if (response.statusCode == 201 && result.containsKey("cart")) {
        print("cart Id  ${result["cart"]["id"]}");
        StorageUtil.putString("cartId", result["cart"]["id"]);
        Loader.hide();
        return "Created";
      } else {
        Loader.hide();
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text("Une erreur s'est produite.${response.body} "),
          ),
        );
        return "error";
      }
    } catch (error) {
      Loader.hide();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text("api/carts :Une erreur s'est produite. "),
        ),
      );
      print("erreur create cart :\n $error}");
    }
    return "error";
  }

  Future<String> createOrders(BuildContext context, String idAddress) async {
    Map<String, dynamic> cashedCustomer =
        jsonDecode(StorageUtil.getString('customer'));
    String idCustomer = Customer.fromJson(cashedCustomer).id;
    String path = "https://looki.mad-impact.com/api/orders?output_format=JSON";
    Loader.show(
      context,
      progressIndicator: LoaderWidget(context),
    );
    String requestBody = await buildCreateOrder(
      products: products,
      idCustomer: idCustomer,
      idAddress: idAddress,
    );
    try {
      var response = await http.post(Uri.parse(path),
          headers: kheaders, body: requestBody);
      print(response.statusCode);
      print(response.body);
      Map<String, dynamic> result = json.decode(response.body);
      if (response.statusCode == 201 && result.containsKey("order")) {
        StorageUtil.putString("order", result["order"]["id"].toString());
        Loader.hide();
        return "Created";
      } else {
        Loader.hide();
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text("Une erreur s'est produite.${response.body} "),
          ),
        );
        return "error";
      }
    } catch (error) {
      Loader.hide();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text("api/orders :Une erreur s'est produite. "),
        ),
      );
      print("response :\n $error}");
    }
    return "error";
  }

  Future<int> createOrderDetails(BuildContext context) async {
    String path = BASE_URL + "order_details?output_format=JSON";
    try {
      var response = await http.post(
        Uri.parse(path),
        headers: kheaders,
        body: buildCreateOrderDetails(
          products: products,
        ),
      );
      print(response.body);
      Map<String, dynamic> result = json.decode(response.body);
      if (result.containsKey("order_detail")) {
        StorageUtil.putString("order_detail", result["order_detail"]["id"]);
        reset();
        return int.parse(result["order_detail"]["id"]);
      } else {
        return 0;
      }
    } catch (error) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text("api/order_details :Une erreur s'est produite. "),
        ),
      );
      return 0;
    }
  }
}
