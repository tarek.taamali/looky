// To parse this JSON data, do
//
//     final cartsResp = cartsRespFromJson(jsonString);

import 'dart:convert';

CartsResp cartsRespFromJson(String str) => CartsResp.fromJson(json.decode(str));

String cartsRespToJson(CartsResp data) => json.encode(data.toJson());

class CartsResp {
  CartsResp({
    required this.cart,
  });

  Cart cart;

  factory CartsResp.fromJson(Map<String, dynamic> json) => CartsResp(
        cart: Cart.fromJson(json["cart"]),
      );

  Map<String, dynamic> toJson() => {
        "cart": cart.toJson(),
      };
}

class Cart {
  Cart({
    required this.id,
    required this.idAddressDelivery,
    required this.idAddressInvoice,
    required this.idCurrency,
    required this.idCustomer,
    required this.idGuest,
    required this.idLang,
    required this.idShopGroup,
    required this.idShop,
    required this.idCarrier,
    this.recyclable,
    this.gift,
    this.giftMessage,
    this.mobileTheme,
    this.deliveryOption,
    this.secureKey,
    this.allowSeperatedPackage,
    this.dateAdd,
    this.dateUpd,
    this.associations,
  });

  String id;
  String idAddressDelivery;
  String idAddressInvoice;
  String idCurrency;
  String idCustomer;
  String idGuest;
  String idLang;
  String idShopGroup;
  int idShop;
  String idCarrier;
  dynamic recyclable;
  dynamic gift;
  dynamic giftMessage;
  dynamic mobileTheme;
  String? deliveryOption;
  String? secureKey;
  dynamic allowSeperatedPackage;
  DateTime? dateAdd;
  DateTime? dateUpd;
  Associations? associations;

  factory Cart.fromJson(Map<String, dynamic> json) => Cart(
        id: json["id"],
        idAddressDelivery: json["id_address_delivery"],
        idAddressInvoice: json["id_address_invoice"],
        idCurrency: json["id_currency"],
        idCustomer: json["id_customer"],
        idGuest: json["id_guest"],
        idLang: json["id_lang"],
        idShopGroup: json["id_shop_group"],
        idShop: json["id_shop"],
        idCarrier: json["id_carrier"],
        recyclable: json["recyclable"],
        gift: json["gift"],
        giftMessage: json["gift_message"],
        mobileTheme: json["mobile_theme"],
        deliveryOption: json["delivery_option"],
        secureKey: json["secure_key"],
        allowSeperatedPackage: json["allow_seperated_package"],
        dateAdd: DateTime.parse(json["date_add"]),
        dateUpd: DateTime.parse(json["date_upd"]),
        associations: Associations.fromJson(json["associations"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "id_address_delivery": idAddressDelivery,
        "id_address_invoice": idAddressInvoice,
        "id_currency": idCurrency,
        "id_customer": idCustomer,
        "id_guest": idGuest,
        "id_lang": idLang,
        "id_shop_group": idShopGroup,
        "id_shop": idShop,
        "id_carrier": idCarrier,
        "recyclable": recyclable,
        "gift": gift,
        "gift_message": giftMessage,
        "mobile_theme": mobileTheme,
        "delivery_option": deliveryOption,
        "secure_key": secureKey,
        "allow_seperated_package": allowSeperatedPackage,
        "associations": associations!.toJson(),
      };
}

class Associations {
  Associations({
    required this.cartRows,
  });

  List<CartRow> cartRows;

  factory Associations.fromJson(Map<String, dynamic> json) => Associations(
        cartRows: List<CartRow>.from(
            json["cart_rows"].map((x) => CartRow.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "cart_rows": List<dynamic>.from(cartRows.map((x) => x.toJson())),
      };
}

class CartRow {
  CartRow({
    required this.idProduct,
    required this.idProductAttribute,
    required this.idAddressDelivery,
    required this.idCustomization,
    required this.quantity,
  });

  String idProduct;
  String idProductAttribute;
  String idAddressDelivery;
  String idCustomization;
  String quantity;

  factory CartRow.fromJson(Map<String, dynamic> json) => CartRow(
        idProduct: json["id_product"],
        idProductAttribute: json["id_product_attribute"],
        idAddressDelivery: json["id_address_delivery"],
        idCustomization: json["id_customization"],
        quantity: json["quantity"],
      );

  Map<String, dynamic> toJson() => {
        "id_product": idProduct,
        "id_product_attribute": idProductAttribute,
        "id_address_delivery": idAddressDelivery,
        "id_customization": idCustomization,
        "quantity": quantity,
      };
}
