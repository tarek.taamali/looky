import 'package:applooki/Utils/utils.dart';
import 'package:applooki/assets/custom_button.dart';
import 'package:applooki/assets/elements.dart';
import 'package:applooki/assets/fonts.dart';
import 'package:applooki/case_screen/success_paiment_screen.dart';
import 'package:applooki/profile/adresses/add_adresse_screen.dart';
import 'package:applooki/profile/adresses/adresses.dart';
import 'package:applooki/services/top_level_providers.dart';
import 'package:flutter/material.dart';
import 'package:applooki/assets/colors.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'component/dotted.dart';
import 'component/item_adress_widget.dart';
import 'component/item_payment_method.dart';
import 'component/setpper_view.dart';
import '../shared/top_bar_widget.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'component/product_card.dart';
import 'package:applooki/profile/adresses/component/loading_list.dart';

class DeliveryScreen extends StatefulWidget {
  @override
  _DeliveryScreenState createState() => _DeliveryScreenState();
}

class _DeliveryScreenState extends State<DeliveryScreen> {
  final stepIcons = [Icons.person, Icons.person, Icons.person];
  int _curStep = 1;
  PageController controller = PageController();
  Address? defaultAddress;
  int paimentType = 0;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      final viewModel = context.read(addresseViewModel);
      viewModel.loadAddresses();
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: primary_white,
      body: SafeArea(
        child: Container(
          width: size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: size.width * 0.075),
                child: TopBarWidget(),
              ),
              Container(
                height: size.height * 0.12,
                padding: EdgeInsets.symmetric(horizontal: size.width * 0.075),
                child: StepProgressView(
                  icons: stepIcons,
                  width: MediaQuery.of(context).size.width,
                  curStep: _curStep,
                  color: primary_blue,
                  titles: ["Adresse", "Paiments", "Résumé"],
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: size.width * 0.075),
                  child: PageView(
                    controller: controller,
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      _buildAdressWidget(),
                      _buildCardWidget(),
                      _buildResume(),
                    ],
                  ),
                ),
              ),
              _buildNextButton(size),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildNextButton(Size size) {
    return _curStep < 3
        ? Container(
            height: size.height * 0.12,
            child: Container(
              padding: EdgeInsets.all(5),
              width: size.width,
              child: Center(
                child: Container(
                  width: size.width * 0.85,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      CustomButton(
                        onPressed: () async {
                          final viewModel = context.read(panierViewModel);
                          if (_curStep == 1) {
                            if (defaultAddress == null) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  content: Text(
                                    "Sélectionnez l'adresse de livraison",
                                  ),
                                ),
                              );
                            } else {
                              await viewModel
                                  .createCart(
                                context,
                                defaultAddress!.id.toString(),
                              )
                                  .then((String value) {
                                if (value == "Created") {
                                  controller.nextPage(
                                      duration: Duration(milliseconds: 1000),
                                      curve: Curves.easeIn);
                                  setState(() {
                                    _curStep++;
                                  });
                                }
                              });
                            }
                          } else if (_curStep == 2) {
                            if (paimentType == 0) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  content: Text(
                                    "Sélectionnez la méthode de paiment",
                                  ),
                                ),
                              );
                            } else {
                              await viewModel
                                  .createOrders(
                                context,
                                defaultAddress!.id.toString(),
                              )
                                  .then((value) {
                                if (value == "Created") {
                                  controller.nextPage(
                                      duration: Duration(milliseconds: 1000),
                                      curve: Curves.easeIn);

                                  setState(() {
                                    _curStep++;
                                  });
                                }
                              });
                            }
                          }
                          /*else if (_curStep < 3) {
                            controller.nextPage(
                                duration: Duration(milliseconds: 1000),
                                curve: Curves.easeIn);

                            setState(() {
                              _curStep++;
                            });
                          }*/
                        },
                        txtButton: "Suivant",
                        color: primary_white,
                        backcolor: primary_blue,
                        boxDecoration: boxDecorationBtnCnx(
                          bgColor: primary_blue,
                        ),
                        sideColor: false,
                        BorderColor: primary_blue,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )
        : Container(
            height: 180,
            child: Container(
              padding: EdgeInsets.all(5),
              width: size.width,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25),
                  topRight: Radius.circular(25),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 3),
                  ),
                ],
              ),
              child: Consumer(
                builder: (context, watch, child) {
                  final viewModel = watch(panierViewModel);
                  return Center(
                    child: Container(
                      width: size.width * 0.85,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Prix"),
                              Text(convertToStringPrice(
                                  viewModel.prix.toString())),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Livraison",
                                style: TextStyle(
                                  fontSize: 12,
                                  fontFamily: fontRegular,
                                ),
                              ),
                              Text(
                                "Gratuit",
                                style: TextStyle(
                                  fontSize: 12,
                                  fontFamily: fontRegular,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          DotWidget(
                            dashColor: primary_blue,
                            dashHeight: 2,
                            dashWidth: 4,
                            totalWidth: size.width * 0.85,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Prix totale",
                                style: TextStyle(
                                  fontSize: 12,
                                  fontFamily: fontRegular,
                                ),
                              ),
                              Text(
                                convertToStringPrice(viewModel.prix.toString()),
                                style: TextStyle(
                                  fontSize: 12,
                                  fontFamily: fontRegular,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          CustomButton(
                            onPressed: () async {
                              await viewModel
                                  .createOrderDetails(
                                context,
                              )
                                  .then((int value) {
                                if (value != 0) {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => SucessPaimentScrenn(
                                        nbreOrder: value,
                                      ),
                                    ),
                                  );
                                }
                              });
                            },
                            txtButton: "Payer",
                            color: primary_white,
                            backcolor: primary_blue,
                            boxDecoration: boxDecorationBtnCnx(
                              bgColor: primary_blue,
                            ),
                            sideColor: false,
                            BorderColor: primary_blue,
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          );
  }

  Widget _buildAdressWidget() {
    Size size = MediaQuery.of(context).size;

    return Container(child: SingleChildScrollView(
      child: Consumer(
        builder: (context, watch, child) {
          final viewModel = watch(addresseViewModel);
          print(viewModel.loadData);
          return Container(
            child: SingleChildScrollView(
              child: viewModel.adresses.length == 0 &&
                      viewModel.loadData != Status.loaded
                  ? LoadingList()
                  : viewModel.adresses.length == 0
                      ? Column(
                          children: [
                            Container(
                              width: size.width,
                              height: 48.0,
                              decoration: BoxDecoration(
                                color: primary_blue,
                                borderRadius: BorderRadius.circular(12),
                              ),
                              margin: EdgeInsets.only(
                                bottom: 20.0,
                                left: MediaQuery.of(context).size.width * 0.075,
                                right:
                                    MediaQuery.of(context).size.width * 0.075,
                              ),
                              child: TextButton(
                                  onPressed: () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                AddAdresseScreen()));
                                  },
                                  child: Text(
                                    "Ajouter une adresse",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontSize: 16.0,
                                      fontFamily: fontBold,
                                      color: primary_white,
                                    ),
                                  )),
                            ),
                          ],
                        )
                      :
                      /* viewModel.adresses.length == 0 &&
                      viewModel.loadData == Status.loading
                  ? LoadingList()
                  :*/
                      Column(
                          children: [
                            Column(
                              children: viewModel.adresses
                                  .map(
                                    (e) => ItemAdresseWidget(
                                      address: e,
                                      onSelect: (address) {
                                        setState(() {
                                          defaultAddress = address;
                                        });
                                      },
                                      defaultAddress: defaultAddress,
                                    ),
                                  )
                                  .toList(),
                            ),
                            Container(
                              width: size.width,
                              height: 48.0,
                              decoration: BoxDecoration(
                                color: primary_blue,
                                borderRadius: BorderRadius.circular(12),
                              ),
                              margin: EdgeInsets.only(
                                bottom: 20.0,
                                left: MediaQuery.of(context).size.width * 0.075,
                                right:
                                    MediaQuery.of(context).size.width * 0.075,
                              ),
                              child: TextButton(
                                  onPressed: () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                AddAdresseScreen()));
                                  },
                                  child: Text(
                                    "Ajouter une adresse",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontSize: 16.0,
                                      fontFamily: fontBold,
                                      color: primary_white,
                                    ),
                                  )),
                            ),
                          ],
                        ),
            ),
          );
        },
      ),
    ));
  }

  Widget _buildCardWidget() {
    return Container(
        child: SingleChildScrollView(
      child: Column(children: [
        ItemMethodPaymentMethod(
          icon: SvgPicture.asset("images/ic_cash.svg"),
          text: 'paiement à la livraison',
          checked: paimentType == 1 ? true : false,
          onTap: () {
            setState(() {
              paimentType = 1;
            });
          },
        ),
        SizedBox(
          height: 15,
        ),
        ItemMethodPaymentMethod(
          icon: SvgPicture.asset("images/ic_card.svg"),
          text: 'Carte de crédit',
          checked: paimentType == 2 ? true : false,
          onTap: () {
            setState(() {
              paimentType = 2;
            });
          },
        ),
      ]),
    ));
  }

  Widget _buildResume() {
    return Container(
      child: SingleChildScrollView(
        child: Consumer(
          builder: (context, watch, child) {
            final viewModel = watch(panierViewModel);
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                for (int i = 0; i < viewModel.products.length; i++)
                  ProductCard(
                    product: viewModel.products[i],
                  ),
                SizedBox(height: 10),
                Text("Adresse"),
                SizedBox(height: 10),
                ItemAdresseWidget(
                  address: defaultAddress ?? Address(id: 0, idCustomer: '0'),
                  onSelect: (address) {},
                ),
                SizedBox(height: 10),
                Text("Méthode de paiment"),
                SizedBox(height: 10),
                ItemMethodPaymentMethod(
                  icon: paimentType == 1
                      ? SvgPicture.asset("images/ic_card.svg")
                      : SvgPicture.asset("images/ic_cash.svg"),
                  text: paimentType == 2
                      ? 'Carte de crédit'
                      : 'paiement à la livraison',
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
