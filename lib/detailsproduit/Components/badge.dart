import 'package:applooki/assets/colors.dart';
import 'package:applooki/assets/fonts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class BadgeProduct extends StatelessWidget {
  final String icon;
  final String title;
  final String content;
  final double widthBadge;
  final double heightBadge;
  const BadgeProduct({
    Key? key,
    required this.icon,
    required this.title,
    required this.content,
    required this.widthBadge,
    this.heightBadge = 50,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final titleText = Text(
      title,
      style: TextStyle(
        fontSize: 11.0,
        fontFamily: fontRegular,
        color: background_blue,
      ),
    );

    final contentText = Text(
      content,
      style: TextStyle(
        fontSize: 12.0,
        fontFamily: fontRegular,
        color: txt_dark,
      ),
    );
    return Padding(
      padding: const EdgeInsets.only(left: 20),
      child: Container(
        height: heightBadge,
        width: widthBadge,
        child: Stack(
          alignment: Alignment.centerLeft,
          children: [
            Container(
              height: heightBadge,
              decoration: BoxDecoration(
                color: back_input_search,
                borderRadius: BorderRadius.circular(12),
              ),
              padding:
                  EdgeInsets.only(left: 20, top: 10, right: 10, bottom: 10),
              child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [if (title.isNotEmpty) titleText, contentText]),
            ),
            Transform.translate(
              offset: Offset(-15, 0),
              child: Container(
                decoration: BoxDecoration(
                    color: primary_blue,
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                height: 32.0,
                width: 32.0,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SvgPicture.asset(
                    icon,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SmallBadgeProduct extends StatelessWidget {
  final String icon;
  final String title;
  final String content;
  final double widthBadge;
  final double heightBadge;
  const SmallBadgeProduct({
    Key? key,
    required this.icon,
    required this.title,
    required this.content,
    required this.widthBadge,
    this.heightBadge = 25,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widthBadge,
      height: heightBadge,
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(left: 10),
            width: widthBadge - 10,
            decoration: BoxDecoration(
              color: back_input_search,
              borderRadius: BorderRadius.circular(12),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: title.isNotEmpty
                  ? [
                      Text(
                        title,
                        style: TextStyle(
                          fontSize: 7.0,
                          fontFamily: fontRegular,
                          color: background_blue,
                        ),
                      ),
                      Text(
                        content,
                        style: TextStyle(
                          fontSize: 7.0,
                          fontFamily: fontRegular,
                          color: txt_dark,
                        ),
                      ),
                    ]
                  : [
                      Text(
                        content,
                        style: TextStyle(
                          fontSize: 7.0,
                          fontFamily: fontRegular,
                          color: txt_dark,
                        ),
                      ),
                    ],
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              decoration: BoxDecoration(
                  color: primary_blue,
                  borderRadius: BorderRadius.all(Radius.circular(3))),
              height: 15.0,
              width: 15.0,
              child: Padding(
                padding: const EdgeInsets.all(3.0),
                child: SvgPicture.asset(icon),
              ),
            ),
          )
        ],
      ),
    );
  }
}
