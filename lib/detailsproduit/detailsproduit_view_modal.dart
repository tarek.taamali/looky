import 'dart:convert';
import 'package:applooki/Utils/local_storage.dart';
import 'package:applooki/Utils/xml_helpers.dart';
import 'package:applooki/assets/elements.dart';
import 'package:applooki/core/api_message.dart';
import 'package:applooki/core/utf_helper.dart';
import 'package:applooki/models/customers.dart';
import 'package:applooki/sellers/category.dart';
import 'package:applooki/sellers/reviews.dart';
import 'package:applooki/sellers/seller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';
import '../constant.dart';

class DetailsProduit extends ChangeNotifier {
  final url = BASE_URL + "kbsellers/";
  List<Review> reviews = [];
  bool notload = false;
  Seller seller = Seller(title: "", phone: "", businessEmail: "");
  Category category = Category(name: "");
  var idx;
  var date;
  List<dynamic> imageUrls = [];

  getSeller(String id) async {
    if (id.isEmpty) return;
    String path = url + "$id?output_format=JSON";
    print(path.toString());
    print(kheaders.toString());

    var response = await http.get(Uri.parse(path), headers: kheaders);
    if (response.statusCode == 200) {
      var result = json.decode(response.body);
      seller = SellerResponse.fromJson(result).seller;

      notifyListeners();
    }
  }

  Future<void> makePhoneCall() async {
    String sellerPhone = seller.phone;
    print("phone: $sellerPhone");
    if (sellerPhone.isEmpty) return;
    String phone = "tel:$sellerPhone";
    await launch(phone);
  }

  Future<void> SendEmail(String productName) async {
    String businessEmail = seller.businessEmail;
    if (businessEmail.isEmpty) return;
    String emailString =
        "mailto:$businessEmail?subject=$productName&body=Bonjour,";
    await launch(emailString);
  }

  getcategory(String id) async {
    String path = BASE_URL +
        "categories" +
        "?filter[id]=$id&output_format=JSON&display=full";
    var response = await http.get(Uri.parse(path), headers: kheaders);
    if (response.statusCode == 200) {
      var result = json.decode(response.body);
      if (result is Map<String, dynamic>)
        category = CategoryResponse.fromJson(result).category;
      notifyListeners();
    }
  }

  getReviews(String idSeller) async {
    String path = BASE_URL +
        "kbsellerreviews?output_format=JSON&display=full&filter[id_seller]=$idSeller";
    var response = await http.get(Uri.parse(path), headers: kheaders);
    if (response.statusCode == 200) {
      var result = json.decode(response.body);
      if (response.body == "[]") return;
      reviews = ReviewResponse.fromJson(result).reviews;
      reviews = reviews.where((element) => element.approved == 1).toList();
      print("reviews");
      notifyListeners();
    }
  }

  sendReviewSeller({
    required BuildContext context,
    required String idSeller,
    required String titleProduct,
    required String comment,
  }) async {
    Map<String, dynamic> jsondatais =
        jsonDecode(StorageUtil.getString('customer'));
    var customerId = Customer.fromJson(jsondatais).id;
    Loader.show(
      context,
      progressIndicator: LoaderWidget(context),
    );
    String path = BASE_URL + "kbsellerreviews?output_format=JSON";
    try {
      var response = await http.post(Uri.parse(path),
          headers: kheaders,
          body: buildReviewXml(
            idSeller: idSeller,
            comment: comment,
            idCustomer: customerId,
            titleProduct: titleProduct,
          ));
      Loader.hide();
      print(response.body);
      if (response.statusCode == 201) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Votre commentaire est en attente d'approbation"),
        ));
      } else {
        var errorMessage = "Il semble qu'il a y eu un problème !";
        Map<String, dynamic> errors = json.decode(response.body);
        if (errors.containsKey("errors")) {
          var errorResponse = ErrorResponse.fromJson(errors);
          errorMessage = errorResponse.errors.length > 0
              ? toDecodedString(errorResponse.errors.first.message)
              : "Il semble qu'il a y eu un problème !";
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(errorMessage),
          ));
        }
      }
      print(response.statusCode.toString());
      print("response \n : ${response.body}");
    } catch (error) {
      print("response :\n $error}");
    }
  }
}
