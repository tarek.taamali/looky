import 'dart:convert';
import 'dart:ui';
import 'package:applooki/Utils/utils.dart';
import 'package:applooki/constant.dart';
import 'package:applooki/home/products_response.dart';
import 'package:applooki/detailsproduit/detailsproduit_view_modal.dart';
import 'package:applooki/assets/fonts.dart';
import 'package:applooki/panier/panier_screen.dart';
import 'package:applooki/screens/image_preview_screen.dart';
import 'package:applooki/sellers/reviews.dart';
import 'package:applooki/services/top_level_providers.dart';
import 'package:applooki/shared/top_bar_widget.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:applooki/assets/colors.dart';
import 'package:applooki/assets/images_links.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';

import 'Components/badge.dart';

final detailsProduitViewModel =
    ChangeNotifierProvider.autoDispose<DetailsProduit>(
  (ref) => DetailsProduit(),
);

class DetailsProduitScreen extends StatefulWidget {
  final Product product;
  const DetailsProduitScreen({Key? key, required this.product})
      : super(key: key);
  @override
  _DetailsProduitScreenState createState() => _DetailsProduitScreenState();
}

class _DetailsProduitScreenState extends State<DetailsProduitScreen> {
  int indexPage = 0;
  int currentPos = 0;
  var inputFormat = DateFormat('dd-MM-yyyy');
  var reviewController = TextEditingController();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      final viewModel = context.read(detailsProduitViewModel);
      viewModel.getSeller(widget.product.idSeller);
      viewModel.getcategory(widget.product.idCategoryDefault.toString());
      viewModel.getReviews(widget.product.idSeller);
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: primary_white,
      bottomNavigationBar: _buildBottomBar(size),
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.symmetric(horizontal: size.width * 0.075),
          child: Consumer(
            builder: (context, watch, child) {
              return ListView(
                children: [
                  TopBarWidget(
                    title: "Détails",
                    actionIcon: Consumer(builder: (context, watch, child) {
                      final viewModel = watch(favorisViewModel);
                      return GestureDetector(
                        onTap: () {
                          viewModel.addOrRemoveProductToFavoris(
                              context, widget.product);
                        },
                        child: Icon(
                          viewModel.isFavoris(widget.product)
                              ? Icons.favorite
                              : Icons.favorite_border,
                          color: red_favoris,
                        ),
                      );
                    }),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 20),
                    width: size.width * 0.8,
                    child: widget.product.images.length == 0
                        ? Image.asset(
                            'images/no-image.png',
                            fit: BoxFit.fitWidth,
                          )
                        : CarouselSlider.builder(
                            itemCount: widget.product.images.length,
                            options: CarouselOptions(
                                enableInfiniteScroll: false,
                                aspectRatio: 16 / 9,
                                viewportFraction: 1,
                                height: 300),
                            itemBuilder: (context, i, id) {
                              return InkWell(
                                child: Container(
                                  width: size.width * 0.8,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    border: Border.all(
                                      color: Colors.white,
                                    ),
                                  ),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(15),
                                    child: Image.network(
                                      widget.product.images[i],
                                      headers: kheaders,
                                      width: MediaQuery.of(context).size.width,
                                      fit: BoxFit.fitHeight,
                                      errorBuilder: (BuildContext context,
                                          Object exception,
                                          StackTrace? stackTrace) {
                                        return Image.asset(
                                          'images/no-image.png',
                                          fit: BoxFit.fitWidth,
                                        );
                                      },
                                    ),
                                  ),
                                ),
                                onTap: () {
                                  Navigator.push(context, MaterialPageRoute(
                                      builder: (BuildContext context) {
                                    return ImagePreviewScreen(
                                      url: widget.product.images[i],
                                      headers: kheaders,
                                    );
                                  }));
                                },
                              );
                            },
                          ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Text(
                          widget.product.name,
                          style: TextStyle(
                            fontSize: 18.0,
                            fontFamily: fontBold,
                            color: blue_article_name,
                          ),
                        ),
                      ),
                      Container(
                        width: 65,
                        child: Text(
                          convertToStringPrice(widget.product.reducedPrice),
                          style: TextStyle(
                            fontSize: 18.0,
                            fontFamily: fontBold,
                            color: primary_blue,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Consumer(builder: (context, watch, child) {
                        final viewModelSeller = watch(detailsProduitViewModel);
                        return Text(
                          'Publier par ${viewModelSeller.seller.title}',
                          style: TextStyle(
                            fontSize: 13.0,
                            fontFamily: fontRegular,
                            color: grey_descrptioProd,
                          ),
                        );
                      }),
                    ],
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Wrap(
                    spacing: 10.0,
                    runSpacing: 10,
                    children: [
                      BadgeProduct(
                        title: "Date de dépôt",
                        content: inputFormat
                            .format(widget.product.dateAdd)
                            .toString(),
                        icon: iconeDateDepot,
                        widthBadge: 128,
                      ),
                      Consumer(builder: (context, watch, child) {
                        final viewModelSeller = watch(detailsProduitViewModel);
                        return BadgeProduct(
                          title: "",
                          content: viewModelSeller.category.name,
                          icon: iconeowner,
                          widthBadge: 128,
                        );
                      }),
                      Visibility(
                        visible: widget.product.productsOptions.length > 0 &&
                            widget.product.productsOptions
                                    .where((element) =>
                                        element.parent == 'Couleur')
                                    .length >
                                0,
                        child: BadgeProduct(
                          title: 'Couleur',
                          icon: iconeColor,
                          content: widget.product.productsOptions.length > 0
                              ? widget.product.productsOptions
                                  .firstWhere(
                                      (element) => element.parent == 'Couleur',
                                      orElse: () => ProductOptionValue())
                                  .name
                              : '',
                          widthBadge: 98,
                        ),
                      ),
                      Visibility(
                          visible: widget.product.productsOptions.length > 0 &&
                              widget.product.productsOptions
                                      .where((element) =>
                                          element.parent == 'Taille')
                                      .length >
                                  0,
                          child: BadgeProduct(
                            title: 'Taille',
                            icon: iconeTaille,
                            content: widget.product.productsOptions.length > 0
                                ? widget.product.productsOptions
                                            .where((element) =>
                                                element.parent == 'Taille')
                                            .length >
                                        0
                                    ? widget.product.productsOptions
                                        .where((element) =>
                                            element.parent == 'Taille')
                                        .first
                                        .name
                                    : ''
                                : '',
                            widthBadge: 98,
                          )),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: size.width * 0.85,
                        margin: EdgeInsets.only(
                          bottom: 0,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            InkWell(
                              onTap: () {
                                setState(() => indexPage = 0);
                              },
                              child: Container(
                                width: size.width * 0.33,
                                padding: EdgeInsets.only(bottom: 10, top: 10),
                                decoration: BoxDecoration(
                                  border: Border(
                                    bottom: BorderSide(
                                        color: indexPage == 0
                                            ? primary_blue
                                            : Colors.transparent,
                                        width: 5.0),
                                  ),
                                ),
                                child: Text(
                                  "Description",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 15.0,
                                    fontFamily: fontRegular,
                                    color: indexPage == 0
                                        ? primary_blue
                                        : inactive_tab,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Visibility(
                              visible: true,
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    indexPage = 1;
                                  });
                                },
                                child: Container(
                                  width: size.width * 0.33,
                                  padding: EdgeInsets.only(bottom: 10, top: 10),
                                  decoration: BoxDecoration(
                                    border: Border(
                                      bottom: BorderSide(
                                          color: indexPage == 1
                                              ? primary_blue
                                              : Colors.transparent,
                                          width: 5.0),
                                    ),
                                  ),
                                  child: Text(
                                    "Commentaires",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontSize: 15.0,
                                      fontFamily: fontRegular,
                                      color: indexPage == 1
                                          ? primary_blue
                                          : inactive_tab,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      if (indexPage == 0)
                        Html(
                          data: utf8.decode(
                              widget.product.descriptionShort.runes.toList()),
                        )
                      else
                        Container(
                            height: 200,
                            child: Consumer(builder: (context, watch, child) {
                              final viewModeReviews =
                                  watch(detailsProduitViewModel);
                              return ListView(
                                children: [
                                  Container(
                                    margin:
                                        EdgeInsets.only(top: 10, bottom: 10),
                                    height: 60,
                                    decoration: BoxDecoration(
                                        color: Color(0XffF2F2F2),
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                        border: Border.all(
                                          color: primary_blue,
                                          width: 2,
                                        )),
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          left: 10, right: 5),
                                      child: Row(
                                        children: [
                                          Container(
                                            height: 45,
                                            width: 45,
                                            margin: EdgeInsets.all(5),
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                shape: BoxShape.circle),
                                            child: ClipOval(
                                              child: SizedBox.fromSize(
                                                size: Size.fromRadius(48),
                                                child: SvgPicture.asset(
                                                  "images/ic_looki.svg",
                                                ),
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            flex: 3,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                  left: 10, right: 10),
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    child: TextField(
                                                      controller:
                                                          reviewController,
                                                      decoration:
                                                          InputDecoration(
                                                        enabledBorder:
                                                            InputBorder.none,
                                                        border:
                                                            InputBorder.none,
                                                        hintText:
                                                            "Ajouter un commentaire…",
                                                      ),
                                                    ),
                                                  ),
                                                  GestureDetector(
                                                    onTap: () async {
                                                      await viewModeReviews
                                                          .sendReviewSeller(
                                                        context: context,
                                                        titleProduct:
                                                            widget.product.name,
                                                        comment:
                                                            reviewController
                                                                .text,
                                                        idSeller: widget
                                                            .product.idSeller,
                                                      );
                                                      reviewController.text =
                                                          "";
                                                    },
                                                    child: Container(
                                                        height: 35,
                                                        width: 35,
                                                        decoration:
                                                            BoxDecoration(
                                                          color: primary_blue,
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                            5.0,
                                                          ),
                                                        ),
                                                        child: Center(
                                                          child: Icon(
                                                            Icons.send,
                                                            color: Colors.white,
                                                          ),
                                                        )),
                                                  )
                                                ],
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  ...viewModeReviews.reviews
                                      .map((e) => _buildComment(e))
                                      .toList()
                                ],
                              );
                            })),
                      SizedBox(
                        height: 25.0,
                      ),
                    ],
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  Container _buildComment(Review review) {
    final globalModel = context.read(menuViewModel);
    return Container(
      margin: EdgeInsets.only(top: 10, bottom: 10),
      constraints: BoxConstraints(
        minHeight: 50,
      ),
      decoration: BoxDecoration(
        color: background_article_grey,
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Row(
        children: [
          Container(
            height: 60,
            width: 60,
            margin: EdgeInsets.all(15),
            decoration:
                BoxDecoration(color: Colors.white, shape: BoxShape.circle),
            child: ClipOval(
              child: SizedBox.fromSize(
                size: Size.fromRadius(48),
                child: SvgPicture.asset(
                  'images/ic_looki.svg',
                ),
              ),
            ),
          ),
          Expanded(
              flex: 3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 5,
                  ),
                  Text(globalModel
                      .getCustomerById(review.idCcustomer.toString())),
                  SizedBox(
                    height: 5,
                  ),
                  Text("${review.comment}"),
                  SizedBox(
                    height: 5,
                  ),
                ],
              ))
        ],
      ),
    );
  }

  BottomAppBar _buildBottomBar(Size size) {
    return BottomAppBar(
      elevation: 0.0,
      child: Container(
        width: size.width * 0.85,
        padding: EdgeInsets.symmetric(horizontal: size.width * 0.075),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            GestureDetector(
              onTap: () {
                /* final panierView = context.read(panierViewModel);
               panierView.addProduct(context, widget.product);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => PanierScreen()));
                    */
                final viewModel = context.read(detailsProduitViewModel);
                // panierView.addProduct(context, widget.product);
                viewModel.SendEmail(widget.product.name);
              },
              child: Container(
                width: 48.0,
                height: 48.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  border: Border.all(color: primary_blue, width: 2.0),
                ),
                child: Icon(
                  Icons.email,
                  color: primary_blue,
                  size: 22.0,
                ),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Container(
                height: 57.0,
                padding: EdgeInsets.symmetric(vertical: 5),
                child: InkWell(
                  child: TextButton(
                    onPressed: () {
                      // final panierView = context.read(panierViewModel);
                      final viewModel = context.read(detailsProduitViewModel);
                      // panierView.addProduct(context, widget.product);
                      viewModel.makePhoneCall();
                    },
                    style: TextButton.styleFrom(
                      backgroundColor: primary_blue,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                    ),
                    child: const Text(
                      "Appeler",
                      style: TextStyle(
                        fontSize: 16.0,
                        fontFamily: fontBold,
                        color: primary_white,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
