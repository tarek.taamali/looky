import 'package:applooki/assets/colors.dart';
import 'package:applooki/assets/custom_button.dart';
import 'package:applooki/assets/elements.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SucessPaimentScrenn extends StatefulWidget {
  final int nbreOrder;
  const SucessPaimentScrenn({Key? key, required this.nbreOrder})
      : super(key: key);

  @override
  _SuccessAddProductScrennState createState() =>
      _SuccessAddProductScrennState();
}

class _SuccessAddProductScrennState extends State<SucessPaimentScrenn> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Container(
            height: size.height,
            child: Column(
              children: [
                Expanded(
                    flex: 7,
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Center(
                            child:
                                SvgPicture.asset("images/ic_success_saved.svg"),
                          ),
                          SizedBox(
                            height: 40,
                          ),
                          Text(
                            "Votre commande ${widget.nbreOrder} a été enregistrer avec succés.",
                            style: TextStyle(fontSize: 16),
                          )
                        ])),
                Container(
                  height: size.height * 0.12,
                  width: size.width,
                  padding: EdgeInsets.all(5),
                  child: Container(
                    width: size.width * 0.85,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        CustomButton(
                          onPressed: () {
                            Navigator.pushNamedAndRemoveUntil(
                                context, 'base', (route) => false);
                          },
                          txtButton: "Aller vers l'acceuil",
                          color: primary_white,
                          backcolor: primary_blue,
                          boxDecoration: boxDecorationBtnCnx(
                            bgColor: primary_blue,
                          ),
                          sideColor: false,
                          BorderColor: primary_blue,
                        ),
                      ],
                    ),
                  ),
                )
              ],
            )),
      ),
    );
  }
}
