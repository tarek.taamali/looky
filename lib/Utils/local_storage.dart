import 'package:shared_preferences/shared_preferences.dart';

class StorageUtil {
  static late SharedPreferences _preferences;
         

  static Future<SharedPreferences> init() async {
    _preferences = await SharedPreferences.getInstance();
    return _preferences;
  }
  
  StorageUtil._();

   static bool containsKey(String key, {String defValue = ''}) {
    return _preferences.containsKey(key);
  }

  static String getString(String key, {String defValue = ''}) {
    return _preferences.getString(key) ?? defValue;
  }

  static getData(String key, {String defValue = ''}) {
    return _preferences.getString(key);
  }

  static Future putString(String key, String value) {
    return _preferences.setString(key, value);
  }

  static int getInt(String key, {int defValue = 0}) {
    return _preferences.getInt(key) ?? defValue;
  }

  static Future putInt(String key, int value) {
    return _preferences.setInt(key, value);
  }

  static double getDouble(String key, {double defValue = 0.0}) {
    return _preferences.getDouble(key) ?? defValue;
  }

  static Future putDouble(String key, double value) {
    return _preferences.setDouble(key, value);
  }
}
