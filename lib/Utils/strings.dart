const firstNameLabel = "Prénom";
const lastNameLabel = "Nom";
const passwordLabel = "Mot de passe";
const emailLabel = "Adresse e-mail";
const mobileLabel = "Numéro du téléphone";
const signupLabel = "S'inscrire";
const requiredFieldLabel = "Champs obligatoire";
const emailNotValidLabel = "Veuillez entrer une adresse e-mail valide";
