import 'dart:convert';

import 'package:crypto/crypto.dart';

import 'constant.dart';

enum Status {
  none,
  loading,
  loaded,
}

String? isNotEmptyField(String? value) {
  if (value!.isEmpty) {
    return '*Champs obligatoire';
  }
  return null;
}

String? isEmailField(String? value) {
  if (value!.isEmpty) {
    return '*Champs obligatoire';
  }
  return null;
}

bool isValidEmail(_email) {
  return RegExp(
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
      .hasMatch(_email);
}

String convertToStringPrice(String price) {
  return removeTrailingZeros(
          double.parse(price).toStringAsFixed(2).toString()) +
      ' MAD';
}

removeTrailingZeros(String n) {
  return n.replaceAll(RegExp(r"([.]*0+)(?!.*\d)"), "");
}

String hashPass(String password) {
  return md5.convert(utf8.encode(Constant.cookieKey + password)).toString();
}

String toUpperCase(String category) {
  return category[0].toUpperCase() + category.substring(1, category.length);
}
