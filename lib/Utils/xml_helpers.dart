import 'dart:convert';

import 'package:applooki/Utils/local_storage.dart';
import 'package:applooki/add_product/product_created.dart';
import 'package:applooki/home/products_response.dart';
import 'package:applooki/models/customers.dart';
import 'package:applooki/models/products_marque.dart';
import 'package:crypto/crypto.dart';

String buildCreatProduct({
  required double price,
  required String description,
  required String link,
  required String title,
  required String categoryText,
  required String defaultCat,
  Manufacturer? manufacturerProduct,
}) {
  return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
      "<prestashop xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n" +
      "<product>\n" +
      "<id_category_default>$defaultCat</id_category_default>\n" +
      "<id_manufacturer>${manufacturerProduct!.id}</id_manufacturer>\n" +
      "<advanced_stock_management>1</advanced_stock_management>\n" +
      " <reference>boom</reference>\n" +
      " <name>\n" +
      "     <language id=\"2\">abc</language>\n" +
      "  </name>\n" +
      "  <price>$price</price>\n" +
      " <show_price>1</show_price>\n" +
      "  <type>simple</type>\n" +
      "  <description>$description</description>\n" +
      "  <description_short>$description</description_short>\n" +
      "  <condition>new</condition>\n" +
      "  <id_shop_default>1</id_shop_default>\n" +
      " <active>0</active>\n" +
      "  <link_rewrite>$link</link_rewrite>\n" +
      "  <name>$title</name>\n" +
      "        <associations>\n" +
      "            <categories>\n" +
      "                $categoryText" +
      "            </categories>\n" +
      "</associations>\n" +
      "</product>\n" +
      "</prestashop>\n";

  /*
  
   "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
      "<prestashop xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n" +
      "    <product>\n" +
      "        <id_category_default>$defaultCat</id_category_default>\n" +
      "        <id_manufacturer>${manufacturerProduct!.id}</id_manufacturer> \n" +
      "        <price>$price</price>\n" +
      "   <advanced_stock_management>1</advanced_stock_management>\n" +
      "<reference>boom</reference>\n" +
      "        <show_price>1</show_price>\n" +
      "        <type>simple</type>\n" +
      "        <description>$description</description>\n" +
      "        <description_short>$description</description_short>\n" +
      "        <condition>new</condition>\n" +
      "        <id_shop_default>1</id_shop_default>\n" +
      "        <state>1</state>\n" +
      "        <active>0</active>\n" +
      "        <link_rewrite>$link</link_rewrite>\n" +
      "        <name>$title</name>\n" +
      "<name>\n" +
      "      <language id=\"1.0\">abc</language>\n" +
      "  </name>\n" +
      "        <associations>\n" +
      "            <categories>\n" +
      "                $categoryText" +
      "            </categories>\n" +
      "</associations>\n" +
      " </product>\n" +
      "</prestashop>";
      */
}

String buildCreatedCustomerXml({
  required String password,
  required String lastName,
  required String firstName,
  required String email,
}) {
  return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
      "<prestashop xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n" +
      "    <customer>\n" +
      "        <passwd>$password</passwd>\n" +
      "        <pass>$password</pass>\n" +
      "        <lastname>$lastName</lastname>\n" +
      "        <firstname>$firstName</firstname>\n" +
      "        <email>$email</email>\n" +
      "        <id_shop>1</id_shop>\n" +
      "        <active>1</active>\n" +
      "        <id_shop_group>1</id_shop_group>\n" +
      "        <id_gender>0</id_gender>\n" +
      "        <id_default_group>1</id_default_group>\n" +
      "        <id_lang>2</id_lang>\n" +
      "        <secure_key>${md5.convert(utf8.encode(firstName + lastName)).toString()}</secure_key>\n" +
      "    </customer>\n" +
      "</prestashop>";
}

String buildCreateSellerXml({
  required String idCustomer,
  required String title,
  required int idDefaultLang,
  required int idCountry,
  required int idShop,
  required int approved,
  required int active,
  required String phoneNumber,
  required String businessEmail,
  required int notificationType,
  required String address,
}) {
  return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
      "<prestashop xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n" +
      "    <seller>\n" +
      "          <title>$title</title>\n" +
      "        <id_customer format=\"isNullOrUnsignedId\">$idCustomer</id_customer>\n" +
      "        <id_default_lang format=\"isNullOrUnsignedId\">2</id_default_lang>\n" +
      "        <id_country format=\"isNullOrUnsignedId\">$idCountry</id_country>\n" +
      "        <id_shop format=\"isNullOrUnsignedId\">$idShop</id_shop>\n" +
      "        <approved format=\"isInt\">$approved</approved>\n" +
      "        <active format=\"isBool\">$active</active>\n" +
      "        <business_email maxSize=\"128\" format=\"isEmail\">$businessEmail</business_email>\n" +
      "        <notification_type format=\"isString\">$notificationType</notification_type>\n" +
      "       <address format=\"isAddress\">lyon</address>" +
      "      <phone_number format=\"isPhoneNumber\">12341234</phone_number>" +
      "    </seller>\n" +
      "</prestashop>";
}

String buildAffectedSeller(
    {required String idSeller, required String idProduct}) {
  return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
      "<prestashop xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n" +
      "    <kbsellerproduct>\n" +
      "        <id></id>\n" +
      "        <id_seller>$idSeller</id_seller>\n" +
      "        <id_product>$idProduct</id_product>\n" +
      "        <id_shop>1</id_shop>\n" +
      "        <approved>0</approved>\n" +
      "        <deleted>0</deleted>\n" +
      "        <date_add></date_add>\n" +
      "        <date_upd></date_upd>\n" +
      "    </kbsellerproduct>\n" +
      "</prestashop>";
}

String buildCreatedCombnaison(
    {required String idProduct, required String idOp1, required String idOp2}) {
  return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
      "<prestashop xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n" +
      "    <product>\n" +
      "        <id_product>$idProduct</id_product>\n" +
      "        <minimal_quantity>1</minimal_quantity>\n" +
      "      <associations>" +
      " <product_option_values nodeType=\"product_option_value\" api=\"product_option_values\">\n" +
      " <product_option_value xlink:href=\"https://looki.ma/api/product_option_values/2\">\n" +
      "        <id>$idOp1</id>\n" +
      "  </product_option_value>\n" +
      " <product_option_value xlink:href=\"https://looki.ma/api/product_option_values/11\">\n" +
      " <id> $idOp2</id>\n" +
      " </product_option_value>\n" +
      "</product_option_values>\n" +
      " </associations>\n" +
      "    </product>\n" +
      "</prestashop>";
}

String buildCategoryUpdated({
  required ProductSaved productCreated,
  required String idCombinaison,
  required String categoryText,
  required int id,
}) {
  return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
      "<prestashop xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n" +
      "    <product>\n" +
      " <id>${productCreated.id}</id>\n" +
      " <cache_default_attribute>$idCombinaison</cache_default_attribute>\n" +
      "   <show_price>1</show_price>\n" +
      "        <link_rewrite>${productCreated.link}</link_rewrite>\n" +
      "        <name>${productCreated.name}</name>\n" +
      "        <description>${productCreated.description}</description>\n" +
      "<description_short>${productCreated.description}</description_short>\n" +
      " <id_category_default>${id.toString()}</id_category_default>\n" +
      " <price>${productCreated.price}</price>\n" +
      " <state>1</state>" +
      "        <associations>\n" +
      "            <categories>\n" +
      "                $categoryText" +
      "            </categories>\n" +
      "<combinations nodeType=\"combination\" api=\"combinations\">\n" +
      "<combination xlink:href=\"https://looki.ma/api/combinations/$idCombinaison\">\n" +
      "<id>$idCombinaison</id>\n" +
      "</combination>\n" +
      "</combinations>\n" +
      "        </associations>\n" +
      " <active>0</active>\n" +
      "    </product>\n" +
      "</prestashop>";
}

Future<String> buildCreateOrder({
  required List<Product> products,
  required String idCustomer,
  required String idAddress,
}) async {
  Map<String, dynamic> cashedCustomer =
      jsonDecode(StorageUtil.getString('customer'));
  String cartId = StorageUtil.getString("cartId");
  String idCustomer = Customer.fromJson(cashedCustomer).id;
  String price = await calculatePriceFromProducts(products);
  String listProductsXml = rowOrder(products);
  return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
      "<prestashop xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n" +
      "<order>\n"
          "<id_address_delivery required=\"true\" format=\"isUnsignedId\">$idAddress</id_address_delivery>\n" +
      "<id_address_invoice required=\"true\" format=\"isUnsignedId\">$idAddress</id_address_invoice>\n" +
      "<id_cart required=\"true\" format=\"isUnsignedId\">$cartId</id_cart>\n" +
      "<id_currency required=\"true\" format=\"isUnsignedId\">2</id_currency>\n" +
      "<id_lang required=\"true\" format=\"isUnsignedId\">2</id_lang>\n" +
      "<id_customer required=\"true\" format=\"isUnsignedId\">$idCustomer</id_customer>\n" +
      "<id_carrier required=\"true\" format=\"isUnsignedId\">1</id_carrier>\n" +
      "<payment required=\"true\" format=\"isGenericName\">Transfert bancaire</payment>\n" +
      "<module required=\"true\" format=\"isModuleName\">ps_wirepayment</module>\n" +
      "<total_paid_real required=\"true\" format=\"isPrice\">$price</total_paid_real>\n" +
      "<total_products required=\"true\" format=\"isPrice\">$price</total_products>\n" +
      "<total_products_wt required=\"true\" format=\"isPrice\">$price</total_products_wt>\n" +
      "<total_paid required=\"true\" format=\"isPrice\">0</total_paid>\n" +
      "<id_shop_group format=\"isUnsignedId\">1</id_shop_group>\n" +
      "<id_shop format=\"isUnsignedId\">1</id_shop>\n" +
      "<valid>1</valid>\n" +
      "<conversion_rate required=\"true\" format=\"isFloat\">1.000000</conversion_rate>\n" +
      "<associations>\n" +
      "<order_rows nodeType=\"order_row\" virtualEntity=\"true\">\n" +
      listProductsXml +
      "</order_rows>\n" +
      "</associations>\n" +
      "</order>\n" +
      "</prestashop>";
}

String rowOrder(List<Product> products) {
  String cartsXml = "";
  products.forEach((pr) {
    cartsXml += " <order_row>\n" +
        " <product_id xlink:href=\"https://looki.ma/api/products/${pr.id}\" required=\"true\">\n" +
        pr.id.toString() +
        "</product_id>\n" +
        "<product_quantity>1</product_quantity>\n" +
        "</order_row>\n";
  });
  return cartsXml;
}

String buildCreateOrderDetails({
  required List<Product> products,
}) {
  return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
      "<prestashop xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n" +
      " <order_detail>\n" +
      "<id_order required=\"true\" format=\"isUnsignedId\">24</id_order>\n" +
      "<product_id format=\"isUnsignedId\">${products.first.id}</product_id>\n" +
      "<product_attribute_id format=\"isUnsignedId\"></product_attribute_id>\n" +
      "<product_quantity_reinjected format=\"isUnsignedInt\"></product_quantity_reinjected>\n" +
      "<group_reduction format=\"isFloat\"></group_reduction>\n" +
      "<discount_quantity_applied format=\"isInt\"></discount_quantity_applied>\n" +
      "<id_warehouse required=\"true\" format=\"isUnsignedId\">0</id_warehouse>\n" +
      "<id_shop required=\"true\" format=\"isUnsignedId\">1</id_shop>\n" +
      "<product_name required=\"true\" format=\"isGenericName\">${products.first.name}</product_name>\n" +
      "<product_quantity required=\"true\" format=\"isInt\">1</product_quantity>\n" +
      "<product_price required=\"true\" format=\"isPrice\">10.2</product_price>\n" +
      "</order_detail>\n" +
      "</prestashop>";
}

String buildListRow(List<Product> products) {
  String cartsXml = "";
  products.forEach((element) {
    String combinaison = element.combinations.length > 0
        ? "<id_product_attribute xlink:href=\"https://looki.ma/api/combinations/${element.combinations.first}\">\n" +
            element.combinations.first +
            " </id_product_attribute>\n"
        : "";

    cartsXml += "<cart_row>\n" +
        "         <id_product xlink:href=\"https://looki.ma/api/products/ ${element.id}\">\n" +
        "             ${element.id}"
            "          </id_product>\n" +
        combinaison +
        "          <quantity>1</quantity>\n" +
        "          <id_address_delivery>8</id_address_delivery>\n" +
        "</cart_row>\n";
  });
  return cartsXml;
}

String buildCreateCart(
    {required List<Product> products,
    required String idCustomer,
    required String idAddress}) {
  String listProductsXml = buildListRow(products);

  return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
      "<prestashop xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n" +
      "  <cart>\n" +
      "    <id_currency>2</id_currency>\n" +
      "     <id_customer>$idCustomer</id_customer>\n" +
      "      <id_lang>2</id_lang>\n" +
      "      <delivery_option></delivery_option>\n" +
      "      <secure_key></secure_key>\n" +
      "    <id_address_delivery>$idAddress</id_address_delivery>" +
      "  <id_address_invoice>$idAddress</id_address_invoice>"
          "      <associations>\n" +
      "         <cart_rows nodeType=\"cart_row\" virtualEntity=\"true\">\n" +
      listProductsXml +
      "           </cart_rows>\n" +
      "       </associations>\n" +
      "   </cart>\n" +
      "</prestashop>";
}

Future<String> calculatePriceFromProducts(List<Product> products) async {
  double price = 0.0;
  await Future.forEach(
      products, (Product pr) => price += double.parse(pr.reducedPrice));
  return price.toString();
}

String buildReviewXml({
  required String idSeller,
  required String titleProduct,
  required String idCustomer,
  required String comment,
}) {
  return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
      "<prestashop xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n" +
      "    <kbsellerreview>\n" +
      "        <id_seller>$idSeller</id_seller>\n" +
      "        <id_customer>$idCustomer</id_customer>\n" +
      "        <title>$titleProduct</title>\n" +
      "        <comment>$comment</comment>\n" +
      "        <id_shop>1</id_shop>\n" +
      "        <approved>0</approved>\n" +
      "        <rating>0</rating>\n" +
      "        <id_lang>2</id_lang>\n" +
      "    </kbsellerreview>\n" +
      "</prestashop>";
}
