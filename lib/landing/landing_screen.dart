import 'package:animator/animator.dart';
import 'package:applooki/Utils/local_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LandingPageScreen extends StatefulWidget {
  @override
  _LandingPageScreenState createState() => _LandingPageScreenState();
}

class _LandingPageScreenState extends State<LandingPageScreen> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      String connected = StorageUtil.getString("connected");
      connected == "connected" ? goto('base', context) : goto('splashScreen', context);
    });
  }

  goto(String page, BuildContext context) {
    new Future.delayed(const Duration(milliseconds: 1500), () {
      Navigator.pushNamedAndRemoveUntil(context, page, (route) => false);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Animator<double>(
              duration: Duration(seconds: 1),
              cycles: 1,
              triggerOnInit: true,
              builder: (_, animationState, __) => SizedBox(
                width: 150 * animationState.value,
                height: 150 * animationState.value,
                child: SvgPicture.asset(
                  "images/ic_looki.svg",
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
