List<FiltreOption> listOptions = [
  FiltreOption(id: 0, name: 'Tous'),
  FiltreOption(id: 0, name: 'Les plus récents', label: 'id_DESC'),
  //FiltreOption(id: 0, name: 'Les plus pertinents', label: ''),
  FiltreOption(id: 0, name: 'Les mois bas', label: 'price_ASC'),
  FiltreOption(id: 0, name: 'Les plus cher', label: 'price_DESC'),
];

List<FiltreOption> listPrice = [
  FiltreOption(id: 0, name: 'Peu couteux'),
  FiltreOption(id: 0, name: 'Modéré', label: 'id_DESC'),
  //FiltreOption(id: 0, name: 'Les plus pertinents', label: ''),
  FiltreOption(id: 0, name: 'Cher', label: 'price_ASC'),
  FiltreOption(id: 0, name: 'Trés cher', label: 'price_DESC'),
];

class FiltreOption {
  FiltreOption({
    required this.id,
    required this.name,
    this.label,
  });

  int id;
  String name;
  String? label;
}
