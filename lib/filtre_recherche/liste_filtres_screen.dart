import 'package:flutter/material.dart';

import 'package:applooki/assets/colors.dart';
import 'package:applooki/assets/custom_button.dart';
import 'package:applooki/assets/elements.dart';

import '../assets/fonts.dart';
import 'filtre_options_list.dart';

class ListeFiltreScreen extends StatefulWidget {
  final Function(FiltreOption) onSelect;
  final FiltreOption? filtreOption;
  const ListeFiltreScreen({
    Key? key,
    required this.onSelect,
    this.filtreOption,
  }) : super(key: key);
  @override
  _ListeFiltreScreenState createState() => _ListeFiltreScreenState();
}

class _ListeFiltreScreenState extends State<ListeFiltreScreen> {
  late FiltreOption filtreOption;

  @override
  void initState() {
    super.initState();
    filtreOption = widget.filtreOption ?? listOptions.first;
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: primary_white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: primary_white,
        title: Center(
          child: Container(
            width: size.width * 0.85,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: 35.0,
                  height: 35.0,
                  decoration: BoxDecoration(
                    color: back_input_search,
                    borderRadius: BorderRadius.circular(9.0),
                  ),
                  child: Center(
                    child: IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: primary_blue,
                        size: 14,
                      ),
                      onPressed: () => Navigator.pop(context),
                    ),
                  ),
                ),
                Text(
                  'Trier par',
                  style: TextStyle(
                    fontSize: 15.0,
                    color: primary_blue,
                    fontFamily: fontBold,
                  ),
                ),
                Text(
                  '',
                ),
              ],
            ),
          ),
        ),
        centerTitle: true,
      ),
      body: Container(
        height: size.height,
        width: size.width,
        padding: EdgeInsets.only(top: size.height * 0.02),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              flex: 4,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: listOptions.map((e) => _buildRow(e)).toList(),
              ),
            ),
            Expanded(
              flex: 1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CustomButton(
                    onPressed: () {
                      widget.onSelect(filtreOption);
                      Navigator.of(context).pop();
                    },
                    txtButton: "Enregistrer",
                    color: primary_white,
                    backcolor: primary_blue,
                    boxDecoration: boxDecorationBtnCnx(
                      bgColor: primary_blue,
                    ),
                    sideColor: false,
                    BorderColor: primary_blue,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildRow(FiltreOption itemFiltre) {
    final size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        setState(() {
          filtreOption = itemFiltre;
        });
      },
      child: Container(
        width: size.width * 0.85,
        padding: EdgeInsets.symmetric(horizontal: 15.0),
        margin: EdgeInsets.only(bottom: size.height * 0.02),
        height: 51.0,
        decoration: BoxDecoration(
          color: background_recentFilter,
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              itemFiltre.name,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 13.0,
                fontFamily: fontMedium,
                color: primary_black,
              ),
            ),
            Visibility(
              visible: itemFiltre.name == filtreOption.name,
              child: Icon(Icons.check_circle, color: primary_blue, size: 17.0),
            )
          ],
        ),
      ),
    );
  }
}
