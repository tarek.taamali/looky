import 'package:applooki/assets/colors.dart';
import 'package:applooki/assets/custom_button.dart';
import 'package:applooki/assets/elements.dart';
import 'package:flutter/material.dart';
import '../assets/fonts.dart';
import 'filtre_options_list.dart';
import 'liste_filtres_screen.dart';

class RechercheFiltreScreen extends StatefulWidget {
  final Function(FiltreOption) onSelect;
  final FiltreOption? filtreOption;
  final FiltreOption? filtrePrice;

  const RechercheFiltreScreen({
    Key? key,
    required this.onSelect,
    this.filtreOption,
    this.filtrePrice,
  }) : super(key: key);
  @override
  _RechercheFiltreScreenState createState() => _RechercheFiltreScreenState();
}

class _RechercheFiltreScreenState extends State<RechercheFiltreScreen> {
  late FiltreOption filtreOption;
  late FiltreOption? filtrePrice;
  @override
  void initState() {
    super.initState();
    filtreOption = widget.filtreOption ?? listOptions.first;
    filtrePrice = widget.filtrePrice;
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: primary_white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: primary_white,
        title: Center(
          child: Container(
            width: size.width * 0.85,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: 35.0,
                  height: 35.0,
                  decoration: BoxDecoration(
                    color: back_input_search,
                    borderRadius: BorderRadius.circular(9.0),
                  ),
                  child: Center(
                    child: IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: primary_blue,
                        size: 14,
                      ),
                      onPressed: () => Navigator.pop(context),
                    ),
                  ),
                ),
                Text(
                  'Filtre',
                  style: TextStyle(
                    fontSize: 15.0,
                    color: primary_blue,
                    fontFamily: fontBold,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      filtreOption = widget.filtreOption ?? listOptions.first;
                    });
                  },
                  child: Text(
                    'Réinitialiser',
                    style: TextStyle(
                      fontFamily: fontRegular,
                      fontSize: 13.0,
                      color: primary_black,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        centerTitle: true,
      ),
      body: Center(
        child: Container(
          height: size.height,
          width: size.width * 0.85,
          child: Stack(fit: StackFit.expand, children: [
            Positioned(
              width: size.width * 0.85,
              top: size.height * 0.03,
              child: Text(
                'Trier par',
                textAlign: TextAlign.left,
              ),
            ),
            Positioned(
              width: size.width * 0.85,
              top: size.height * 0.075,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ListeFiltreScreen(
                            filtreOption: filtreOption,
                            onSelect: (sfiltreOption) {
                              setState(() {
                                filtreOption = sfiltreOption;
                              });
                            },
                          ),
                        ),
                      );
                    },
                    child: Container(
                      width: size.width * 0.85,
                      padding: EdgeInsets.symmetric(horizontal: 15.0),
                      height: 51.0,
                      decoration: BoxDecoration(
                        color: background_recentFilter,
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            filtreOption.name,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 13.0,
                              fontFamily: fontMedium,
                              color: primary_black,
                            ),
                          ),
                          Icon(Icons.arrow_forward_ios,
                              color: primary_black, size: 12.0),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: size.height * 0.02),
                  Text(
                    'Prix',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 13.0,
                      fontFamily: fontMedium,
                      color: primary_black,
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Container(
                    height: 115,
                    child: GestureDetector(
                      onTap: () {},
                      child: GridView(
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          childAspectRatio: (1 / .4),
                          crossAxisSpacing: 10,
                        ),
                        children: listPrice.map((e) => _buildRow(e)).toList(),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              top: size.height * 0.75,
              child: CustomButton(
                onPressed: () {
                  widget.onSelect(filtreOption);
                  Navigator.of(context).pop();
                },
                txtButton: "Appliquer",
                color: primary_white,
                backcolor: primary_blue,
                boxDecoration: boxDecorationBtnCnx(
                  bgColor: primary_blue,
                ),
                sideColor: false,
                BorderColor: primary_blue,
              ),
            ),
          ]),
        ),
      ),
    );
  }

  Widget _buildRow(FiltreOption itemFiltre) {
    final size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        setState(() {
          filtrePrice = itemFiltre;
        });
      },
      child: SizedBox(
        height: 51.0,
        child: Container(
          width: size.width * 0.35,
          padding: EdgeInsets.symmetric(horizontal: 15.0),
          margin: EdgeInsets.only(bottom: size.height * 0.02),
          decoration: BoxDecoration(
            color: background_recentFilter,
            borderRadius: BorderRadius.circular(8.0),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                itemFiltre.name,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 13.0,
                  fontFamily: fontMedium,
                  color: primary_black,
                ),
              ),
              Visibility(
                visible: itemFiltre.name == filtrePrice?.name,
                child:
                    Icon(Icons.check_circle, color: primary_blue, size: 17.0),
              )
            ],
          ),
        ),
      ),
    );
  }
}
