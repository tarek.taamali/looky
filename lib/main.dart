import 'package:applooki/auth/sign_in_screen.dart';
import 'package:applooki/categrory_builder/category_screen.dart';
import 'package:applooki/screens/splash_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'Utils/local_storage.dart';
import 'package:applooki/enter_page/enter_page_screen.dart';
import 'package:applooki/landing/landing_screen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await StorageUtil.init();
  return runApp(ProviderScope(child: App()));
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          errorColor: (Colors.redAccent[200]),
        ),
       //home :SplashScreen()
        initialRoute: '/',
        routes: {
          '/': (context) => LandingPageScreen(),
          'login': (context) => SignInScreen(),
          'base': (context) => EnterPageScreen(),
          'splashScreen':(context) => SplashScreen(),
        }
        );
  }
}
