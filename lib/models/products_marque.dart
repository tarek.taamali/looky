// To parse this JSON data, do
//
//     final adresseResponse = adresseResponseFromJson(jsonString);

import 'dart:convert';

import 'package:applooki/core/utf_helper.dart';

ManufacturersResponse optionsValuesResponseFromJson(String str) =>
    ManufacturersResponse.fromJson(json.decode(str));

String optionsValuesResponseToJson(ManufacturersResponse data) =>
    json.encode(data.toJson());

class ManufacturersResponse {
  ManufacturersResponse({
    required this.manufacturers,
  });

  List<Manufacturer> manufacturers;

  factory ManufacturersResponse.fromJson(Map<String, dynamic> json) =>
      ManufacturersResponse(
        manufacturers: List<Manufacturer>.from(
            json["manufacturers"].map((x) => Manufacturer.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "manufacturers":
            List<dynamic>.from(manufacturers.map((x) => x.toJson())),
      };
}

class Manufacturer {
  Manufacturer({
    required this.id,
    required this.name,
  });

  int id;
  String name;

  factory Manufacturer.fromJson(Map<String, dynamic> json) => Manufacturer(
        id: json["id"],
        name: toDecodedString(json["name"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
      };
}
