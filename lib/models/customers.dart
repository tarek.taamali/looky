// To parse this JSON data, do
//
//     final customers = customersFromJson(jsonString);

import 'dart:convert';

Customers customersFromJson(String str) => Customers.fromJson(json.decode(str));

class Customers {
  Customers({
    required this.customers,
  });

  List<Customer> customers;

  factory Customers.fromJson(Map<String, dynamic> json) => Customers(
        customers: List<Customer>.from(
            json["customers"].map((x) => Customer.fromJson(x))),
      );
}

class Customer {
  Customer({
    required this.id,
    required this.idDefaultGroup,
    required this.idLang,
    required this.secureKey,
    required this.passwd,
    required this.pass,
    required this.lastname,
    required this.firstname,
    required this.email,
    this.sellerId = "",
  });

  String id;
  String idDefaultGroup;
  String idLang;
  String secureKey;
  String passwd;
  String pass;
  String lastname;
  String firstname;
  String email;
  String sellerId;

  factory Customer.fromJson(Map<String, dynamic> json) => Customer(
        id: json["id"].toString(),
        idDefaultGroup: json["id_default_group"],
        idLang: json["id_lang"],
        secureKey: "",
       // deleted: json["deleted"],
        passwd: json["passwd"],
        pass: json["pass"],
        lastname: json["lastname"],
        firstname: json["firstname"],
        email: json["email"],
        sellerId: json["sellerId"]?.toString() ?? "",
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "id_default_group": idDefaultGroup,
        "id_lang": idLang,
        "secure_key": secureKey,
        "passwd": passwd,
        "pass": pass,
        "lastname": lastname,
        "firstname": firstname,
        "email": email,
        "sellerId": sellerId
      };
}
