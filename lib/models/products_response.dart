// To parse this JSON data, do
//
//     final adresseResponse = adresseResponseFromJson(jsonString);

import 'dart:convert';

import 'package:applooki/core/utf_helper.dart';

OptionsValuesResponse optionsValuesResponseFromJson(String str) =>
    OptionsValuesResponse.fromJson(json.decode(str));

String optionsValuesResponseToJson(OptionsValuesResponse data) =>
    json.encode(data.toJson());

class OptionsValuesResponse {
  OptionsValuesResponse({
    required this.productOptionValues,
  });

  List<ProductOptionValue> productOptionValues;

  factory OptionsValuesResponse.fromJson(Map<String, dynamic> json) =>
      OptionsValuesResponse(
        productOptionValues: List<ProductOptionValue>.from(
            json["product_option_values"]
                .map((x) => ProductOptionValue.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "product_option_values":
            List<dynamic>.from(productOptionValues.map((x) => x.toJson())),
      };
}

class ProductOptionValue {
  ProductOptionValue({
    required this.id,
    required this.idAttributeGroup,
    required this.color,
    required this.position,
    required this.name,
  });

  int id;
  String idAttributeGroup;
  String color;
  String position;
  String name;

  factory ProductOptionValue.fromJson(Map<String, dynamic> json) =>
      ProductOptionValue(
        id: json["id"],
        idAttributeGroup: json["id_attribute_group"],
        color: json["color"],
        position: json["position"],
        name: toDecodedString(json["name"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "id_attribute_group": idAttributeGroup,
        "color": color,
        "position": position,
        "name": name,
      };
}
