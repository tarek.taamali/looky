import 'dart:io';
import 'dart:ui';
import 'package:applooki/add_product/add_product_view_model.dart';
import 'package:applooki/assets/colors.dart';
import 'package:applooki/case_screen/success_add_product_screen.dart';
import 'package:applooki/shared/bottom_bar_custom.dart';
import 'package:applooki/shared/top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:image_picker/image_picker.dart';

final addProductViewModel = ChangeNotifierProvider<AddProductViewModel>(
  (ref) => AddProductViewModel(),
);

class AjoutProduitScreen extends StatefulWidget {
  _AjoutProduitScreenState createState() => _AjoutProduitScreenState();
}

class _AjoutProduitScreenState extends State<AjoutProduitScreen> {
  int indexPage = 0;
  var imagePicker;
  List<File> _pickedFiles = [];
  @override
  void initState() {
    super.initState();
    imagePicker = new ImagePicker();
  }

  showAlertSourceImage() async {
    await showDialog(
      context: context,
      builder: (context) => new AlertDialog(
          content: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          TextButton(
            onPressed: () async {
              Navigator.of(context).pop();

              XFile? image = await imagePicker.pickImage(
                  source: ImageSource.camera,
                  preferredCameraDevice: CameraDevice.front);
              if (image != null) {
                setState(() {
                  _pickedFiles.add(File(image.path));
                });
              }
            },
            style: TextButton.styleFrom(
              primary: Colors.white,
              backgroundColor: Colors.black,
              onSurface: Colors.grey,
            ),
            child: Container(
                width: 80,
                child: Text(
                  'Camera',
                  textAlign: TextAlign.center,
                )),
          ),
          TextButton(
            style: TextButton.styleFrom(
              primary: Colors.white,
              backgroundColor: Colors.black,
              onSurface: Colors.grey,
            ),
            onPressed: () async {
              Navigator.of(context).pop();

              XFile? image = await imagePicker.pickImage(
                source: ImageSource.gallery,
              );
              if (image != null) {
                setState(() {
                  _pickedFiles.add(File(image.path));
                });
              }
            },
            child: Container(
                width: 80,
                child: Text(
                  'Galery',
                  textAlign: TextAlign.center,
                )),
          ),
        ],
      )),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: primary_white,
      bottomNavigationBar: BottomBarCustom(
        labelText: "Suivant",
        onPressed: () async {
          final viewModel = context.read(addProductViewModel);
          if (_pickedFiles.length > 0) {
            await viewModel.upload(context, _pickedFiles).then((value) {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => SuccessAddProductScrenn(),
                ),
              );
            });
          }
        },
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: size.width * 0.075),
          child: Column(
            children: [
              TopBarWidget(
                title: "Nouveau Article",
              ),
              Container(
                  height: size.height * 0.739,
                  child: Column(
                    children: [
                      _buildCardTakePhoto(size),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.8,
                        child: Wrap(
                            spacing: 10,
                            children: _pickedFiles
                                .map((e) => _buildPhotoTakedWidget(e))
                                .toList()),
                      ),
                    ],
                  )),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildPhotoTakedWidget(File file) {
    return Container(
      width: 110.0,
      height: 110.0,
      child: Stack(
        children: [
          Container(
            child: Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                width: 100,
                height: 100,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(7),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(7),
                  child: Image.file(
                    file,
                    width: 200.0,
                    height: 200.0,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
          ),
          Positioned(
              right: 0,
              // top: -10.0,
              child: GestureDetector(
                onTap: () {
                  _pickedFiles.remove(file);
                  setState(() {});
                },
                child: Container(
                  height: 20,
                  width: 20,
                  decoration: BoxDecoration(
                    color: primary_blue,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Icon(
                    Icons.close,
                    size: 15,
                    color: Colors.white,
                  ),
                ),
              ))
        ],
      ),
    );
  }

  Widget _buildCardTakePhoto(Size size) {
    return GestureDetector(
      onTap: () {
        showAlertSourceImage();
      },
      child: Container(
        height: 200.0,
        width: size.width * 0.85,
        padding: EdgeInsets.all(15.0),
        decoration: BoxDecoration(
          color: background_article_grey,
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Align(
          alignment: Alignment.center,
          child: Container(
              width: 200,
              height: 80,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(Icons.camera),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Ajouter au minimum deux photos de votre article',
                    textAlign: TextAlign.center,
                  )
                ],
              )),
        ),
      ),
    );
  }
}
