// To parse this JSON data, do
//
//     final productCreated = productCreatedFromJson(jsonString);

import 'dart:convert';

ProductCreated productCreatedFromJson(String str) =>
    ProductCreated.fromJson(json.decode(str));

String productCreatedToJson(ProductCreated data) => json.encode(data.toJson());

class ProductCreated {
  ProductCreated({
    required this.product,
  });

  ProductSaved product;

  factory ProductCreated.fromJson(Map<String, dynamic> json) => ProductCreated(
        product: ProductSaved.fromJson(json["product"]),
      );

  Map<String, dynamic> toJson() => {
        "product": product.toJson(),
      };
}

class ProductSaved {
  ProductSaved({
    required this.id,
    required this.price,
    required this.name,
    required this.description,
    required this.link,
  });

  String id;
  String price;
  String name;
  String description;
  String link;
  factory ProductSaved.fromJson(Map<String, dynamic> json) => ProductSaved(
        id: json["id"],
        price: json["price"],
        name: json["name"],
        description: json["description"],
        link: json["link"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "price": price,
        "name": name,
        "link": link,
        "description": description,
      };
}
