import 'dart:ui';
import 'package:applooki/Utils/utils.dart';
import 'package:applooki/add_product/add_product_photos_screen.dart';
import 'package:applooki/assets/colors.dart';
import 'package:applooki/categrory_builder/categories_responses.dart';
import 'package:applooki/models/products_marque.dart';
import 'package:applooki/models/products_response.dart';
import 'package:applooki/shared/bottom_bar_custom.dart';
import 'package:applooki/shared/top_bar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'add_product_view_model.dart';

final addProductViewModel = ChangeNotifierProvider<AddProductViewModel>(
  (ref) => AddProductViewModel(),
);

class AddProductDetailsScreen extends StatefulWidget {
  final List<Category?> listCategory;

  const AddProductDetailsScreen({Key? key, required this.listCategory})
      : super(key: key);
  _AddProductDetailsScreenState createState() =>
      _AddProductDetailsScreenState();
}

class _AddProductDetailsScreenState extends State<AddProductDetailsScreen> {
  final _formKey = GlobalKey<FormState>();

  var nameProductController = TextEditingController();
  var priceProductController = TextEditingController();
  var colorProductController = TextEditingController();
  var sizeProductController = TextEditingController();
  var conditionProductController = TextEditingController();
  var descriptionProductController = TextEditingController();
  var manufacturerProductController = TextEditingController();

  var nameProductFocusNode = FocusNode();
  var priceProductFocusNode = FocusNode();
  var colorProductFocusNode = FocusNode();
  var sizeProductFocusNode = FocusNode();
  var conditionProductFocusNode = FocusNode();
  var descriptionProductFocusNode = FocusNode();
  var manufacturerProductFocusNode = FocusNode();
  ProductOptionValue? tailleProduct;
  ProductOptionValue? colorProduct;
  Manufacturer? manufacturerProduct;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      final viewModel = context.read(addProductViewModel);
      viewModel.loadOptions(widget.listCategory);
      viewModel.loadManufacturers();
    });
  }

  void _createBottomSheet(String option) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      builder: (context) => Consumer(
        builder: ((context, watch, child) {
          final viewModel = watch(addProductViewModel);
          List<ProductOptionValue> options = option == "taille"
              ? viewModel.optionValuesT
              : viewModel.optionValuesC;

          return Container(
            height: options.length < 6
                ? options.length * 70
                : MediaQuery.of(context).size.height * 0.9,
            decoration: new BoxDecoration(
              color: Colors.white,
              borderRadius: new BorderRadius.only(
                topLeft: const Radius.circular(36.0),
                topRight: const Radius.circular(36.0),
              ),
            ),
            child: Padding(
              padding: EdgeInsets.fromLTRB(12, 16, 12, 0),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      width: 86,
                      height: 5,
                      decoration: BoxDecoration(
                          color: Color(0xff483C25),
                          borderRadius: BorderRadius.circular(5)),
                    ),
                    SizedBox(
                      height: 35,
                    ),
                    SingleChildScrollView(
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: options
                              .map((e) => BottomSheetElement(
                                    last: false,
                                    onSelect: (productOptionValue) {
                                      if (option == "taille") {
                                        tailleProduct = productOptionValue;
                                        sizeProductController.text =
                                            productOptionValue.name;
                                      } else {
                                        colorProduct = productOptionValue;
                                        colorProductController.text =
                                            productOptionValue.name;
                                      }
                                      Navigator.of(context).pop();
                                      setState(() {});
                                    },
                                    productOptionValue: e,
                                  ))
                              .toList()),
                    )
                  ],
                ),
              ),
            ),
          );
        }),
      ),
    );
  }

  void _createBottomSheetMarque() {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      builder: (context) => Consumer(
        builder: ((context, watch, child) {
          final viewModel = watch(addProductViewModel);
          List<Manufacturer> options = viewModel.productManufactures;

          return Container(
            height: options.length < 6
                ? options.length * 70
                : MediaQuery.of(context).size.height * 0.9,
            decoration: new BoxDecoration(
              color: Colors.white,
              borderRadius: new BorderRadius.only(
                topLeft: const Radius.circular(36.0),
                topRight: const Radius.circular(36.0),
              ),
            ),
            child: Padding(
              padding: EdgeInsets.fromLTRB(12, 16, 12, 0),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      width: 86,
                      height: 5,
                      decoration: BoxDecoration(
                          color: Color(0xff483C25),
                          borderRadius: BorderRadius.circular(5)),
                    ),
                    SizedBox(
                      height: 35,
                    ),
                    SingleChildScrollView(
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: options
                              .map((e) => BottomSheetElementManufacture(
                                    last: false,
                                    onSelect: (productOptionValue) {
                                      manufacturerProduct = productOptionValue;
                                      manufacturerProductController.text =
                                          productOptionValue.name;

                                      Navigator.of(context).pop();
                                      setState(() {});
                                    },
                                    productOptionValue: e,
                                  ))
                              .toList()),
                    )
                  ],
                ),
              ),
            ),
          );
        }),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: primary_white,
      bottomNavigationBar: BottomBarCustom(
        labelText: "Suivant",
        onPressed: () {
          final viewModel = context.read(addProductViewModel);
          if (_formKey.currentState!.validate()) {
            viewModel.productCreated(
              context: context,
              title: nameProductController.text,
              price: double.tryParse(priceProductController.text) ?? 0.0,
              description: descriptionProductController.text,
              condition: "new",
              productOptionColor: colorProduct,
              productOptionTailleValue: tailleProduct,
              listCategories: widget.listCategory,
              manufacturerProduct: manufacturerProduct,
            );
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AjoutProduitScreen(),
              ),
            );
          }
        },
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: size.width * 0.075),
          child: SingleChildScrollView(
            child: Column(
              children: [
                TopBarWidget(
                  title: "Nouveau Article",
                ),
                SizedBox(height: 20),
                Container(
                    height: size.height * 0.739,
                    child: Form(
                      key: _formKey,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      child: ListView(
                        children: [
                          buildTextField(
                            label: "Donner un nom pour votre article",
                            hint: "Saisissez le nom de l’article",
                            controller: nameProductController,
                            autoFocus: true,
                            focusNode: nameProductFocusNode,
                            validator: (value) => isNotEmptyField(value),
                            onFieldSubmitted: (term) {
                              nameProductFocusNode.unfocus();
                              FocusScope.of(context)
                                  .requestFocus(priceProductFocusNode);
                            },
                            onSubmit: () {},
                          ),
                          SizedBox(height: 20),
                          buildTextField(
                            label: "Prix de l’article",
                            hint: "Saisissez le prix de l’article",
                            textInputType: TextInputType.number,
                            controller: priceProductController,
                            autoFocus: false,
                            focusNode: priceProductFocusNode,
                            validator: (value) => isNotEmptyField(value),
                            onFieldSubmitted: (term) {
                              priceProductFocusNode.unfocus();
                              FocusScope.of(context)
                                  .requestFocus(colorProductFocusNode);
                            },
                            onSubmit: () {},
                          ),
                          SizedBox(height: 20),
                          buildTextField(
                              label: "Couleur de l’article",
                              hint: "Saisissez la couleur de l’article",
                              controller: colorProductController,
                              autoFocus: false,
                              focusNode: colorProductFocusNode,
                              validator: (value) => isNotEmptyField(value),
                              onFieldSubmitted: (term) {
                                colorProductFocusNode.unfocus();
                                FocusScope.of(context)
                                    .requestFocus(sizeProductFocusNode);
                              },
                              onSubmit: () {},
                              readOnly: true,
                              onTap: () {
                                _createBottomSheet("");
                              }),
                          SizedBox(height: 20),
                          buildTextField(
                              label: "Taille de l’article",
                              hint: "Saisissez la taille de l’article",
                              controller: sizeProductController,
                              autoFocus: false,
                              focusNode: sizeProductFocusNode,
                              validator: (value) => isNotEmptyField(value),
                              onFieldSubmitted: (term) {
                                sizeProductFocusNode.unfocus();
                                FocusScope.of(context)
                                    .requestFocus(manufacturerProductFocusNode);
                              },
                              onSubmit: () {},
                              readOnly: true,
                              onTap: () {
                                _createBottomSheet("taille");
                              }),
                          SizedBox(height: 20),
                          buildTextField(
                              label: "Marque de l’article",
                              hint: "Saisissez la marque de l’article",
                              controller: manufacturerProductController,
                              autoFocus: false,
                              focusNode: manufacturerProductFocusNode,
                              validator: (value) => isNotEmptyField(value),
                              onFieldSubmitted: (term) =>
                                  sizeProductFocusNode.unfocus(),
                              onSubmit: () {},
                              readOnly: true,
                              onTap: () {
                                _createBottomSheetMarque();
                              }),
                          SizedBox(height: 20),
                          buildTextAreaField(
                            label: "Description de l’article",
                            hint: "Parler un peu de l’article",
                            controller: descriptionProductController,
                            autoFocus: false,
                            focusNode: descriptionProductFocusNode,
                            validator: (value) => isNotEmptyField(value),
                            onFieldSubmitted: (term) {
                              descriptionProductFocusNode.unfocus();
                            },
                            onSubmit: () {},
                          ),
                          SizedBox(height: 20),
                        ],
                      ),
                    )),
                SizedBox(height: 20),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildTextField({
    required String label,
    required String hint,
    required TextEditingController controller,
    required FocusNode focusNode,
    required Function onSubmit,
    required bool autoFocus,
    Function(String)? onFieldSubmitted,
    required String? Function(String?) validator,
    bool readOnly = false,
    TextInputType? textInputType,
    Function? onTap,
  }) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(label, style: TextStyle(fontWeight: FontWeight.w600)),
          SizedBox(
            height: 15,
          ),
          TextFormField(
            readOnly: readOnly,
            onTap: () {
              if (onTap != null) onTap();
            },
            autofocus: autoFocus,
            controller: controller,
            keyboardType: textInputType,
            focusNode: focusNode,
            decoration: InputDecoration(
              suffixIcon: readOnly ? Icon(Icons.arrow_drop_down) : null,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0)),
                borderSide: BorderSide(
                  color: primary_blue,
                ),
              ),
              filled: true,
              hintStyle: TextStyle(fontSize: 15),
              hintText: hint,
              fillColor: Colors.white70,
            ),
            onFieldSubmitted: onFieldSubmitted,
            validator: validator,
          ),
        ],
      ),
    );
  }

  Widget buildTextAreaField({
    required String label,
    required String hint,
    required TextEditingController controller,
    required FocusNode focusNode,
    required Function onSubmit,
    required bool autoFocus,
    Function(String)? onFieldSubmitted,
    required String? Function(String?) validator,
  }) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(label, style: TextStyle(fontWeight: FontWeight.w600)),
          SizedBox(
            height: 15,
          ),
          TextFormField(
            minLines: 6,
            controller: controller,
            keyboardType: TextInputType.multiline,
            maxLines: null,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15.0)),
                  borderSide: BorderSide(
                    color: primary_blue,
                  )),
              filled: true,
              hintStyle: TextStyle(fontSize: 15),
              hintText: hint,
              fillColor: Colors.white70,
            ),
            onFieldSubmitted: onFieldSubmitted,
            validator: validator,
            focusNode: focusNode,
            autofocus: false,
          ),
        ],
      ),
    );
  }
}

class BottomSheetElement extends StatelessWidget {
  final ProductOptionValue productOptionValue;
  final Function(ProductOptionValue)? onSelect;

  bool last = false;

  BottomSheetElement({
    required this.productOptionValue,
    required this.onSelect,
    required this.last,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onSelect!(productOptionValue);
      },
      child: Container(
        height: 55,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(width: 1.0, color: Color(0xffD6D6D6)),
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [
                SizedBox(width: 10),
                Text(productOptionValue.name, style: TextStyle(fontSize: 12)),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class BottomSheetElementManufacture extends StatelessWidget {
  final Manufacturer productOptionValue;
  final Function(Manufacturer)? onSelect;

  bool last = false;

  BottomSheetElementManufacture({
    required this.productOptionValue,
    required this.onSelect,
    required this.last,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onSelect!(productOptionValue);
      },
      child: Container(
        height: 55,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(width: 1.0, color: Color(0xffD6D6D6)),
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [
                SizedBox(width: 10),
                Text(productOptionValue.name, style: TextStyle(fontSize: 12)),
              ],
            ),
            /* (last == false)
                ? Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.width * 0.05,
                    ),
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.9,
                      height: 1,
                      color: Color(0xffD6D6D6),
                    ),
                  )
                : SizedBox()*/
          ],
        ),
      ),
    );
  }
}
