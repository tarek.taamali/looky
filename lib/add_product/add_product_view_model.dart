import 'dart:convert';
import 'dart:io';
import 'package:applooki/Utils/local_storage.dart';
import 'package:applooki/Utils/xml_helpers.dart';
import 'package:applooki/core/api_message.dart';
import 'package:applooki/core/utf_helper.dart';
import 'package:applooki/add_product/product_created.dart';
import 'package:applooki/assets/elements.dart';
import 'package:applooki/models/customers.dart';
import 'package:applooki/models/products_marque.dart';
import 'package:applooki/models/products_response.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:http/http.dart' as http;
import '../constant.dart';
import 'package:diacritic/diacritic.dart';
import 'package:applooki/categrory_builder/categories_responses.dart';

class AddProductViewModel extends ChangeNotifier {
  Dio dio = new Dio();
  List<ProductOptionValue> productOptionValues = [];
  List<Manufacturer> productManufactures = [];

  List<ProductOptionValue> optionValuesT = [];
  List<ProductOptionValue> optionValuesC = [];
  ProductOptionValue? productOptionTaille;

  loadOptions(List<Category?> listCategory) async {
    final response = await http.get(
        Uri.parse(
          BASE_URL + "product_option_values?output_format=JSON&display=full",
        ),
        headers: kheaders);
    if (response.statusCode == 200) {
      String filter = "";
      var resp = json.decode(response.body);
      var category = listCategory.first;
      if (category!.rootCat.contains("Hommes")) {
        if (category.rootCat.contains("chaussures")) {
          filter = "3";
        } else {
          filter = "4";
        }
      } else if (category.rootCat.contains('Femmes')) {
        if (category.rootCat.contains("Chaussures")) {
          filter = "3";
        } else {
          filter = "1";
        }
      }
      productOptionValues =
          OptionsValuesResponse.fromJson(resp).productOptionValues;
      optionValuesT = productOptionValues
          .where((element) => element.idAttributeGroup == filter)
          .toList();
      optionValuesC = productOptionValues
          .where((element) => element.idAttributeGroup == "2")
          .toList();
    }
    notifyListeners();
  }

  loadManufacturers() async {
    final response = await http.get(
        Uri.parse(
          BASE_URL + "manufacturers?output_format=JSON&display=full",
        ),
        headers: kheaders);
    if (response.statusCode == 200) {
      var resp = json.decode(response.body);
      productManufactures = ManufacturersResponse.fromJson(resp).manufacturers;
    }
    notifyListeners();
  }

  Future<bool> upload(BuildContext context, List<File> files) async {
    Loader.show(
      context,
      progressIndicator: LoaderWidget(context),
    );
    Map<String, dynamic> jsondatais =
        jsonDecode(StorageUtil.getString('productSaved'));
    ProductSaved productCreated = ProductSaved.fromJson(jsondatais);
    if (files.length > 0) {
      try {
        await Future.forEach(files, (File file) async {
          String fileName = file.path.split('/').last;

          FormData data = FormData.fromMap({
            "image": await MultipartFile.fromFile(
              file.path,
              filename: fileName,
            ),
          });

          var res = await dio.post(
              BASE_URL +
                  "images/products/${productCreated.id}?output_format=JSON",
              data: data,
              options: Options(headers: kheaders));
          if (res.statusCode == 200) {}
        });
        Loader.hide();
      } catch (error) {
        print(error.toString());
      }
    }
    return true;
  }

  void productCreated({
    required BuildContext context,
    required String title,
    required double price,
    required String description,
    required String condition,
    required ProductOptionValue? productOptionTailleValue,
    required ProductOptionValue? productOptionColor,
    required List<Category?> listCategories,
    Manufacturer? manufacturerProduct,
  }) async {
    Loader.show(
      context,
      progressIndicator: LoaderWidget(context),
    );
    productOptionTaille = productOptionTailleValue;
    Map<String, dynamic> jsondatais =
        jsonDecode(StorageUtil.getString('customer'));
    var customer = Customer.fromJson(jsondatais);
    var link = removeDiacritics(title)
        .replaceAll(new RegExp(r'[^\w\s]+'), '_')
        .replaceAll(" ", "_");
    String path = PRODUCTS_URL + "?output_format=JSON";
    var categoryText = "";
    print(
      "lsit cate ${listCategories.first!.id}-${listCategories.first!.name}",
    );
    String defaultCat = listCategories.first!.id.toString();
    listCategories.forEach((element) {
      categoryText += "              <category>\n" +
          "                    <id>${element!.id}</id>\n" +
          "                </category>\n";
    });

    try {
            print(
        buildCreatProduct(
            price: price,
            description: description,
            link: link,
            title: title,
            categoryText: categoryText,
            defaultCat: defaultCat,
            manufacturerProduct: manufacturerProduct),
      );
      print("-------------");
      print(path);
      var response = await http.post(
        Uri.parse(path),
        headers: kheaders,
        body: buildCreatProduct(
          categoryText: categoryText,
          description: description,
          link: link,
          price: price,
          title: title,
          defaultCat: defaultCat,
          manufacturerProduct: manufacturerProduct,
        ),
      );
      print("create product ${response.body}");
      if (response.statusCode == 201) {
        Map<String, dynamic> result = json.decode(response.body);
        if (result.containsKey("product")) {
          ProductSaved productCreated = ProductSaved(
            id: result["product"]["id"],
            price: price.toString(),
            link: link,
            name: title,
            description: description,
          );
          StorageUtil.putString(
            'productSaved',
            jsonEncode(
              productCreated.toJson(),
            ),
          );
          await combinaisonCreated(
            idProduct: result["product"]["id"],
            productOptionColor: productOptionColor,
            productOptionTailleValue: productOptionTailleValue,
          );

          await productAffected(
              idSeller: customer.sellerId.toString(),
              idProduct: result["product"]["id"]);
        }
      } else {
        String errorMessage = "Il semble qu'il a y eu un problème !";
        Map<String, dynamic> errors = json.decode(response.body);
        if (errors.containsKey("errors")) {
          var errorResponse = ErrorResponse.fromJson(errors);
          errorMessage = errorResponse.errors.length > 0
              ? toDecodedString(errorResponse.errors.first.message)
              : "Il semble qu'il a y eu un problème !";
        }
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(errorMessage),
        ));
      }
    } catch (error) {
      Loader.hide();
      print("response :\n $error}");
    }
    Loader.hide();
  }

  Future<void> combinaisonCreated({
    required String idProduct,
    required ProductOptionValue? productOptionTailleValue,
    required ProductOptionValue? productOptionColor,
  }) async {
    String path = COMBINAISON_URL + "?output_format=JSON";
    try {
      var response = await http.post(Uri.parse(path),
          headers: kheaders,
          body: buildCreatedCombnaison(
            idProduct: idProduct,
            idOp1: productOptionTailleValue!.id.toString(),
            idOp2: productOptionColor!.id.toString(),
          ));
      print(response.statusCode.toString());
      var result = json.decode(response.body);
      StorageUtil.putString(
          "idCombinaison", result["combination"]["id"].toString());
    } catch (error) {
      print("response :\n $error}");
    }
  }

  Future<void> productAffected({
    required String idSeller,
    required String idProduct,
  }) async {
    String path = KSELLERS_PRODUCTS_URL + "?output_format=JSON";
    try {
      var response = await http.post(Uri.parse(path),
          headers: kheaders,
          body: buildAffectedSeller(
            idSeller: idSeller,
            idProduct: idProduct,
          ));
      print(response.statusCode.toString());
      print("productAffected \n : ${response.body}");
    } catch (error) {
      print("response :\n $error}");
    }
  }

  Future<void> productCategoryUpdate1(
    BuildContext context,
    Category? defaultCategory,
    List<Category?> listCategories,
  ) async {
    Loader.show(
      context,
      progressIndicator: LoaderWidget(context),
    );
    Map<String, dynamic> jsondatais =
        jsonDecode(StorageUtil.getString('productSaved'));
    ProductSaved productCreated = ProductSaved.fromJson(jsondatais);
    String idCombinaison = StorageUtil.getString("idCombinaison");
    var categoryText = "";
    listCategories.forEach((element) {
      categoryText += "              <category>\n" +
          "                    <id>${element!.id}</id>\n" +
          "                </category>\n";
    });

    String path = PRODUCTS_URL + "?output_format=JSON";
    try {
      await http.put(Uri.parse(path),
          headers: kheaders,
          body: buildCategoryUpdated(
            categoryText: categoryText,
            idCombinaison: idCombinaison,
            productCreated: productCreated,
            id: defaultCategory!.id,
          ));
      Loader.hide();
    } catch (error) {
      Loader.hide();
      print("response :\n $error}");
    }
  }
}
