import 'package:applooki/Utils/strings.dart';
import 'package:applooki/Utils/utils.dart';
import 'package:applooki/auth/forget_password/forget_password_screen.dart';
import 'package:applooki/auth/signup_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'sign_in_notifier.dart';
import 'package:applooki/assets/colors.dart';
import 'package:applooki/assets/custom_button.dart';
import 'package:applooki/assets/custom_password_form_field.dart';
import 'package:applooki/assets/fonts.dart';
import 'package:applooki/assets/images_links.dart';
import 'package:applooki/assets/custom_text_field.dart';
import 'package:applooki/assets/elements.dart';

final signIn = ChangeNotifierProvider<SignInNotifier>(
  (ref) => SignInNotifier(),
);

class SignInScreen extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<SignInScreen> {
  final _formKey = GlobalKey<FormState>();
  FocusNode emailFocusNode = new FocusNode();
  FocusNode passwordFocusNode = new FocusNode();
  var emailController = TextEditingController();
  var passwordController = TextEditingController();
  bool _showPassword = false;

  @override
  void initState() {
    super.initState();
    //emailController.text = "tarek.taa95@gmail.com";
    //passwordController.text = "12345690";
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      final viewModel = context.read(signIn);
      viewModel.reset();
    });
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final viewModel = context.read(signIn);

    return SafeArea(
      child: Scaffold(
        backgroundColor: primary_white,
        resizeToAvoidBottomInset: true,
        body: Container(
          height: size.height,
          width: size.width,
          color: primary_blue,
          child: ProviderListener<SignInNotifier>(
            provider: signIn,
            onChange: (context, _signIn) {
              if (_signIn.loadingData == Status.loading) {
                Loader.show(
                  context,
                  progressIndicator: LoaderWidget(context),
                );
              } else if (_signIn.loadingData == Status.loaded &&
                  _signIn.errorMessage.isNotEmpty) {
                Loader.hide();
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text(_signIn.errorMessage),
                ));
              }
              if (_signIn.signInMethod == SignInMethod.isConnected ||
                  _signIn.signInMethod == SignInMethod.anonymous) {
                Navigator.pushNamedAndRemoveUntil(
                    context, 'base', (route) => false);
              }
            },
            child: SingleChildScrollView(
              physics:
                  ClampingScrollPhysics(parent: NeverScrollableScrollPhysics()),
              child: Column(
                children: [
                  Container(
                    height: size.height * 0.25,
                    child: SvgPicture.asset(
                      "images/ic_looki.svg",
                      color: Colors.white,
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(30),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        const SizedBox(
                          height: 25,
                        ),
                        Container(
                          margin: const EdgeInsets.only(bottom: 10),
                          child: Text(
                            "Bienvenue",
                            style: TextStyle(
                              fontSize: 25,
                              fontFamily: fontBold,
                              color: primary_blue,
                            ),
                          ),
                        ),
                        Text(
                          "Connectez-vous à votre compte",
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: fontMedium,
                            color: primary_black,
                          ),
                          textAlign: TextAlign.left,
                        ),
                        Form(
                          key: _formKey,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(height: size.height * 0.03),
                              Container(
                                child: CustomFormField(
                                  prefixIcon: const Icon(
                                    Icons.mail,
                                    color: background_blue,
                                    size: 15.0,
                                  ),
                                  width: size.width * 0.85,
                                  keyboardType: TextInputType.emailAddress,
                                  hintText: emailLabel,
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return requiredFieldLabel;
                                    }
                                    if (!isValidEmail(value)) {
                                      return emailNotValidLabel;
                                    }

                                    return null;
                                  },
                                  controller: emailController,
                                  focusNode: emailFocusNode,
                                  autofocus: true,
                                  onFieldSubmitted: (term) {
                                    emailFocusNode.unfocus();
                                    FocusScope.of(context)
                                        .requestFocus(passwordFocusNode);
                                  },
                                ),
                              ),
                              SizedBox(height: size.height * 0.03),
                              Container(
                                child: CustomPasswordField(
                                  prefixIcon: const Icon(
                                    Icons.lock,
                                    color: background_blue,
                                    size: 15.0,
                                  ),
                                  width: size.width * 0.85,
                                  controller: passwordController,
                                  focusNode: passwordFocusNode,
                                  labelText: passwordLabel,
                                  obscureText: !this._showPassword,
                                  suffixIcon: IconButton(
                                    icon: Icon(
                                      _showPassword
                                          ? Icons.visibility
                                          : Icons.visibility_off,
                                      color: this._showPassword
                                          ? primary_blue
                                          : Colors.grey,
                                    ),
                                    onPressed: () {
                                      setState(
                                          () => _showPassword = !_showPassword);
                                    },
                                  ),
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return requiredFieldLabel;
                                    }
                                    return null;
                                  },
                                  onFieldSubmitted: (term) {
                                    passwordFocusNode.unfocus();
                                    if (_formKey.currentState!.validate()) {
                                      viewModel.signInWithEmailAndPassword(
                                        context,
                                        emailController.text,
                                        passwordController.text,
                                      );
                                    }
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: size.height * 0.01,
                        ),
                        Container(
                          width: (MediaQuery.of(context).size.width) * 0.8,
                          child: GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => ForgetPasswordScreen(),
                                ),
                              );
                            },
                            child: Text(
                              'Mot de passe oublié ?',
                              style: const TextStyle(
                                color: primary_blue,
                                fontFamily: fontMedium,
                                fontSize: 13,
                              ),
                              textAlign: TextAlign.right,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: size.height * 0.03,
                        ),
                        CustomButton(
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              viewModel.signInWithEmailAndPassword(
                                context,
                                emailController.text,
                                passwordController.text,
                              );
                            }
                          },
                          txtButton: "S'identifier",
                          color: primary_white,
                          backcolor: primary_blue,
                          boxDecoration: boxDecorationBtnCnx(
                            bgColor: primary_blue,
                          ),
                          sideColor: false,
                          BorderColor: primary_blue,
                        ),
                        SizedBox(height: size.height * 0.03),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Vous n\'avez pas de compte? ',
                              style: const TextStyle(
                                color: primary_black,
                                fontFamily: fontMedium,
                                fontSize: 13,
                              ),
                              textAlign: TextAlign.center,
                            ),
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => SignUpPage(),
                                  ),
                                );
                              },
                              child: Text(
                                'S\'inscrire',
                                style: const TextStyle(
                                  color: primary_blue,
                                  fontFamily: fontMedium,
                                  fontSize: 13,
                                ),
                                textAlign: TextAlign.right,
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: size.height * 0.02),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Text(
                              'Continuer comme ',
                              style: TextStyle(
                                color: primary_black,
                                fontFamily: fontMedium,
                                fontSize: 13,
                              ),
                              textAlign: TextAlign.center,
                            ),
                            GestureDetector(
                              onTap: () {
                                viewModel.signInAnonymous();
                              },
                              child: const Text(
                                'visiteur',
                                style: TextStyle(
                                  color: primary_blue,
                                  fontFamily: fontMedium,
                                  fontSize: 13,
                                ),
                                textAlign: TextAlign.right,
                              ),
                            )
                          ],
                        ),
                        Opacity(
                          opacity: 0,
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Expanded(
                                  child: new Container(
                                    margin: const EdgeInsets.only(
                                        left: 0, right: 12.0),
                                    child: Divider(
                                      color: txt_grey,
                                      height: 50,
                                    ),
                                  ),
                                ),
                                Text(
                                  "ou connectez-vous avec",
                                  style: const TextStyle(
                                    fontSize: 14,
                                    fontFamily: fontRegular,
                                    color: txt_grey,
                                  ),
                                ),
                                Expanded(
                                  child: new Container(
                                    margin: const EdgeInsets.only(
                                        left: 12.0, right: 0),
                                    child: const Divider(
                                      color: txt_grey,
                                      height: 50,
                                    ),
                                  ),
                                ),
                              ]),
                        ),
                        SizedBox(height: size.height * 0.01),
                        Opacity(
                          opacity: 0,
                          child: Center(
                            child: GestureDetector(
                              onTap: () {
                                setState(() {});
                              },
                              child: Image.asset(
                                iconeFb,
                                width: 45.0,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: size.height * 0.01),
                        /*  Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Text(
                              'Continuer comme ',
                              style: TextStyle(
                                color: primary_black,
                                fontFamily: fontMedium,
                                fontSize: 13,
                              ),
                              textAlign: TextAlign.center,
                            ),
                            GestureDetector(
                              onTap: () {
                                viewModel.signInAnonymous();
                              },
                              child: const Text(
                                'visiteur',
                                style: TextStyle(
                                  color: primary_blue,
                                  fontFamily: fontMedium,
                                  fontSize: 13,
                                ),
                                textAlign: TextAlign.right,
                              ),
                            )
                          ],
                        ),*/
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
