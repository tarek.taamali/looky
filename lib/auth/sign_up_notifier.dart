import 'dart:convert';
import 'package:applooki/Utils/local_storage.dart';
import 'package:applooki/Utils/utils.dart';
import 'package:applooki/Utils/xml_helpers.dart';
import 'package:applooki/auth/sign_in_notifier.dart';
import 'package:applooki/constant.dart';
import 'package:applooki/core/api_message.dart';
import 'package:applooki/core/utf_helper.dart';
import 'package:applooki/models/customers.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SignUpNotifier extends ChangeNotifier {
  SignInMethod signInMethod = SignInMethod.none;
  Status loadingData = Status.none;
  String errorMessage = '';

  reset() {
    signInMethod = SignInMethod.none;
    loadingData = Status.none;
    errorMessage = '';
    notifyListeners();
  }

  void signUpWithEmailAndPassword(BuildContext context, String _firstName,
      String _lastName, String _email, String _password) async {
    errorMessage = '';
    loadingData = Status.loading;
    notifyListeners();
    String path = CUSTOMERS_URL + "?output_format=JSON";
    try {
      var response = await http.post(
        Uri.parse(path),
        headers: kheaders,
        body: buildCreatedCustomerXml(
          password: _password,
          firstName: _firstName,
          lastName: _lastName,
          email: _email,
        ),
      );
      if (response.statusCode == 201) {
        var customer =
            Customer.fromJson(json.decode(response.body)['customer']);
        _createSeller(customer.id.toString(), _firstName + _lastName, 1, 8, 1,
            0, 0, '', _email, 1, '', customer);
      } else {
        errorMessage = "Il semble qu'il a y eu un problème !";
        Map<String, dynamic> errors = json.decode(response.body);
        if (errors.containsKey("errors")) {
          var errorResponse = ErrorResponse.fromJson(errors);
          errorMessage = errorResponse.errors.length > 0
              ? toDecodedString(errorResponse.errors.first.message)
              : "Il semble qu'il a y eu un problème !";
        }
        loadingData = Status.loaded;
        notifyListeners();
      }
    } catch (e) {
      loadingData = Status.loaded;
      errorMessage = "Il semble qu'il a y eu un problème !";
      notifyListeners();
    }
  }

  Future<void> _createSeller(
    String idCustomer,
    String title,
    int idDefaultLang,
    int idCountry,
    int idShop,
    int approved,
    int active,
    String phoneNumber,
    String businessEmail,
    int notificationType,
    String address,
    Customer customer,
  ) async {
    String path = BASE_URL + "kbsellers?output_format=JSON";
    try {
      var response = await http.post(Uri.parse(path),
          headers: kheaders,
          body: buildCreateSellerXml(
            idCustomer: idCustomer,
            title: title,
            idDefaultLang: idDefaultLang,
            idCountry: idCountry,
            idShop: idShop,
            approved: approved,
            active: active,
            phoneNumber: phoneNumber,
            businessEmail: businessEmail,
            notificationType: notificationType,
            address: address,
          ));
      print(response.body);
      if (response.statusCode == 201) {
        print("seller created --------");

        var _idSeller = json.decode(response.body)["seller"]["id"];

        Customer createdCustmer = customer;
        createdCustmer.sellerId = _idSeller;
        StorageUtil.putString('customer', jsonEncode(createdCustmer.toJson()));
        print(createdCustmer.toJson().toString());
        StorageUtil.putString("connected", "connected");
        loadingData = Status.loaded;
        signInMethod = SignInMethod.isConnected;
        notifyListeners();
      } else {
        loadingData = Status.loaded;
        errorMessage = "Il semble qu'il a y eu un problème !";
        notifyListeners();
      }
    } catch (e) {
      print(e.toString());
    }
  }
}
