import 'dart:async';
import 'dart:io';

import 'package:applooki/assets/colors.dart';
import 'package:applooki/assets/fonts.dart';
import 'package:applooki/shared/top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../../constant.dart';

class ForgetPasswordScreen extends StatefulWidget {
  const ForgetPasswordScreen({Key? key}) : super(key: key);

  @override
  State<ForgetPasswordScreen> createState() => _ForgetPasswordScreenState();
}

class _ForgetPasswordScreenState extends State<ForgetPasswordScreen> {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) {
      WebView.platform = SurfaceAndroidWebView();
    }
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
        /*  appBar: AppBar(
          backgroundColor: Color(0xff1CB2BC),
          title: Text("Mot de passe oublié"),
        ),*/
        body: SafeArea(
      child: Container(
        height: size.height,
        child: Stack(
          children: [
            WebView(
              initialUrl:
                URL_FORGET_PASSWORD+  'recuperation-mot-de-passe',
              javascriptMode: JavascriptMode.unrestricted,
              onWebViewCreated: (WebViewController webViewController) {
                _controller.complete(webViewController);
              },
              onProgress: (int progress) {
                print('WebView is loading (progress : $progress%)');
              },
            ),
            Container(
              height: 100,
              padding: EdgeInsets.only(top: 20),
              width: double.infinity,
              color: Colors.white,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: size.width * 0.075),
                child: Container(
                  height: size.height * 0.1,
                  child: Stack(
                    children: [
                      Positioned(
                        left: 15,
                        child: Container(
                          width: 35.0,
                          height: 35.0,
                          decoration: BoxDecoration(
                            color: back_input_search,
                            borderRadius: BorderRadius.circular(9.0),
                          ),
                          child: Center(
                            child: IconButton(
                              icon: Icon(
                                Icons.arrow_back_ios,
                                color: primary_blue,
                                size: 14,
                              ),
                              onPressed: () => Navigator.pop(context),
                            ),
                          ),
                        ),
                      ),
                      Align(
                        //    alignment: Alignment.bottomCenter,
                        child: Text(
                          "",
                          style: TextStyle(
                            fontSize: 15.0,
                            color: primary_blue,
                            fontFamily: fontBold,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ), /*Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 35.0,
                      height: 35.0,
                      decoration: BoxDecoration(
                        color: back_input_search,
                        borderRadius: BorderRadius.circular(9.0),
                      ),
                      child: Center(
                        child: IconButton(
                          icon: Icon(
                            Icons.arrow_back_ios,
                            color: primary_blue,
                            size: 14,
                          ),
                          onPressed: () => Navigator.pop(context),
                        ),
                      ),
                    ),
                    Text(
                      "Mot de passe oublié",
                      style: TextStyle(
                        fontSize: 15.0,
                        color: primary_blue,
                        fontFamily: fontBold,
                      ),
                    )
                  ]),*/
            )
          ],
        ),
      ),
    ));
  }
}
