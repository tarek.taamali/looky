import 'dart:convert';
import 'package:applooki/Utils/local_storage.dart';
import 'package:applooki/Utils/utils.dart';
import 'package:applooki/constant.dart';
import 'package:applooki/models/customers.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

enum SignInMethod { none, isConnected, disConnected, anonymous }

class SignInNotifier extends ChangeNotifier {
  SignInMethod signInMethod = SignInMethod.none;
  Status loadingData = Status.none;
  String errorMessage = '';

  void signInAnonymous() {
    signInMethod = SignInMethod.anonymous;
    notifyListeners();
  }

  reset() {
    signInMethod = SignInMethod.none;
    loadingData = Status.none;
    errorMessage = '';
    notifyListeners();
  }

  void signInWithEmailAndPassword(
    BuildContext context,
    String email,
    String password,
  ) async {
    errorMessage = '';
    loadingData = Status.loading;
    notifyListeners();
    try {
      String path = CUSTOMERS_URL +
          "?output_format=JSON&display=full&filter[email]=$email&filter[pass]=${hashPass(password)}";
      var response = await http.get(Uri.parse(path), headers: kheaders);
      if (response.statusCode == 200) {
        print(response.body);
        if (response.body.toString() == "[]") {
          loadingData = Status.loaded;
          errorMessage = 'Veuillez vérifier vos informations';
          notifyListeners();
        } else {
          var resp = Customers.fromJson(json.decode(response.body));

          await _sellerByCustomerId(customerId: resp.customers.first.id)
              .then((value) {
            var _customer = resp.customers.first;
            _customer.sellerId = value;
            StorageUtil.putString('customer', jsonEncode(_customer.toJson()));
            StorageUtil.putString("connected", "connected");
          });

          loadingData = Status.loaded;
          signInMethod = SignInMethod.isConnected;
          notifyListeners();
        }
      }
    } catch (e) {
      print(e);
      loadingData = Status.loaded;
      errorMessage = "Il semble qu'il a y eu un problème.";
      notifyListeners();
    }
  }

  Future<String> _sellerByCustomerId({required String customerId}) async {
    final response = await http.get(
        Uri.parse(
          BASE_URL +
              "kbsellers?filter[id_customer]=${customerId.toString()}&display=[id,id_customer,business_email,address,phone_number,title,id_country,id_default_lang,id_shop,approved,active,notification_type]&output_format=JSON",
        ),
        headers: kheaders);
    var resp = json.decode(response.body);
    var seller =
        List<String>.from(resp["kbsellers"].map((x) => x["id"].toString()));
    if (seller.length > 0) {
      return seller.first;
    }
    return '';
  }
}
