import 'package:applooki/Utils/strings.dart';
import 'package:applooki/Utils/utils.dart';
import 'package:applooki/assets/colors.dart';
import 'package:applooki/assets/custom_button.dart';
import 'package:applooki/assets/custom_password_form_field.dart';
import 'package:applooki/assets/custom_text_field.dart';
import 'package:applooki/assets/elements.dart';
import 'package:applooki/assets/fonts.dart';
import 'package:applooki/auth/sign_in_screen.dart';
import 'package:applooki/auth/sign_up_notifier.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/svg.dart';

import 'sign_in_notifier.dart';

final signIn = ChangeNotifierProvider<SignUpNotifier>(
  (ref) => SignUpNotifier(),
);

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final _formKey2 = GlobalKey<FormState>();

  var lastNameController = TextEditingController();
  var lastNameFocusNode = FocusNode();
  var firstNameController = TextEditingController();
  var firstNameFocusNode = FocusNode();
  var emailController = TextEditingController();
  var emailFocusNode = FocusNode();
  var passwordController = TextEditingController();
  var passwordFocusNode = FocusNode();
  bool _showPassword = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      final viewModel = context.read(signIn);
      viewModel.reset();
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final viewModel = context.read(signIn);
    return SafeArea(
      child: Scaffold(
        backgroundColor: primary_white,
        body: Container(
          height: size.height,
          width: size.width,
          color: primary_blue,
          child: ProviderListener<SignUpNotifier>(
            provider: signIn,
            onChange: (context, _signIn) {
              if (_signIn.loadingData == Status.loading) {
                Loader.show(
                  context,
                  progressIndicator: LoaderWidget(context),
                );
              } else if (_signIn.loadingData == Status.loaded &&
                  _signIn.errorMessage.isNotEmpty) {
                Loader.hide();
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text(_signIn.errorMessage),
                ));
              }
              if (_signIn.signInMethod == SignInMethod.isConnected) {
                Loader.hide();
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SignInScreen(),
                  ),
                );
              }
              if (_signIn.signInMethod == SignInMethod.anonymous) {
                Navigator.pushNamedAndRemoveUntil(
                    context, 'base', (route) => false);
              }
            },
            child: SingleChildScrollView(
              physics:
                  ClampingScrollPhysics(parent: NeverScrollableScrollPhysics()),
              child: Column(
                children: [
                  Container(
                    height: size.height * 0.25,
                    child: SvgPicture.asset(
                      "images/ic_looki.svg",
                      color: Colors.white,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(30),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height: 25,
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: Text(
                            "Créer votre compte",
                            style: TextStyle(
                                fontSize: 25,
                                fontFamily: fontBold,
                                color: primary_blue),
                          ),
                        ),
                        Text(
                          "Entrez vos informations",
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: fontMedium,
                            color: primary_black,
                          ),
                          textAlign: TextAlign.left,
                        ),
                        Form(
                          key: _formKey2,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(height: size.height * 0.025),
                              Container(
                                child: CustomFormField(
                                  autofocus: true,
                                  focusNode: lastNameFocusNode,
                                  prefixIcon: Icon(
                                    Icons.person,
                                    color: background_blue,
                                    size: 15.0,
                                  ),
                                  width: size.width * 0.85,
                                  keyboardType: TextInputType.text,
                                  hintText: firstNameLabel,
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return requiredFieldLabel;
                                    }
                                    return null;
                                  },
                                  controller: lastNameController,
                                  onFieldSubmitted: (term) {
                                    lastNameFocusNode.unfocus();
                                    FocusScope.of(context)
                                        .requestFocus(firstNameFocusNode);
                                  },
                                ),
                              ),
                              SizedBox(height: size.height * 0.02),
                              Container(
                                child: CustomFormField(
                                  prefixIcon: Icon(
                                    Icons.person,
                                    color: background_blue,
                                    size: 15.0,
                                  ),
                                  width: size.width * 0.85,
                                  keyboardType: TextInputType.text,
                                  hintText: lastNameLabel,
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return requiredFieldLabel;
                                    }
                                    return null;
                                  },
                                  controller: firstNameController,
                                  focusNode: firstNameFocusNode,
                                  onFieldSubmitted: (term) {
                                    firstNameFocusNode.unfocus();
                                    FocusScope.of(context)
                                        .requestFocus(emailFocusNode);
                                  },
                                ),
                              ),
                              SizedBox(height: size.height * 0.02),
                              Container(
                                child: CustomFormField(
                                  prefixIcon: Icon(
                                    Icons.mail,
                                    color: background_blue,
                                    size: 15.0,
                                  ),
                                  width: size.width * 0.85,
                                  keyboardType: TextInputType.emailAddress,
                                  hintText: emailLabel,
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return requiredFieldLabel;
                                    }
                                    if (!isValidEmail(value)) {
                                      return emailNotValidLabel;
                                    }

                                    return null;
                                  },
                                  controller: emailController,
                                  focusNode: emailFocusNode,
                                  onFieldSubmitted: (term) {
                                    emailFocusNode.unfocus();
                                    FocusScope.of(context)
                                        .requestFocus(passwordFocusNode);
                                  },
                                ),
                              ),
                              SizedBox(height: size.height * 0.02),
                              Container(
                                child: CustomPasswordField(
                                  prefixIcon: Icon(
                                    Icons.lock,
                                    color: background_blue,
                                    size: 15.0,
                                  ),
                                  width: size.width * 0.85,
                                  controller: passwordController,
                                  focusNode: passwordFocusNode,
                                  labelText: passwordLabel,
                                  obscureText: !this._showPassword,
                                  suffixIcon: IconButton(
                                    icon: Icon(
                                      _showPassword
                                          ? Icons.visibility
                                          : Icons.visibility_off,
                                      color: this._showPassword
                                          ? primary_blue
                                          : Colors.grey,
                                    ),
                                    onPressed: () {
                                      setState(
                                          () => _showPassword = !_showPassword);
                                    },
                                  ),
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return requiredFieldLabel;
                                    }
                                    return null;
                                  },
                                  onFieldSubmitted: (term) {
                                    passwordFocusNode.unfocus();
                                    if (_formKey2.currentState!.validate()) {
                                      viewModel.signUpWithEmailAndPassword(
                                        context,
                                        firstNameController.text,
                                        lastNameController.text,
                                        emailController.text,
                                        passwordController.text,
                                      );
                                    }
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: size.height * 0.02,
                        ),
                        CustomButton(
                          onPressed: () {
                            if (_formKey2.currentState!.validate()) {
                              viewModel.signUpWithEmailAndPassword(
                                context,
                                firstNameController.text,
                                lastNameController.text,
                                emailController.text,
                                passwordController.text,
                              );
                            }
                          },
                          txtButton: signupLabel,
                          color: primary_white,
                          backcolor: primary_blue,
                          boxDecoration: boxDecorationBtnCnx(
                            bgColor: primary_blue,
                          ),
                          sideColor: false,
                          BorderColor: primary_blue,
                        ),
                        SizedBox(height: size.height * 0.06),
                        Center(
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Vous avez déjà un compte? ',
                                  style: TextStyle(
                                    color: primary_black,
                                    fontFamily: fontMedium,
                                    fontSize: 13,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                                GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => SignInScreen(),
                                      ),
                                    );
                                  },
                                  child: Text(
                                    'S\'identifier',
                                    style: TextStyle(
                                      color: primary_blue,
                                      fontFamily: fontMedium,
                                      fontSize: 13,
                                    ),
                                    textAlign: TextAlign.right,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
