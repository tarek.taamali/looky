import 'dart:convert';

class Categories {
  Categories({
    required this.categories,
  });

  List<Category> categories;

  factory Categories.fromJson(Map<String, dynamic> json) => Categories(
        categories: List<Category>.from(
            json["categories"].map((x) => Category.fromJson(x))).toList(),
      );
}

class Category {
  Category({
    required this.id,
    required this.name,
    required this.levelDepth,
    required this.idParent,
    required this.subCategories,
  });

  int id;
  String name;
  String levelDepth;
  String idParent;
  List<AssociationsCategory> subCategories;
  List<String> rootCat = [];

  factory Category.fromJson(Map<String, dynamic> json) => Category(
      id: json["id"],
      name: json["name"],
      levelDepth: json["level_depth"],
      idParent: json["id_parent"],
      subCategories: json["associations"] != null &&
              json["associations"]["categories"] != null
          ? List<AssociationsCategory>.from(json["associations"]["categories"]
              .map((x) => AssociationsCategory.fromJson(x)))
          : []);

  factory Category.empty() => Category(
      id: 0, name: '', levelDepth: '', idParent: '', subCategories: []);
}

class AssociationsCategory {
  AssociationsCategory({
    required this.id,
  });

  String id;

  factory AssociationsCategory.fromJson(Map<String, dynamic> json) =>
      AssociationsCategory(
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
      };
}
