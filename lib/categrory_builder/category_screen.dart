import 'dart:convert';

import 'package:applooki/add_product/add_product_details_screen.dart';
import 'package:applooki/add_product/add_product_view_model.dart';
import 'package:applooki/assets/colors.dart';
import 'package:applooki/assets/fonts.dart';
import 'package:applooki/categrory_builder/category_view_model.dart';
import 'package:applooki/shared/bottom_bar_custom.dart';
import 'package:applooki/shared/top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final addProductViewModel =
    ChangeNotifierProvider.autoDispose<AddProductViewModel>(
  (ref) => AddProductViewModel(),
);

final catViewModel = ChangeNotifierProvider.autoDispose<CatBuilderViewModel>(
  (ref) => CatBuilderViewModel(),
);

class CategortyBuilderFlutter extends StatefulWidget {
  const CategortyBuilderFlutter({Key? key}) : super(key: key);

  @override
  State<CategortyBuilderFlutter> createState() =>
      _CategortyBuilderFlutterState();
}

class _CategortyBuilderFlutterState extends State<CategortyBuilderFlutter> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      loadCategories();
    });
  }

  loadCategories() {
    final viewModel = context.read(catViewModel);
    viewModel.loadCategories();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(body: Consumer(builder: (context, watch, child) {
      final viewModel = watch(catViewModel);
      //  final viewModelTWo = watch(catViewModel);
      return Scaffold(
        bottomNavigationBar: viewModel.listtopsCategory.length == 0
            ? Container(
                height: 50,
              )
            : BottomBarCustom(
                labelText: "Suivant",
                onPressed: () async {
                  if (viewModel.selectedCategory.length > 0) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => AddProductDetailsScreen(
                            listCategory: viewModel.selectedCategory),
                      ),
                    );
                  }
                },
              ),
        body: viewModel.listtopsCategory.length == 0
            ? Container(
                height: size.height,
                width: double.infinity,
                child: Center(
                  child: CircularProgressIndicator(
                    color: primary_blue,
                  ),
                ),
              )
            : SafeArea(
                child: Container(
                padding: EdgeInsets.symmetric(horizontal: size.width * 0.075),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      TopBarWidget(
                        title: "Nouveau Article",
                      ),
                      Container(
                          height: size.height * 0.739,
                          width: size.width * 0.85,
                          child: ListView(
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Choisir votre catégorie"),
                                  CategoriesHorizentalBarWidget()
                                ],
                              ),
                              SizedBox(height: 20),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Choisir votre catégorie"),
                                  CategoriesVerticalBarWidget()
                                ],
                              )
                            ],
                          )),
                    ],
                  ),
                ),
              )),
      );

      /* ListView.builder(
        itemCount: viewModel.listCat.length,
        itemBuilder: (context, i) {
          return ListTile(
            title: Text(viewModel.listCat[i].name),
            trailing: viewModel.listCat[i].subCategories.length > 0
                ? Icon(Icons.arrow_forward)
                : Icon(Icons.radio),
            onTap: () {
              viewModel.editCat(viewModel.listCat[i].id);

              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => CategortyBuilderFlutter(),
                ),
              );
            },
          );
        },
      );*/
    }));
  }
}

class CategoriesHorizentalBarWidget extends StatelessWidget {
  const CategoriesHorizentalBarWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Consumer(builder: (context, watch, child) {
      final viewModel = watch(catViewModel);
      return Container(
          padding: EdgeInsets.symmetric(vertical: 5),
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: viewModel.listlevel2Category
                  .map(
                    (e) => Container(
                      padding: EdgeInsets.only(right: 10),
                      child: OutlinedButton(
                        child: Text(
                          e.name,
                          style: TextStyle(
                            fontSize: 15,
                            fontFamily: fontSemiBold,
                            color: viewModel.selectedTopParent?.id == e.id
                                ? primary_white
                                : primary_blue,
                          ),
                        ),
                        style: OutlinedButton.styleFrom(
                          primary: primary_blue,
                          backgroundColor:
                              viewModel.selectedTopParent?.id == e.id
                                  ? primary_blue
                                  : primary_white,
                          onSurface: primary_blue,
                          side: BorderSide(color: primary_blue, width: 1),
                          elevation: 0,
                          minimumSize: Size(size.width * 0.2, 40),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12)),
                        ),
                        onPressed: () {
                          viewModel.setTopSelected(e);
                        },
                      ),
                    ),
                  )
                  .toList(),
            ),
          ));
    });
  }
}

class CategoriesVerticalBarWidget extends StatelessWidget {
  const CategoriesVerticalBarWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Consumer(builder: (context, watch, child) {
      final viewModel = watch(catViewModel);
      return Container(
          padding: EdgeInsets.symmetric(vertical: 5),
          child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: viewModel.listCat
                      .map(
                        (subCat) => GestureDetector(
                            onTap: () {
                              /*if (subCat == viewModel.selectedCategory) {
                                print('exist');
                                //  onPress(_, "remove");
                              } else {
                                print('inexist');
                                //  onPress(_, "add");
                              }*/
                              subCat.subCategories.length == 0
                                  ? viewModel.setSelected(subCat)
                                  : viewModel.editCat(subCat.id, subCat.name);
                            },
                            child: Container(
                              margin: EdgeInsets.symmetric(vertical: 5),
                              width: size.width * 0.85,
                              height: 50,
                              padding: EdgeInsets.only(left: 15, right: 15),
                              decoration: BoxDecoration(
                                color: background_article_grey,
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(utf8.decode(subCat.name.runes.toList())),
                                  subCat.subCategories.length == 0
                                      ? Opacity(
                                          opacity: viewModel.selectedCategory
                                                  .contains(subCat)
                                              ? 1
                                              : 0,
                                          child: Container(
                                            width: 20,
                                            height: 20,
                                            child: Icon(
                                              Icons.check,
                                              size: 15,
                                              color: Colors.white,
                                            ),
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: primary_blue,
                                            ),
                                          ),
                                        )
                                      : Opacity(
                                          opacity: 1,
                                          child: Container(
                                            //width: 20,
                                            // height: 20,
                                            child: Icon(
                                              Icons.arrow_forward_ios,
                                              size: 25,
                                              color: primary_blue,
                                            ),
                                            /* decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: primary_blue,
                                            ),*/
                                          ),
                                        ),
                                ],
                              ),
                            )),
                      )
                      .toList())));
    });
  }
}
