import 'dart:convert';

import 'package:applooki/constant.dart';
import 'package:applooki/core/utf_helper.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'categories_responses.dart';

class CatBuilderViewModel extends ChangeNotifier {
  List<Category> listtopsCategory = [];
  List<Category> listlevel2Category = [];
  List<Category> listCat = [];
  Category? selectedTopParent;
  List<Category> selectedCategory = [];
  List<String> rootCat = [];

  loadCategories() async {
    final response = await http.get(
        Uri.parse(
          BASE_URL + "categories?output_format=JSON&display=full",
        ),
        headers: kheaders);
    var resp = json.decode(response.body);
    if (response.statusCode == 200) {
      listtopsCategory = Categories.fromJson(resp).categories;
      listlevel2Category = listtopsCategory
          .where((element) => element.levelDepth == "2")
          .toList();
      // listCat = listtopsCategory;
    }
    notifyListeners();
  }

  editCat(int cat, String categorieName,) {
    listCat = listtopsCategory
        .where((element) => element.idParent == cat.toString())
        .toList();
    rootCat.add(categorieName);
    print(rootCat);
    notifyListeners();
  }

  setTopSelected(Category category) {
    print("---------------------------");
    selectedTopParent = category;
    editCat(category.id, category.name);
    notifyListeners();
  }

  setSelected(Category category) {
    if (selectedCategory.contains(category)) {
      selectedCategory.remove(category);
    } else {
      print("root catégory ${rootCat.length}");
      category.rootCat = rootCat;
      selectedCategory.add(category);
    }
    notifyListeners();
  }
}
