import 'package:applooki/services/top_level_providers.dart';
import 'package:flutter/material.dart';
import 'bottom_bar.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class EnterPageScreen extends StatefulWidget {
  @override
  _EnterPageScreenState createState() => _EnterPageScreenState();
}

class _EnterPageScreenState extends State<EnterPageScreen> {
  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, watch, child) {
      final viewModel = watch(enterPageViewModel);
      return Scaffold(
        body: PageView(
          children: viewModel.pages,
          physics: NeverScrollableScrollPhysics(),
          controller: viewModel.pageController,
        ),
        bottomNavigationBar: BottomBar(),
        extendBody: true,
      );
    });
  }
}
