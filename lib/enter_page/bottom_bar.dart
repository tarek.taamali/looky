import 'package:applooki/add_product/add_product_details_screen.dart';
import 'package:applooki/assets/colors.dart';
import 'package:applooki/categrory_builder/category_screen.dart';
import 'package:applooki/panier/panier_screen.dart';
import 'package:applooki/services/top_level_providers.dart';
import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class BottomBar extends StatelessWidget {
  const BottomBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final viewModel = context.read(enterPageViewModel);
    return Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(20), topLeft: Radius.circular(20)),
          boxShadow: [
            BoxShadow(color: Colors.black12, spreadRadius: 0, blurRadius: 10),
          ],
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.0),
            topRight: Radius.circular(20.0),
          ),
          child: BottomAppBar(
              child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              IconButton(
                icon: Icon(
                  viewModel.selectedIndex == 0
                      ? Icons.home
                      : Icons.home_outlined,
                  size: 30.0,
                  color: primary_blue,
                ),
                onPressed: () {
                  viewModel.updateTabSelection(0, context);
                },
              ),
              IconButton(
                icon: Icon(
                  viewModel.selectedIndex == 1
                      ? Icons.favorite
                      : Icons.favorite_border,
                  size: 30.0,
                  color: primary_blue,
                ),
                onPressed: () {
                  viewModel.updateTabSelection(1, context);
                },
              ),
              Container(
                width: 35,
                height: 35,
                decoration: BoxDecoration(
                    color: primary_blue,
                    borderRadius: BorderRadius.circular(8)),
                child: Center(
                  child: IconButton(
                    icon: Icon(
                      Icons.add,
                      size: 20.0,
                      color: primary_white,
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => CategortyBuilderFlutter(),
                        ),
                      );
                      //AddProductDetailsScreen
                    },
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => PanierScreen()));
                },
                child: Badge(
                  badgeColor: Colors.transparent.withOpacity(0.001),
                  elevation: 0,
                  position: BadgePosition.topEnd(top: -10, end: -17),
                  badgeContent: Text('',
                      style: TextStyle(
                          color: primary_blue, fontWeight: FontWeight.bold)),
                  child: Icon(
                    viewModel.selectedIndex == 3
                        ? Icons.shopping_bag_rounded
                        : Icons.shopping_bag_outlined,
                    size: 30.0,
                    color: primary_blue,
                  ),
                ),
              ),
              IconButton(
                icon: Icon(
                  viewModel.selectedIndex == 4
                      ? Icons.person
                      : Icons.person_outline,
                  size: 25.0,
                  color: primary_blue,
                ),
                onPressed: () {
                  viewModel.updateTabSelection(4, context);
                },
              ),
            ],
          )),
        ));
  }
}
