import 'package:applooki/Utils/local_storage.dart';
import 'package:applooki/auth/sign_in_screen.dart';
import 'package:applooki/categrory_builder/category_screen.dart';
import 'package:applooki/home/home_screen.dart';
import 'package:applooki/favoris/favoris_screen.dart';
import 'package:applooki/panier/panier_screen.dart';
import 'package:applooki/profile/profile_screen.dart';
import 'package:flutter/material.dart';

class EnterViewModel extends ChangeNotifier {
  late GlobalKey<NavigatorState> navigatorKey;
  List<Widget> pages = [
    HomeScreen(),
    FavorisScreen(),
    CategortyBuilderFlutter(),
    PanierScreen(),
    ProfilScreen(),
    SignInScreen()
  ];
  int selectedIndex = 0;
  var pageController = PageController(initialPage: 0);

  void updateTabSelection(int index, BuildContext context) {
    selectedIndex = index;
    if (index == 4) {
      checkStatusAuth(context);
    } else {
      pageController.jumpToPage(index);
    }
    notifyListeners();
  }

  void setIndex(int index, GlobalKey<NavigatorState> navigatorKey) {
    selectedIndex = index;
    pageController.jumpToPage(index);
    this.navigatorKey = navigatorKey;
    notifyListeners();
  }

  checkStatusAuth(BuildContext context) {
    String isAuth = StorageUtil.getString("connected");
    if (isAuth == 'connected') {
      pageController.jumpToPage(4);
    } else {
      pageController.jumpToPage(5);
      /*Navigator.of(context).pushNamedAndRemoveUntil(
        'login',
        (route) => false,
      );*/
    }
  }
}
