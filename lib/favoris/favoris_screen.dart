import 'package:applooki/assets/colors.dart';
import 'package:applooki/assets/images_links.dart';
import 'package:applooki/services/top_level_providers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/svg.dart';
import '../assets/fonts.dart';
import 'package:applooki/home/home_screen.dart';

class FavorisScreen extends StatefulWidget {
  @override
  _FavorisScreenState createState() => _FavorisScreenState();
}

class _FavorisScreenState extends State<FavorisScreen> {
  int val = 1;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final double itemHeight = 189.0;
    final double itemWidth = 146.0;
    return Scaffold(
        backgroundColor: primary_white,
        body: SafeArea(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: size.width * 0.075),
            child: SingleChildScrollView(
              child: Consumer(builder: (context, watch, child) {
                final viewModel = watch(favorisViewModel);
                return Column(children: [
                  Container(
                    height: size.height * 0.1,
                    width: size.width,
                    child: Center(
                      child: Text(
                        "Favoris",
                        style: TextStyle(
                          fontSize: 15.0,
                          color: primary_blue,
                          fontFamily: fontBold,
                        ),
                      ),
                    ),
                  ),
                  viewModel.listProduits.length == 0
                      ? Column(
                          children: [
                            Container(
                              width: size.width * 0.5,
                              margin: EdgeInsets.only(
                                  top: size.height * 0.05,
                                  bottom: size.height * 0.075),
                              decoration: BoxDecoration(
                                color: Colors.white,
                              ),
                              child: SvgPicture.asset("images/ic_looki.svg"),
                            ),
                            Container(
                              width: size.width * 0.85,
                              decoration: BoxDecoration(
                                color: Colors.white,
                              ),
                              child: Column(
                                children: [
                                  Image.asset(favorisVide),
                                  SizedBox(
                                    height: size.height * 0.03,
                                  ),
                                  Text(
                                    'Votre liste de favoris est vide',
                                    style: TextStyle(
                                      fontSize: 18.0,
                                      fontFamily: fontSemiBold,
                                      color: primary_black,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        )
                      : GridView.builder(
                          padding: EdgeInsets.only(top: 5, bottom: 10),
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                                  childAspectRatio: (itemWidth / itemHeight),
                                  crossAxisCount: 2,
                                  crossAxisSpacing: 10,
                                  mainAxisSpacing: 20),
                          shrinkWrap: true,
                          itemCount: viewModel.listProduits.length,
                          itemBuilder: (context, index) {
                            return buildItemCard(
                                context, viewModel.listProduits[index], size);
                          },
                        ),
                ]);
              }),
            ),
          ),
        ));
  }
}
