import 'dart:convert';

import 'package:applooki/Utils/local_storage.dart';
import 'package:applooki/home/products_response.dart';
import 'package:applooki/models/customers.dart';
import 'package:flutter/material.dart';

class FavorisViewModel extends ChangeNotifier {
  List<Product> listProduits = [];
  List<Product> listProduits1 = [];
  bool isFavoris(Product product) => listProduits.contains(product);

  FavorisViewModel() {
    getProductsFromStroage();
  }

  addOrRemoveProductToFavoris(BuildContext context, Product product) {
    if (listProduits.contains(product)) {
      listProduits.removeWhere((element) => element.id == product.id);
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Cet article à été supprimer à vos favoris'),
      ));
    } else {
      listProduits.add(product);
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Cet article à été ajouter à vos favoris'),
      ));
    }
    saveProductsFromStroage();
    notifyListeners();
  }

  saveProductsFromStroage() async {
    StorageUtil.putString("listProduits",
        json.encode(List<dynamic>.from(listProduits.map((x) => x.toJson()))));
  }

  getProductsFromStroage() async {
    if (StorageUtil.containsKey("listProduits")) {
      var json1 = json.decode(StorageUtil.getString("listProduits"));
      try {
        listProduits = List<Product>.from(json1.map((x) {
          return Product.fromJsonLocal(x);
        }));
      } catch (error) {
        print(error.toString());
      }
    }
  }
}
