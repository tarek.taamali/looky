import 'package:applooki/assets/colors.dart';
import 'package:applooki/assets/fonts.dart';
import 'package:flutter/material.dart';

class BottomBarCustom extends StatelessWidget {
  final Function()? onPressed;
  final String labelText;
  const BottomBarCustom({
    Key? key,
    this.onPressed,
    this.labelText = "Enregistrer",
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return BottomAppBar(
      elevation: 0.0,
      child: Container(
        height: 60,
        padding:
            EdgeInsets.symmetric(horizontal: size.width * 0.07, vertical: 5),
        child: TextButton(
          onPressed: onPressed,
          style: TextButton.styleFrom(
            backgroundColor: primary_blue,
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(12),
              ),
            ),
          ),
          child: Text(
            labelText,
            style: const TextStyle(
              fontSize: 16.0,
              fontFamily: fontBold,
              color: primary_white,
            ),
          ),
        ),
      ),
    );
  }
}
