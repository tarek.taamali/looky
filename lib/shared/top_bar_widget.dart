import 'package:applooki/assets/colors.dart';
import 'package:applooki/assets/fonts.dart';
import 'package:flutter/material.dart';

class TopBarWidget extends StatelessWidget {
  final String title;
  final Widget? actionIcon;
  const TopBarWidget({Key? key, this.title = '', this.actionIcon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Container(
      height: size.height * 0.1,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: 35.0,
            height: 35.0,
            decoration: BoxDecoration(
              color: back_input_search,
              borderRadius: BorderRadius.circular(9.0),
            ),
            child: Center(
              child: IconButton(
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: primary_blue,
                  size: 14,
                ),
                onPressed: () => Navigator.pop(context),
              ),
            ),
          ),
          Text(
            title,
            style: TextStyle(
              fontSize: 15.0,
              color: primary_blue,
              fontFamily: fontBold,
            ),
          ),
          Visibility(
            visible: actionIcon == null ? false : true,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: back_input_search,
              ),
              constraints: BoxConstraints(minHeight: 35.0, minWidth: 35.0),
              child: actionIcon ??
                  Icon(
                    Icons.favorite_border,
                    color: red_favoris,
                  ),
            ),
          ),
        ],
      ),
    );
  }
}
