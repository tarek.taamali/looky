import 'package:applooki/Utils/utils.dart';
import 'package:applooki/home/categories_responses.dart';
import 'package:applooki/home/home_view_model.dart';
import 'package:applooki/assets/colors.dart';
import 'package:applooki/assets/fonts.dart';
import 'package:applooki/services/top_level_providers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:skeleton_loader/skeleton_loader.dart';

class CategoriesBarWidget extends StatelessWidget {
  final Function(Category) onPress;
  const CategoriesBarWidget({Key? key, required this.onPress})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Consumer(builder: (context, watch, child) {
      final viewModel = watch(menuViewModel);
      return Container(
        padding: EdgeInsets.symmetric(vertical: 5),
        child: viewModel.filterCategoris.length > 0
            ? SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: viewModel.filterCategoris
                      .map(
                        (e) => buildButton(e, viewModel, size),
                      )
                      .toList(),
                ))
            : SkeletonLoader(
                builder: Container(
                    child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        for (int i = 0; i < 4; i++)
                          buildButton(null, viewModel, size),
                      ]),
                )),
              ),
      );
    });
  }

  Widget buildButton(Category? e, MenuViewModel viewModel, Size size) {
    return Container(
      padding: EdgeInsets.only(right: 10),
      child: OutlinedButton(
        child: Text(
          e == null ? '' : toUpperCase(e.name),
          style: TextStyle(
            fontSize: 15,
            fontFamily: fontSemiBold,
            color: e == null
                ? primary_white
                : viewModel.selectedCategory.id == e.id
                    ? primary_white
                    : primary_blue,
          ),
        ),
        style: OutlinedButton.styleFrom(
          primary: primary_blue,
          backgroundColor: e == null
              ? primary_white
              : viewModel.selectedCategory.id == e.id
                  ? primary_blue
                  : primary_white,
          onSurface: primary_blue,
          side: BorderSide(color: primary_blue, width: 1),
          elevation: 0,
          minimumSize: Size(size.width * 0.2, 40),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
        ),
        onPressed: () {
          if (e != null) {
            onPress(e);
          }
        },
      ),
    );
  }
}
