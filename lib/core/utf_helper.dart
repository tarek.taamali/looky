import 'dart:convert';

///
/// String convert UTF8
///
String toDecodedString(String text) {
  return utf8.decode(text.runes.toList());
}
