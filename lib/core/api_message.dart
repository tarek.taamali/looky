class ErrorResponse {
  ErrorResponse({
    required this.errors,
  });

  List<ApiMessage> errors;

  factory ErrorResponse.fromJson(Map<String, dynamic> json) => ErrorResponse(
        errors: List<ApiMessage>.from(
            json["errors"].map((x) => ApiMessage.fromJson(x))),
      );
}

class ApiMessage {
  ApiMessage({
    required this.code,
    required this.message,
  });

  int code;
  String message;

  factory ApiMessage.fromJson(Map<String, dynamic> json) => ApiMessage(
        code: json["code"],
        message: json["message"],
      );
}
