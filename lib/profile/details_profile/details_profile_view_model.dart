import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:applooki/auth/sign_in_screen.dart';
import 'package:crypto/crypto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:applooki/Utils/local_storage.dart';
import 'package:applooki/assets/elements.dart';
import 'package:http/http.dart' as http;
import '../../constant.dart';

class DetailsProfileNotifier extends ChangeNotifier {
  final url = BASE_URL + "customers";

  logoutUser(BuildContext context) {
    Loader.show(
      context,
      progressIndicator: LoaderWidget(context),
    );
    Future.delayed(new Duration(seconds: 3), () {
      Loader.hide();
      logout(context);
    });
  }

  logout(BuildContext context) async {
    deleteUser();
    await logoutFacebok();
    navigateToNextPage(context);
  }

  deleteUser() {
    StorageUtil.putInt("cu", 0);
  }

  navigateToNextPage(BuildContext context) {
    //Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => HomePage()));
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => SignInScreen()),
        (Route<dynamic> route) => false);
  }

  logoutFacebok() async {}

  String sha1RandomString() {
    final randomNumber = Random().nextDouble();
    final randomBytes = utf8.encode(randomNumber.toString());
    final randomString = sha1.convert(randomBytes).toString();
    return randomString;
  }

  updateProfile(
      {required BuildContext context,
      required String id,
      required firstName,
      required lastName,
      required String email,
      required String password}) async {
    Loader.show(
      context,
      progressIndicator: LoaderWidget(context),
    );
    print(sha1RandomString());
    String passwd = password.isEmpty
        ? ""
        : "<passwd>$password</passwd>\n" + "<pass>$password</pass>\n";
    String strRequestBody = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
        "<prestashop xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n" +
        "    <customer>\n" +
        "        <id>$id</id>\n" +
        passwd +
        "        <lastname>$lastName</lastname>\n" +
        "        <active>1</active>\n" +
        "        <firstname>$firstName</firstname>\n" +
        "        <email>$email</email>\n" +
        "        <id_shop>1</id_shop>\n" +
        "        <active>1</active>\n" +
        "        <id_shop_group>1</id_shop_group>\n" +
        "        <id_gender>0</id_gender>\n" +
        "        <id_default_group>1</id_default_group>\n" +
        "        <id_lang>2</id_lang>\n" +
        "  <secure_key>${md5.convert(utf8.encode(firstName + lastName)).toString()}</secure_key>\n" +
        "    </customer>\n" +
        "</prestashop>";

    String path = url + "?output_format=JSON";
    try {
      var response = await http.put(Uri.parse(path),
          headers: kheaders, body: strRequestBody);
      print("------------------");
      print(response.body.toString());
      if (response.statusCode == 200 || response.statusCode == 201) {
        Loader.hide();
        showDialog(
            context: context,
            builder: (context) {
              return PopupSuccess(
                  context, "Succès", "Votre compte a été créé avec succès !",
                  () {
                Navigator.of(context).pop();
              });
            });
      } else {
        Loader.hide();
        showDialog(
            context: context,
            builder: (context) {
              return PopupEchec(context, "Attention",
                  "Il semble qu'il a y eu un problème !", () {});
            });
      }
    } on SocketException {
      Loader.hide();
      showDialog(
          context: context,
          builder: (context) {
            return PopupEchec(context, 'Attention',
                "Merci de vérifier votre connexion réseau.", () {});
          });
    } on HttpException {
      Loader.hide();
      showDialog(
          context: context,
          builder: (context) {
            return PopupEchec(context, 'Attention',
                "Il semble qu'il a y eu un problème.", () {});
          });
    } on FormatException {
      Loader.hide();
      showDialog(
          context: context,
          builder: (context) {
            return PopupEchec(context, 'Attention',
                "Il semble qu'il a y eu un problème.", () {});
          });
    }
  }
}
