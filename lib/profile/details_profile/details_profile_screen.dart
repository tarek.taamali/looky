import 'package:applooki/Utils/strings.dart';
import 'package:applooki/Utils/utils.dart';
import 'package:applooki/assets/custom_password_form_field.dart';
import 'package:applooki/assets/custom_text_field.dart';
import 'package:applooki/assets/images_links.dart';
import 'package:applooki/models/customers.dart';
import 'package:applooki/shared/bottom_bar_custom.dart';
import 'package:applooki/shared/top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:applooki/assets/colors.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'details_profile_view_model.dart';

final profilViewModel = ChangeNotifierProvider<DetailsProfileNotifier>(
  (ref) => DetailsProfileNotifier(),
);

class DetailsCompteScreen extends StatefulWidget {
  final Customer customer;

  const DetailsCompteScreen({Key? key, required this.customer})
      : super(key: key);

  @override
  _DetailsCompteScreenState createState() => _DetailsCompteScreenState();
}

class _DetailsCompteScreenState extends State<DetailsCompteScreen> {
  final _formKey = GlobalKey<FormState>();
  bool _showPassword = false;

  var firstNameController = TextEditingController();
  var lastNameController = TextEditingController();
  var emailController = TextEditingController();
  var passwordController = TextEditingController();
  var numberController = TextEditingController();

  var firstNameFocusNode = FocusNode();
  var lastNameFocusNode = FocusNode();
  var emailFocusNode = FocusNode();
  var passwordFocusNode = FocusNode();
  var numberFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    firstNameController.text = widget.customer.firstname;
    lastNameController.text = widget.customer.lastname;
    emailController.text = widget.customer.email;
    passwordController.text = "";
  }

  @override
  Widget build(BuildContext context) {
    final viewModel = context.read(profilViewModel);

    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: primary_white,
      bottomNavigationBar: Padding(
        padding: EdgeInsets.only(bottom: 10),
        child: BottomBarCustom(
          onPressed: () {
            if (_formKey.currentState!.validate()) {
              viewModel.updateProfile(
                context: context,
                id: widget.customer.id.toString(),
                firstName: firstNameController.text,
                lastName: lastNameController.text,
                email: emailController.text,
                password: passwordController.text,
              );
            }
          },
        ),
      ),
      body: _buildBody(size),
    );
  }

  Widget _buildBody(Size size) {
    return SafeArea(
      child: Container(
        height: size.height,
        padding: EdgeInsets.symmetric(
          horizontal: size.width * 0.075,
        ),
        child: ListView(
          children: [
            TopBarWidget(
              title: "Mon compte",
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            Center(
              child: Stack(
                children: [
                  Container(
                    width: 101.0,
                    height: 101.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.0),
                      image: DecorationImage(
                        image: AssetImage(
                          imgProfile,
                        ),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Container(
                    width: 101.0,
                    height: 101.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: Center(
                      child: Container(
                        width: 29.25,
                        height: 29.25,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: primary_blue,
                        ),
                        child: Icon(
                          Icons.add,
                          color: primary_white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: size.height * 0.03),
                  CustomFormField(
                    prefixIcon: Icon(
                      Icons.person,
                      color: background_blue,
                    ),
                    width: size.width * 0.85,
                    keyboardType: TextInputType.text,
                    hintText: lastNameLabel,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return requiredFieldLabel;
                      }
                      return null;
                    },
                    controller: firstNameController,
                  ),
                  SizedBox(height: size.height * 0.03),
                  CustomFormField(
                    prefixIcon: Icon(
                      Icons.person,
                      color: background_blue,
                    ),
                    width: size.width * 0.85,
                    keyboardType: TextInputType.text,
                    hintText: firstNameLabel,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return requiredFieldLabel;
                      }
                      return null;
                    },
                    controller: lastNameController,
                  ),
                  SizedBox(height: size.height * 0.03),
                  CustomFormField(
                    controller: emailController,
                    prefixIcon: Icon(
                      Icons.mail,
                      color: background_blue,
                    ),
                    width: size.width * 0.85,
                    keyboardType: TextInputType.emailAddress,
                    hintText: emailLabel,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return requiredFieldLabel;
                      }
                      if (!isValidEmail(value)) {
                        return emailNotValidLabel;
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: size.height * 0.03),
                  CustomPasswordField(
                    prefixIcon: Icon(
                      Icons.lock,
                      color: background_blue,
                    ),
                    width: size.width * 0.85,
                    controller: passwordController,
                    labelText: passwordLabel,
                    obscureText: !this._showPassword,
                    suffixIcon: IconButton(
                      icon: Icon(
                        _showPassword ? Icons.visibility : Icons.visibility_off,
                        color: this._showPassword ? primary_blue : Colors.grey,
                      ),
                      onPressed: () {
                        setState(() => _showPassword = !_showPassword);
                      },
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return requiredFieldLabel;
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: size.height * 0.03),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
