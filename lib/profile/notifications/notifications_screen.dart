import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:applooki/assets/fonts.dart';
import 'package:applooki/assets/colors.dart';

class NotificationScreen extends StatefulWidget {
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  int val=1;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: primary_white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: primary_white,
        title: Center(
          child: Container(
            width: size.width * 0.85,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: 35.0,
                  height: 35.0,
                  decoration: BoxDecoration(
                    color: back_input_search,
                    borderRadius: BorderRadius.circular(9.0),
                  ),
                  child: Center(
                    child: IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: primary_blue,
                        size: 14,
                      ),
                      onPressed: () => Navigator.pop(context),
                    ),
                  ),
                ),
                Text(
                  'Notifications',
                  style: TextStyle(
                    fontSize: 15.0,
                    color: primary_blue,
                    fontFamily: fontBold,
                  ),
                ),
                Text(''),
              ],
            ),
          ),
        ),
        centerTitle: true,
      ),
      body: Center(
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: size.width * 0.075,),
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(height: size.height * 0.05,),
                Container(
                  height: 83.0,
                  padding: EdgeInsets.all(15.0),
                  decoration: BoxDecoration(
                    color: primary_white,
                    borderRadius: BorderRadius.circular(15.0),
                    boxShadow:[
                      BoxShadow(
                        color: Colors.black.withOpacity(0.2), //color of shadow
                        spreadRadius: 2, //spread radius
                        blurRadius: 10, // blur radius
                        offset: Offset(0, 5), // changes position of shadow
                        //first paramerter of offset is left-right
                        //second parameter is top to down
                      ),
                      //you can set more BoxShadow() here
                    ],
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Icon(Icons.notifications, color: background_blue),
                          Container(
                            width: size.width * 0.55,
                            margin: EdgeInsets.only(left: 10.0),
                            child: Text('Votre annonce #098345 a été acceptée',
                              style: TextStyle(
                                fontFamily: fontMedium,
                                fontSize: 13.0,
                                color: primary_black,
                              ),),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text('14 Octobre 2021',
                            textAlign: TextAlign.right,
                            style: TextStyle(
                              fontSize: 10.0,
                              fontFamily: fontRegular,
                              color: txt_adresse,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(height: size.height * 0.03,),
                Container(
                  height: 83.0,
                  padding: EdgeInsets.all(15.0),
                  decoration: BoxDecoration(
                    color: primary_white,
                    borderRadius: BorderRadius.circular(15.0),
                    boxShadow:[
                      BoxShadow(
                        color: Colors.black.withOpacity(0.2), //color of shadow
                        spreadRadius: 2, //spread radius
                        blurRadius: 10, // blur radius
                        offset: Offset(0, 5), // changes position of shadow
                        //first paramerter of offset is left-right
                        //second parameter is top to down
                      ),
                      //you can set more BoxShadow() here
                    ],
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Icon(Icons.notifications, color: background_blue),
                          Container(
                            width: size.width * 0.55,
                            margin: EdgeInsets.only(left: 10.0),
                            child: Text('Votre annonce #098345 a été acceptée',
                              style: TextStyle(
                                fontFamily: fontMedium,
                                fontSize: 13.0,
                                color: primary_black,
                              ),),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text('14 Octobre 2021',
                            textAlign: TextAlign.right,
                            style: TextStyle(
                              fontSize: 10.0,
                              fontFamily: fontRegular,
                              color: txt_adresse,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(height: size.height * 0.03,),
              ],
            )
          ],
        ),
      ),
    );
  }
}
