import 'dart:ui';
import 'package:applooki/assets/elements.dart';
import 'package:applooki/assets/fonts.dart';
import 'package:applooki/profile/profile_view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:applooki/assets/colors.dart';
import 'package:applooki/profile/paiement/paiement_screen.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/svg.dart';
import 'a_propos.dart';
import 'adresses/adresses_screen.dart';
import 'details_profile/details_profile_screen.dart';
import 'historique/historique_screen.dart';
import 'notifications/notifications_screen.dart';
import 'package:applooki/Utils/local_storage.dart';

final profilViewModel = ChangeNotifierProvider.autoDispose<ProfileViewModel>(
  (ref) => ProfileViewModel(),
);

class ProfilScreen extends StatefulWidget {
  _ProfilScreenState createState() => _ProfilScreenState();
}

class _ProfilScreenState extends State<ProfilScreen> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      final viewModel = context.read(profilViewModel);
      viewModel.getProfile();
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: primary_white,
      body: SafeArea(
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: size.width * 0.075),
          children: [
            Center(
              child: Container(
                constraints: BoxConstraints(
                  minHeight: MediaQuery.of(context).size.height,
                ),
                color: primary_white,
                child: Consumer(builder: (context, watch, child) {
                  final _viewModel = watch(profilViewModel);
                  return Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: size.height * 0.05),
                        child: Text(
                          'Mon compte',
                          style: TextStyle(
                            fontSize: 15.0,
                            color: primary_blue,
                            fontFamily: fontBold,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: size.height * 0.03,
                      ),
                      Container(
                        width: 75.0,
                        height: 75.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15.0),
                          border: Border.all(width: 2, color: primary_blue),
                        ),
                        child: SvgPicture.asset(
                          "images/ic_looki.svg",
                        ),
                      ),
                      SizedBox(
                        height: size.height * 0.017,
                      ),
                      Text(
                        '${_viewModel.customer.firstname} ${_viewModel.customer.lastname}',
                        style: TextStyle(
                          fontSize: 15.0,
                          fontFamily: fontBold,
                          color: primary_black,
                        ),
                      ),
                      SizedBox(
                        height: size.height * 0.017,
                      ),
                      Text(
                        _viewModel.customer.email,
                        style: TextStyle(
                          fontSize: 13.0,
                          fontFamily: fontRegular,
                          color: primary_black,
                        ),
                      ),
                      SizedBox(
                        height: size.height * 0.05,
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => DetailsCompteScreen(
                                customer: _viewModel.customer,
                              ),
                            ),
                          );
                        },
                        child: Container(
                          height: 36.0,
                          color: primary_white,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: <Color>[
                                      primary_blue,
                                      primary_blue.withOpacity(0.67),
                                    ],
                                  ),
                                  borderRadius: BorderRadius.circular(7),
                                ),
                                width: size.width * 0.1,
                                height: size.width * 0.1,
                                child: Icon(
                                  Icons.person,
                                  color: primary_white,
                                ),
                              ),
                              Container(
                                width: size.width * 0.6,
                                child: Text(
                                  'Profile',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 15.0,
                                    fontFamily: fontSemiBold,
                                    color: primary_blue,
                                  ),
                                ),
                              ),
                              Container(
                                width: size.width * 0.04,
                                height: 36.0,
                                child: Icon(
                                  Icons.arrow_forward_ios,
                                  color: primary_blue,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: size.height * 0.03,
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => HistoriqueScreen(
                                customer: _viewModel.customer,
                              ),
                            ),
                          );
                        },
                        child: Container(
                          height: 36.0,
                          color: primary_white,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: <Color>[
                                      primary_blue,
                                      primary_blue.withOpacity(0.67),
                                    ],
                                  ),
                                  borderRadius: BorderRadius.circular(7),
                                ),
                                width: size.width * 0.1,
                                height: size.width * 0.1,
                                child: Icon(
                                  Icons.history,
                                  color: primary_white,
                                ),
                              ),
                              Container(
                                width: size.width * 0.6,
                                child: Text(
                                  'Historique',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 15.0,
                                    fontFamily: fontSemiBold,
                                    color: primary_blue,
                                  ),
                                ),
                              ),
                              Container(
                                width: size.width * 0.04,
                                height: 36.0,
                                child: Icon(
                                  Icons.arrow_forward_ios,
                                  color: primary_blue,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: size.height * 0.03,
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => PaiementScreen(),
                            ),
                          );
                        },
                        child: Container(
                          height: 36.0,
                          color: primary_white,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: <Color>[
                                      primary_blue,
                                      primary_blue.withOpacity(0.67),
                                    ],
                                  ),
                                  borderRadius: BorderRadius.circular(7),
                                ),
                                width: size.width * 0.1,
                                height: size.width * 0.1,
                                child: Icon(
                                  Icons.credit_card_rounded,
                                  color: primary_white,
                                ),
                              ),
                              Container(
                                width: size.width * 0.6,
                                child: Text(
                                  'Paiement',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 15.0,
                                    fontFamily: fontSemiBold,
                                    color: primary_blue,
                                  ),
                                ),
                              ),
                              Container(
                                width: size.width * 0.04,
                                height: 36.0,
                                child: Icon(
                                  Icons.arrow_forward_ios,
                                  color: primary_blue,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: size.height * 0.03,
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => AdresseScreen(),
                            ),
                          );
                        },
                        child: Container(
                          height: 36.0,
                          color: primary_white,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: <Color>[
                                      primary_blue,
                                      primary_blue.withOpacity(0.67),
                                    ],
                                  ),
                                  borderRadius: BorderRadius.circular(7),
                                ),
                                width: size.width * 0.1,
                                height: size.width * 0.1,
                                child: Icon(
                                  Icons.location_pin,
                                  color: primary_white,
                                ),
                              ),
                              Container(
                                width: size.width * 0.6,
                                child: Text(
                                  'Adresse de livraison',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 15.0,
                                    fontFamily: fontSemiBold,
                                    color: primary_blue,
                                  ),
                                ),
                              ),
                              Container(
                                width: size.width * 0.04,
                                height: 36.0,
                                child: Icon(
                                  Icons.arrow_forward_ios,
                                  color: primary_blue,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: size.height * 0.03,
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => NotificationScreen(),
                            ),
                          );
                        },
                        child: Container(
                          height: 36.0,
                          color: primary_white,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: <Color>[
                                      primary_blue,
                                      primary_blue.withOpacity(0.67),
                                    ],
                                  ),
                                  borderRadius: BorderRadius.circular(7),
                                ),
                                width: size.width * 0.1,
                                height: size.width * 0.1,
                                child: Icon(
                                  Icons.notifications,
                                  color: primary_white,
                                ),
                              ),
                              Container(
                                width: size.width * 0.6,
                                child: Text(
                                  'Notifications',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 15.0,
                                    fontFamily: fontSemiBold,
                                    color: primary_blue,
                                  ),
                                ),
                              ),
                              Container(
                                width: size.width * 0.04,
                                height: 36.0,
                                child: Icon(
                                  Icons.arrow_forward_ios,
                                  color: primary_blue,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: size.height * 0.03,
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => AproposScreen(),
                            ),
                          );
                        },
                        child: Container(
                          height: 36.0,
                          color: primary_white,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: <Color>[
                                      primary_blue,
                                      primary_blue.withOpacity(0.67),
                                    ],
                                  ),
                                  borderRadius: BorderRadius.circular(7),
                                ),
                                width: size.width * 0.1,
                                height: size.width * 0.1,
                                child: Icon(
                                  Icons.info,
                                  color: primary_white,
                                ),
                              ),
                              Container(
                                width: size.width * 0.6,
                                child: Text(
                                  'À propos',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 15.0,
                                    fontFamily: fontSemiBold,
                                    color: primary_blue,
                                  ),
                                ),
                              ),
                              Container(
                                width: size.width * 0.04,
                                height: 36.0,
                                child: Icon(
                                  Icons.arrow_forward_ios,
                                  color: primary_blue,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: size.height * 0.03,
                      ),
                      GestureDetector(
                        onTap: () {
                          showDialog(
                              context: context,
                              builder: (context) {
                                return alertWithTwoButton(
                                    context,
                                    'Déconnexion',
                                    "Êtes-vous sûr de vouloir vous déconnecter de notre application ?",
                                    'Oui',
                                    'Non', () async {
                                  Navigator.of(context).pop();
                                  await StorageUtil.putString('customer', '');
                                  await StorageUtil.putString(
                                      "connected", "disconnected");

                                  Navigator.of(context).pushNamedAndRemoveUntil(
                                      'login', (route) => false);
                                }, () {
                                  Navigator.of(context).pop();
                                });
                              });
                        },
                        child: Container(
                          height: 36.0,
                          color: primary_white,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: <Color>[
                                      primary_blue,
                                      primary_blue.withOpacity(0.67),
                                    ],
                                  ),
                                  borderRadius: BorderRadius.circular(7),
                                ),
                                width: size.width * 0.1,
                                height: size.width * 0.1,
                                child: Icon(
                                  Icons.logout,
                                  color: primary_white,
                                ),
                              ),
                              Container(
                                width: size.width * 0.6,
                                child: Text(
                                  'Déconnexion',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 15.0,
                                    fontFamily: fontSemiBold,
                                    color: primary_blue,
                                  ),
                                ),
                              ),
                              Container(
                                width: size.width * 0.04,
                                height: 36.0,
                                child: Icon(
                                  Icons.arrow_forward_ios,
                                  color: primary_blue,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  );
                }),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
