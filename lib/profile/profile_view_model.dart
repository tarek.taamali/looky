import 'dart:convert';

import 'package:applooki/Utils/local_storage.dart';
import 'package:applooki/models/customers.dart';
import 'package:flutter/cupertino.dart';

class ProfileViewModel extends ChangeNotifier {
  Customer customer = Customer(
    id: '',
    idDefaultGroup: '',
    idLang: '',
    secureKey: '',
    passwd: '',
    pass: '',
    lastname: '',
    firstname: '',
    email: '',
  );

  getProfile() {
    Map<String, dynamic> jsondatais =
        jsonDecode(StorageUtil.getString('customer'));
    customer = Customer.fromJson(jsondatais);
    notifyListeners();
  }
}
