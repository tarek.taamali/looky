import 'dart:ui';
import 'package:applooki/assets/fonts.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:applooki/assets/colors.dart';
import 'package:flutter_svg/svg.dart';

class AproposScreen extends StatefulWidget {
  _AproposScreenState createState() => _AproposScreenState();
}

class _AproposScreenState extends State<AproposScreen> {
  int indexPage = 0;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: primary_white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: primary_white,
        title: Center(
          child: Container(
            width: size.width * 0.85,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: 35.0,
                  height: 35.0,
                  decoration: BoxDecoration(
                    color: back_input_search,
                    borderRadius: BorderRadius.circular(9.0),
                  ),
                  child: Center(
                    child: IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: primary_blue,
                        size: 14,
                      ),
                      onPressed: () => Navigator.pop(context),
                    ),
                  ),
                ),
                Text(
                  'À propos',
                  style: TextStyle(
                    fontSize: 15.0,
                    color: primary_blue,
                    fontFamily: fontBold,
                  ),
                ),
                Text(''),
              ],
            ),
          ),
        ),
        centerTitle: true,
      ),
      body: Center(
        child: ListView(
          padding: EdgeInsets.symmetric(
            horizontal: size.width * 0.075,
          ),
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  height: size.height * 0.1,
                ),
                Container(
                  width: size.width * 0.45,
                  child: SvgPicture.asset("images/ic_looki.svg"),
                ),
                SizedBox(
                  height: size.height * 0.15,
                ),
                Text(
                  'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, '
                  'sed diam nonumy eirmod tempor invidunt ut labore et dolore '
                  'magna aliquyam erat, sed diam voluptua. At vero eos et accusam '
                  'et justo duo dolores et ea rebum. Stet clita kasd gubergren,'
                  ' no sea takimata sanctus est Lorem ipsum dolor sit amet. '
                  'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed '
                  'diam nonumy eirmod tempor invidunt ut labore et dolore magna '
                  'aliquyam erat, sed diam voluptua. At vero eos et accusam et '
                  'justo duo dolores et ea rebum. Stet clita kasd gubergren, no '
                  'sea takimata sanctus est Lorem ipsum dolor sit amet.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 13.0,
                    fontFamily: fontMedium,
                    color: primary_black,
                  ),
                ),
                SizedBox(
                  height: size.height * 0.05,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
