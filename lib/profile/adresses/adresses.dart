// To parse this JSON data, do
//
//     final adresseResponse = adresseResponseFromJson(jsonString);

import 'dart:convert';

AdresseResponse adresseResponseFromJson(String str) =>
    AdresseResponse.fromJson(json.decode(str));

String adresseResponseToJson(AdresseResponse data) =>
    json.encode(data.toJson());

class AdresseResponse {
  AdresseResponse({
    required this.addresses,
  });

  List<Address> addresses;

  factory AdresseResponse.fromJson(Map<String, dynamic> json) =>
      AdresseResponse(
        addresses: List<Address>.from(
            json["addresses"].map((x) => Address.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "addresses": List<dynamic>.from(addresses.map((x) => x.toJson())),
      };
}

class Address {
  Address({
    required this.id,
    required this.idCustomer,
    this.alias = "",
    this.address1 = "",
    this.address2 = "",
    this.postcode = "",
    this.city = "",
    this.phoneMobile = "",
  });

  int id;
  String idCustomer;
  String alias;
  String address1;
  String address2;
  String postcode;
  String city;
  String phoneMobile;

  factory Address.fromJson(Map<String, dynamic> json) => Address(
      id: json["id"],
      idCustomer: json["id_customer"],
      alias: json["alias"],
      address1: json["address1"],
      address2: json["address2"],
      postcode: json["postcode"].toString(),
      city: json["city"],
      phoneMobile: json["phone_mobile"]);

  Map<String, dynamic> toJson() => {
        "id": id,
        "id_customer": idCustomer,
        "alias": alias,
        "address1": address1,
        "address2": address2,
        "postcode": postcode,
        "city": city,
      };
}
