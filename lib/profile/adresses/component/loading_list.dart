import 'package:applooki/assets/colors.dart';
import 'package:applooki/assets/fonts.dart';
import 'package:flutter/material.dart';
import 'package:skeleton_loader/skeleton_loader.dart';

class LoadingList extends StatelessWidget {
  const LoadingList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 5),
      child: SingleChildScrollView(
        child: SkeletonLoader(
          builder: Column(children: [
            _buildCard(),
            _buildCard(),
            _buildCard(),
            _buildCard(),
            _buildCard(),
          ]),
        ),
      ),
    );
  }

  Widget _buildCard() {
    return Container(
      height: 120.0,
      margin: EdgeInsets.only(bottom: 10),
      padding: EdgeInsets.all(15.0),
      decoration: BoxDecoration(
        color: background_article_grey,
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Maison',
                style: TextStyle(
                  fontFamily: fontSemiBold,
                  fontSize: 16.0,
                  color: primary_black,
                ),
              ),
              GestureDetector(
                child: Icon(
                  Icons.edit_rounded,
                  color: primary_blue,
                ),
              )
            ],
          ),
          Text(
            '0227485165',
            style: TextStyle(
              fontSize: 13.0,
              fontFamily: fontRegular,
              color: txt_adresse,
            ),
          ),
          Text(
            '325 Mansoura, Hay Aljamaa 122 Baker',
            style: TextStyle(
              fontSize: 13.0,
              fontFamily: fontRegular,
              color: txt_adresse,
            ),
          ),
        ],
      ),
    );
  }
}
