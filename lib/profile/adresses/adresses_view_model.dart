import 'package:applooki/Utils/local_storage.dart';
import 'package:applooki/Utils/utils.dart';
import 'package:applooki/assets/elements.dart';
import 'package:applooki/models/customers.dart';
import 'package:applooki/profile/adresses/adresses.dart';
import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../../constant.dart';

class AddresseViewModel extends ChangeNotifier {
  List<Address> adresses = [];
  Status loadData = Status.none;
  bool showLoader = false;

  loadAddresses() async {
    loadData = Status.loading;
    this.showLoader = showLoader;
    notifyListeners();
    Map<String, dynamic> jsondatais =
        jsonDecode(StorageUtil.getString('customer'));
    var customer = Customer.fromJson(jsondatais);
    final response = await http.get(
        Uri.parse(
          BASE_URL +
              'addresses?output_format=JSON&display=full&filter[id_customer]=${customer.id}',
        ),
        headers: kheaders);
    print(
        "addresses?output_format=JSON&display=full&filter[id_customer]=${customer.id}");
    print(response.body);
    loadData = Status.loaded;
    if (response.statusCode == 200) {
      if (response.body.toString() == "[]") {
      } else {
        var resp = json.decode(response.body);
        print(resp);
        adresses = AdresseResponse.fromJson(resp).addresses;
      }
    }
    notifyListeners();
  }

  void createAdresse({
    required BuildContext context,
    required String adress1,
    required String adress2,
    required String country,
    required String zipCode,
    required String mobile,
    required String alias,
  }) async {
    Loader.show(
      context,
      progressIndicator: LoaderWidget(context),
    );
    Map<String, dynamic> jsondatais =
        jsonDecode(StorageUtil.getString('customer'));
    var customer = Customer.fromJson(jsondatais);
    String path = BASE_URL + "addresses?output_format=JSON";
    try {
      var response = await http.post(
        Uri.parse(path),
        headers: kheaders,
        body: buildCreateAdresse(
          idCustomer: customer.id,
          lastName: customer.lastname,
          firstName: customer.firstname,
          adress1: adress1,
          adress2: adress2,
          country: country,
          zipCode: zipCode,
          mobile: mobile,
          alias: alias,
        ),
      );
      //var resp = json.decode(response.body);
      print("response \n : ${response.body}");
      if (response.statusCode == 201) {
        loadAddresses();
        // var result = json.decode(response.body);
        Loader.hide();
        Navigator.pop(context);
      }
      Loader.hide();
    } catch (error) {
      print("response :\n $error}");
      Loader.hide();
    }
  }

  void editAdresse({
    required BuildContext context,
    required String id,
    required String adress1,
    required String adress2,
    required String country,
    required String zipCode,
    required String mobile,
    required String alias,
  }) async {
    Loader.show(
      context,
      progressIndicator: LoaderWidget(context),
    );
    Map<String, dynamic> jsondatais =
        jsonDecode(StorageUtil.getString('customer'));
    var customer = Customer.fromJson(jsondatais);
    String path = BASE_URL + "addresses?output_format=JSON";
    try {
      var response = await http.put(
        Uri.parse(path),
        headers: kheaders,
        body: buildEditAdresse(
          id: id,
          idCustomer: customer.id,
          lastName: customer.lastname,
          firstName: customer.firstname,
          adress1: adress1,
          adress2: adress2,
          country: country,
          zipCode: zipCode,
          mobile: mobile,
          alias: alias,
        ),
      );
      //var resp = json.decode(response.body);
      print("response \n : ${response.body}");
      if (response.statusCode == 200) {
        loadAddresses();
        // var result = json.decode(response.body);
        Loader.hide();
        Navigator.pop(context);
      }
      Loader.hide();
    } catch (error) {
      print("response :\n $error}");
      Loader.hide();
    }
  }

  String buildCreateAdresse({
    required String idCustomer,
    required String lastName,
    required String firstName,
    required String adress1,
    required String adress2,
    required String country,
    required String zipCode,
    required String mobile,
    required String alias,
  }) {
    return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
        "<prestashop xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n" +
        "    <adresse>\n" +
        "        <id_country>8</id_country>\n" +
        "        <id_state>0</id_state>\n" +
        "        <id_customer>$idCustomer</id_customer>\n" +
        "        <lastname>$lastName</lastname>\n" +
        "        <firstname>$firstName</firstname>\n" +
        "        <alias>$alias</alias>\n" +
        "        <company></company>\n" +
        "        <vat_number></vat_number>\n" +
        "        <address1>$adress1</address1>\n" +
        "        <address2>$adress2</address2>\n" +
        "        <postcode>$zipCode</postcode>\n" +
        "        <city>$country</city>\n" +
        "        <phone></phone>\n" +
        "        <phone_mobile>$mobile</phone_mobile>\n" +
        "    </adresse>\n" +
        "</prestashop>";
  }

  String buildEditAdresse({
    required String id,
    required String idCustomer,
    required String lastName,
    required String firstName,
    required String adress1,
    required String adress2,
    required String country,
    required String zipCode,
    required String mobile,
    required String alias,
  }) {
    return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
        "<prestashop xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n" +
        "    <adresse>\n" +
        "        <id_country>8</id_country>\n" +
        "        <id_state>0</id_state>\n" +
        "        <id>$id</id>\n" +
        "        <id_customer>$idCustomer</id_customer>\n" +
        "        <lastname>$lastName</lastname>\n" +
        "        <firstname>$firstName</firstname>\n" +
        "        <alias>$alias</alias>\n" +
        "        <company></company>\n" +
        "        <vat_number></vat_number>\n" +
        "        <address1>$adress1</address1>\n" +
        "        <address2>$adress2</address2>\n" +
        "        <postcode>$zipCode</postcode>\n" +
        "        <city>$country</city>\n" +
        "        <phone></phone>\n" +
        "        <phone_mobile>$mobile</phone_mobile>\n" +
        "    </adresse>\n" +
        "</prestashop>";
  }
}
