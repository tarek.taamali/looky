import 'package:applooki/Utils/utils.dart';
import 'package:applooki/shared/top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:applooki/assets/fonts.dart';
import 'package:applooki/assets/colors.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'add_adresse_screen.dart';
import 'adresses.dart';
import 'adresses_view_model.dart';
import 'component/loading_list.dart';
import 'edit_adresse_screen.dart';

final addresseViewModel = ChangeNotifierProvider<AddresseViewModel>(
  (ref) => AddresseViewModel(),
);

class AdresseScreen extends StatefulWidget {
  @override
  _AdresseScreenState createState() => _AdresseScreenState();
}

class _AdresseScreenState extends State<AdresseScreen> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      final viewModel = context.read(addresseViewModel);
      viewModel.loadAddresses();
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: primary_white,
      bottomNavigationBar: BottomAppBar(
        elevation: 0.0,
        child: Container(
          width: size.width,
          height: 48.0,
          decoration: BoxDecoration(
            color: primary_blue,
            borderRadius: BorderRadius.circular(12),
          ),
          margin: EdgeInsets.only(
            bottom: 20.0,
            left: MediaQuery.of(context).size.width * 0.075,
            right: MediaQuery.of(context).size.width * 0.075,
          ),
          child: TextButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => AddAdresseScreen()));
              },
              child: Text(
                "Ajouter une adresse",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 16.0,
                  fontFamily: fontBold,
                  color: primary_white,
                ),
              )),
        ),
      ),
      body: SafeArea(
        child: Container(
          width: size.width,
          padding: EdgeInsets.symmetric(horizontal: size.width * 0.075),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TopBarWidget(
                title: 'Adresse de livraison',
              ),
              Expanded(
                child: Consumer(
                  builder: (context, watch, child) {
                    final viewModel = watch(addresseViewModel);
                    return Container(
                      child: SingleChildScrollView(
                        child: viewModel.adresses.length == 0 &&
                                viewModel.loadData == Status.loading
                            ? LoadingList()
                            : Column(
                                children: viewModel.adresses
                                    .map((e) => _buildCard(e))
                                    .toList(),
                              ),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildCard(Address address) {
    return Container(
      height: 120.0,
      margin: EdgeInsets.only(bottom: 10),
      padding: EdgeInsets.all(15.0),
      decoration: BoxDecoration(
        color: background_article_grey,
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                address.alias,
                style: TextStyle(
                  fontFamily: fontSemiBold,
                  fontSize: 16.0,
                  color: primary_black,
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) =>
                          EditAdresseScreen(address: address)));
                },
                child: Icon(
                  Icons.edit_rounded,
                  color: primary_blue,
                ),
              )
            ],
          ),
          Text(
            address.phoneMobile,
            style: TextStyle(
              fontSize: 13.0,
              fontFamily: fontRegular,
              color: txt_adresse,
            ),
          ),
          Text(
            address.address1,
            style: TextStyle(
              fontSize: 13.0,
              fontFamily: fontRegular,
              color: txt_adresse,
            ),
          ),
        ],
      ),
    );
  }
}
