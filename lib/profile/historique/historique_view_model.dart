import 'dart:convert';

import 'package:applooki/Utils/local_storage.dart';
import 'package:applooki/Utils/utils.dart';
import 'package:applooki/home/products_response.dart';
import 'package:applooki/models/customers.dart';
import 'package:applooki/profile/historique/orders_responses.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

import '../../constant.dart';

class HistoriqueViewModel extends ChangeNotifier {
  List<Product> listMenu = [];
  List<Order> listOrders = [];
  Status loadData = Status.none;

  loadOrders() async {
    Map<String, dynamic> jsondatais =
        jsonDecode(StorageUtil.getString('customer'));
    var customer = Customer.fromJson(jsondatais);
    final response = await http.get(
        Uri.parse(
          BASE_URL +
              "orders?output_format=JSON&display=full&filter[id_customer]=${customer.id}",
        ),
        headers: kheaders);
    if (response.statusCode == 200) {
      var resp = json.decode(response.body);
      print(response.body);
      if (resp is Map<String, dynamic>) if (resp.containsKey('orders'))
        listOrders =
            List<Order>.from(resp["orders"].map((x) => Order.fromJson(x)));
      listOrders.reversed;
      //     Products.fromJson(resp).products;
    }
    notifyListeners();
  }

  loadProducts() async {
    Map<String, dynamic> jsondatais =
        jsonDecode(StorageUtil.getString('customer'));
    var customer = Customer.fromJson(jsondatais);
    final response = await http.get(
        Uri.parse(
          BASE_URL + "kbsellers/${customer.sellerId}?output_format=JSON",
        ),
        headers: kheaders);
    if (response.statusCode == 200) {
      var resp = json.decode(response.body);

      if (resp is Map<String, dynamic>) if (resp.containsKey('seller'))
        listMenu = List<Product>.from(resp["seller"]["kbsellerproducts"]
            .map((x) => Product.fromJson2(x)));
      //     Products.fromJson(resp).products;
    }
    notifyListeners();
  }
}
