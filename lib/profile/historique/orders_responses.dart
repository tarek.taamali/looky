class Orders {
  Orders({
    required this.orders,
  });

  List<Order> orders;

  factory Orders.fromJson(Map<String, dynamic> json) => Orders(
        orders: List<Order>.from(json["orders"].map((x) => Order.fromJson(x))),
      );
}

class Order {
  Order({
    required this.id,
    required this.dateAdd,
    required this.totalPaid,
    required this.payment,
  });

  int id;
  String dateAdd;
  String totalPaid;
  String payment;

  factory Order.fromJson(Map<String, dynamic> json) => Order(
        id: json["id"],
        dateAdd: json["date_add"],
        totalPaid: json["total_paid"],
        payment: json["payment"],
      );
}
