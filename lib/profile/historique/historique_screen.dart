import 'package:applooki/Utils/utils.dart';
import 'package:applooki/assets/images_links.dart';
import 'package:applooki/home/products_response.dart';
import 'package:applooki/models/customers.dart';
import 'package:applooki/profile/historique/orders_responses.dart';
import 'package:applooki/shared/top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:applooki/assets/fonts.dart';
import 'package:applooki/assets/colors.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../constant.dart';
import 'details_product/details_product_screen.dart';
import 'historique_view_model.dart';

final historyViewModel = ChangeNotifierProvider<HistoriqueViewModel>(
  (ref) => HistoriqueViewModel(),
);

class HistoriqueScreen extends StatefulWidget {
  final Customer customer;

  const HistoriqueScreen({Key? key, required this.customer}) : super(key: key);

  @override
  _HistoriqueScreenState createState() => _HistoriqueScreenState();
}

class _HistoriqueScreenState extends State<HistoriqueScreen> {
  late PageController _pageController;
  int indexPage = 0;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      final viewModel = context.read(historyViewModel);
      viewModel.loadProducts();
      viewModel.loadOrders();
    });
  }

  updateSwipe(int index) {
    setState(() {
      indexPage = index;
    });
  }

  swipe(int index) {
    _pageController.animateToPage(index,
        duration: Duration(milliseconds: 500), curve: Curves.ease);
    updateSwipe(index);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: primary_white,
      body: SafeArea(child: _buildBody(size)),
    );
  }

  Widget _buildBody(Size size) {
    return Container(
      height: size.height,
      padding: EdgeInsets.symmetric(
        horizontal: size.width * 0.075,
      ),
      child: Column(
        children: [
          TopBarWidget(
            title: "Historique",
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: size.width * 0.33,
                height: 40.0,
                decoration: BoxDecoration(
                  color: indexPage == 0 ? primary_blue : Colors.transparent,
                  borderRadius: BorderRadius.circular(12.0),
                ),
                child: TextButton(
                  onPressed: () {
                    swipe(0);
                  },
                  child: Text(
                    "Mes ventes",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 15.0,
                      fontFamily: fontSemiBold,
                      color: indexPage == 0 ? primary_white : primary_blue,
                    ),
                  ),
                ),
              ),
              Container(
                width: size.width * 0.33,
                height: 40.0,
                decoration: BoxDecoration(
                  color: indexPage == 1 ? primary_blue : Colors.transparent,
                  borderRadius: BorderRadius.circular(12.0),
                ),
                child: TextButton(
                  onPressed: () {
                    swipe(1);
                  },
                  child: Text(
                    "Mes achats",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 15.0,
                      fontFamily: fontSemiBold,
                      color: indexPage == 1 ? primary_white : primary_blue,
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: size.height * 0.03,
          ),
          Expanded(
            child: PageView(
                controller: _pageController,
                onPageChanged: updateSwipe,
                children: [
                  Container(
                    width: size.width * 0.75,
                    child: Consumer(builder: (context, watch, child) {
                      final _viewModel = watch(historyViewModel);
                      return ListView(
                          children: _viewModel.listMenu
                              .map((e) => CardProduct(product: e))
                              .toList());
                    }),
                  ),
                  Container(
                    width: size.width * 0.75,
                    decoration: BoxDecoration(
                      color: Colors.white,
                    ),
                    child: Container(
                        child: Consumer(builder: (context, watch, child) {
                      final _viewModel = watch(historyViewModel);
                      return _viewModel.listOrders.length != 0
                          ? Expanded(
                              child: ListView(
                                  children: _viewModel.listOrders
                                      .map((e) => CardOrder(order: e))
                                      .toList()),
                            )
                          : Column(
                              children: [
                                Image.asset(panierVide),
                                SizedBox(
                                  height: size.height * 0.03,
                                ),
                                Text(
                                  'Aucune Achat',
                                  style: TextStyle(
                                    fontSize: 18.0,
                                    fontFamily: fontSemiBold,
                                    color: primary_black,
                                  ),
                                ),
                                SizedBox(
                                  height: size.height * 0.015,
                                ),
                                SizedBox(
                                  width: size.width * 0.75,
                                  child: Text(
                                    'Vous n\'avez encore aucun Achat pour le moment',
                                    style: TextStyle(
                                      fontSize: 15.0,
                                      fontFamily: fontRegular,
                                      color: primary_black,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ],
                            );
                    })),
                  ),
                ]),
          ),
        ],
      ),
    );
  }
}

class CardOrder extends StatelessWidget {
  final Order order;
  const CardOrder({
    Key? key,
    required this.order,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 99.0,
      margin: EdgeInsets.only(bottom: 10),
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      decoration: BoxDecoration(
        color: primary_white,
        borderRadius: BorderRadius.circular(15.0),
        border: Border(
          right: BorderSide(color: primary_blue.withOpacity(0.59), width: 1.5),
          left: BorderSide(color: primary_blue.withOpacity(0.59), width: 1.5),
          bottom: BorderSide(color: primary_blue.withOpacity(0.59), width: 1.5),
          top: BorderSide(color: primary_blue.withOpacity(0.59), width: 1.5),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Commande N° ' + order.id.toString(),
                  style: TextStyle(
                    fontSize: 14.0,
                    fontFamily: fontSemiBold,
                    color: primary_black,
                  ),
                ),
                Text(
                  '${double.parse(order.totalPaid).toStringAsFixed(2)}€',
                  style: TextStyle(
                    fontSize: 15.0,
                    fontFamily: fontSemiBold,
                    color: primary_blue,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              child: Text(
                'Mode de paiment : ' + order.payment.toString(),
                style: TextStyle(
                  fontSize: 14.0,
                  fontFamily: fontRegular,
                  color: primary_black,
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              child: Text(
                order.dateAdd.toString(),
                style: TextStyle(
                  fontSize: 12.0,
                  fontFamily: fontRegular,
                  color: primary_black,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class CardProduct extends StatelessWidget {
  final Product product;
  const CardProduct({
    Key? key,
    required this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 99.0,
      margin: EdgeInsets.only(bottom: 10),
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      decoration: BoxDecoration(
        color: primary_white,
        borderRadius: BorderRadius.circular(15.0),
        border: Border(
          right: BorderSide(color: primary_blue.withOpacity(0.59), width: 1.5),
          left: BorderSide(color: primary_blue.withOpacity(0.59), width: 1.5),
          bottom: BorderSide(color: primary_blue.withOpacity(0.59), width: 1.5),
          top: BorderSide(color: primary_blue.withOpacity(0.59), width: 1.5),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(7),
              image: DecorationImage(
                image: NetworkImage(
                  product.idDefaultImage,
                  headers: kheaders,
                ),
                fit: BoxFit.fill,
              ),
            ),
            width: 58,
            height: 65.0,
          ),
          Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: 150,
                        child: Text(
                          product.name,
                          style: TextStyle(
                            fontSize: 14.0,
                            fontFamily: fontSemiBold,
                            color: primary_black,
                          ),
                        ),
                      ),
                      Text(
                        '  ${convertToStringPrice(product.reducedPrice)}',
                        style: TextStyle(
                          fontSize: 15.0,
                          fontFamily: fontSemiBold,
                          color: primary_blue,
                        ),
                      ),
                    ],
                  ),
                ),
                /* Text(
                  'Publier par vente online',
                  style: TextStyle(
                    fontSize: 9.0,
                    fontFamily: fontRegular,
                  ),
                ),*/
              ],
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => DetailsProduitScreen(
                    idProduct: product.id,
                  ),
                ),
              );
            },
            child: Container(
              width: 35.0,
              height: 35.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(9),
                color: primary_blue,
              ),
              child: Icon(
                Icons.arrow_forward_ios,
                color: primary_white,
              ),
            ),
          )
        ],
      ),
    );
  }
}
