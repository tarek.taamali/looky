import 'package:applooki/assets/colors.dart';
import 'package:applooki/assets/fonts.dart';
import 'package:flutter/material.dart';

class BadgeProduct extends StatelessWidget {
  final String icon;
  final String title;
  final String content;
  final double widthBadge;
  final double heightBadge;
  const BadgeProduct({
    Key? key,
    required this.icon,
    required this.title,
    required this.content,
    required this.widthBadge,
    this.heightBadge = 50,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widthBadge,
      height: heightBadge,
      child: Stack(
        children: [
          Positioned(
            left: 16.0,
            child: Container(
              width: widthBadge - 20,
              height: heightBadge,
              decoration: BoxDecoration(
                color: back_input_search,
                borderRadius: BorderRadius.circular(12),
              ),
              padding: EdgeInsets.all(10.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: title.isNotEmpty
                    ? [
                        Text(
                          title,
                          style: TextStyle(
                            fontSize: 11.0,
                            fontFamily: fontRegular,
                            color: background_blue,
                          ),
                        ),
                        Text(
                          content,
                          style: TextStyle(
                            fontSize: 12.0,
                            fontFamily: fontRegular,
                            color: txt_dark,
                          ),
                        ),
                      ]
                    : [
                        Text(
                          content,
                          style: TextStyle(
                            fontSize: 12.0,
                            fontFamily: fontRegular,
                            color: txt_dark,
                          ),
                        ),
                      ],
              ),
            ),
          ),
          Container(
            height: 32.0,
            width: 32.0,
            margin: EdgeInsets.only(top: 9),
            child: Image.asset(icon),
          ),
        ],
      ),
    );
  }
}

class SmallBadgeProduct extends StatelessWidget {
  final String icon;
  final String title;
  final String content;
  final double widthBadge;
  final double heightBadge;
  const SmallBadgeProduct({
    Key? key,
    required this.icon,
    required this.title,
    required this.content,
    required this.widthBadge,
    this.heightBadge = 25,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widthBadge,
      height: heightBadge,
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(left: 10),
            width: widthBadge - 10,
            decoration: BoxDecoration(
              color: back_input_search,
              borderRadius: BorderRadius.circular(12),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: title.isNotEmpty
                  ? [
                      Text(
                        title,
                        style: TextStyle(
                          fontSize: 7.0,
                          fontFamily: fontRegular,
                          color: background_blue,
                        ),
                      ),
                      Text(
                        content,
                        style: TextStyle(
                          fontSize: 7.0,
                          fontFamily: fontRegular,
                          color: txt_dark,
                        ),
                      ),
                    ]
                  : [
                      Text(
                        content,
                        style: TextStyle(
                          fontSize: 7.0,
                          fontFamily: fontRegular,
                          color: txt_dark,
                        ),
                      ),
                    ],
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              height: 17.0,
              width: 17.0,
              child: Image.asset(icon),
            ),
          )
        ],
      ),
    );
  }
}
