import 'dart:convert';
import 'package:applooki/constant.dart';
import 'package:applooki/home/products_response.dart';
import 'package:applooki/sellers/category.dart';
import 'package:applooki/sellers/seller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class DetailsProduitViewModel extends ChangeNotifier {
  final url = BASE_URL + "kbsellers/";
  Product? product;
  bool notload = false;
  Seller seller = Seller(title: "", phone: "", businessEmail: "");
  Category category = Category(name: "");
  var idx;
  var date;
  List<dynamic> imageUrls = [];

  getProductById(int id) async {
    String path = BASE_URL + "products/" + "$id?output_format=JSON";
    var response = await http.get(Uri.parse(path), headers: kheaders);
    if (response.statusCode == 200) {
      var result = json.decode(response.body);
      product = Product.fromJson(result["product"]);
      getSeller(product!.idSeller);
      // viewModel.getcategory(widget.product.idCategoryDefault.toString());
      notifyListeners();
    }
  }

  getSeller(String id) async {
    if (id.isEmpty) return;
    String path = url + "$id?output_format=JSON";
    var response = await http.get(Uri.parse(path), headers: kheaders);
    if (response.statusCode == 200) {
      var result = json.decode(response.body);
      seller = SellerResponse.fromJson(result).seller;

      notifyListeners();
    }
  }

  getcategory(String id) async {
    String path = BASE_URL +
        "categories" +
        "?filter[id]=$id&output_format=JSON&display=full";
    var response = await http.get(Uri.parse(path), headers: kheaders);
    if (response.statusCode == 200) {
      var result = json.decode(response.body);
      if (result is Map<String, dynamic>)
        category = CategoryResponse.fromJson(result).category;
      notifyListeners();
    }
  }
}
