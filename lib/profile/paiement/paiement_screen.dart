import 'package:applooki/assets/images_links.dart';
import 'package:applooki/shared/top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:applooki/assets/fonts.dart';
import 'package:applooki/assets/colors.dart';
import 'package:flutter_svg/svg.dart';

class PaiementScreen extends StatefulWidget {
  @override
  _PaiementScreenState createState() => _PaiementScreenState();
}

class _PaiementScreenState extends State<PaiementScreen> {
  int val = 1;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: primary_white,
        bottomNavigationBar: BottomAppBar(
          elevation: 0.0,
          child: Container(
            width: size.width,
            height: 48.0,
            decoration: BoxDecoration(
              color: primary_blue,
              borderRadius: BorderRadius.circular(12),
            ),
            margin: EdgeInsets.only(
              bottom: 20.0,
              left: MediaQuery.of(context).size.width * 0.075,
              right: MediaQuery.of(context).size.width * 0.075,
            ),
            child: TextButton(
                onPressed: () {
                  /* Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => AddAdresseScreen()));
                    */
                },
                child: Text(
                  "Ajouter une carte",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 16.0,
                    fontFamily: fontBold,
                    color: primary_white,
                  ),
                )),
          ),
        ),
        body: SafeArea(
          child: Container(
            width: size.width,
            padding: EdgeInsets.symmetric(horizontal: size.width * 0.075),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                TopBarWidget(
                  title: 'Cartes de paiment',
                ),
                Expanded(
                    child: Column(
                  children: [
                    Container(
                      width: size.width * 0.5,
                      decoration: BoxDecoration(
                        color: Colors.white,
                      ),
                      child: SvgPicture.asset("images/ic_looki.svg"),
                    ),
                    SizedBox(
                      height: size.height * 0.1,
                    ),
                    Container(
                      width: size.width * 0.75,
                      decoration: BoxDecoration(
                        color: Colors.white,
                      ),
                      child: Column(
                        children: [
                          Image.asset(carteVide),
                          SizedBox(
                            height: size.height * 0.03,
                          ),
                          Text(
                            'Aucune Cartes',
                            style: TextStyle(
                              fontSize: 18.0,
                              fontFamily: fontSemiBold,
                              color: primary_black,
                            ),
                          ),
                          SizedBox(
                            height: size.height * 0.015,
                          ),
                          SizedBox(
                            width: size.width * 0.75,
                            child: Text(
                              'Vous n\'avez pas encore ajouter votre carte de paiement',
                              style: TextStyle(
                                fontSize: 15.0,
                                fontFamily: fontRegular,
                                color: primary_black,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ))
              ],
            ),
          ),
        ));
  }
}
