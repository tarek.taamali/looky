import 'package:applooki/Utils/strings.dart';
import 'package:applooki/assets/custom_text_field.dart';
import 'package:applooki/shared/top_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:applooki/assets/fonts.dart';
import 'package:applooki/assets/colors.dart';

class AddCardScreen extends StatefulWidget {
  @override
  _AdresseScreenState createState() => _AdresseScreenState();
}

class _AdresseScreenState extends State<AddCardScreen> {
  final _formKey = GlobalKey<FormState>();
  var aliasController = TextEditingController();
  var aliasFocusNode = FocusNode();
  var adresse1Controller = TextEditingController();
  var adresse1FocusNode = FocusNode();
  var adresse2Controller = TextEditingController();
  var adresse2FocusNode = FocusNode();
  var paysController = TextEditingController();
  var paysFocusNode = FocusNode();
  var codePosteController = TextEditingController();
  var codePostFocusNode = FocusNode();
  var telController = TextEditingController();
  var telFocusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    //final viewModel = context.read(addresseViewModel);

    return Scaffold(
      backgroundColor: primary_white,
      bottomNavigationBar: BottomAppBar(
        child: FlatButton(
            onPressed: () {
              if (_formKey.currentState!.validate()) {
                /*  viewModel.createAdresse(
                  context: context,
                  adress1: adresse1Controller.text,
                  adress2: adresse2Controller.text,
                  country: paysController.text,
                  zipCode: codePosteController.text,
                  mobile: telController.text,
                  alias: aliasController.text,
                );*/
              }
            },
            child: Container(
              margin: EdgeInsets.only(bottom: 20.0),
              padding: EdgeInsets.only(
                top: 12.0,
              ),
              constraints: BoxConstraints(
                maxWidth: size.width * 0.85,
              ),
              width: size.width * 0.85,
              height: 48.0,
              decoration: BoxDecoration(
                color: primary_blue,
                borderRadius: BorderRadius.circular(12),
              ),
              child: Text(
                "Ajouter une Carte",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 16.0,
                  fontFamily: fontBold,
                  color: primary_white,
                ),
              ),
            )),
      ),
      body: SafeArea(
        child: Container(
          width: size.width,
          padding: EdgeInsets.symmetric(horizontal: size.width * 0.075),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TopBarWidget(
                title: 'Nouvelle carte de paiment',
              ),
              Expanded(
                  child: Form(
                key: _formKey,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      CustomFormField(
                        prefixIcon: Icon(
                          Icons.link,
                          color: background_blue,
                          size: 15.0,
                        ),
                        width: size.width * 0.85,
                        keyboardType: TextInputType.text,
                        hintText: "Nom sur la carte",
                        validator: (value) {
                          if (value!.isEmpty) {
                            return requiredFieldLabel;
                          }
                          return null;
                        },
                        controller: aliasController,
                        focusNode: aliasFocusNode,
                        onFieldSubmitted: (term) {
                          aliasFocusNode.unfocus();
                          FocusScope.of(context)
                              .requestFocus(adresse2FocusNode);
                        },
                      ),
                      SizedBox(height: size.height * 0.02),
                      CustomFormField(
                        prefixIcon: Icon(
                          Icons.pin_drop_outlined,
                          color: background_blue,
                          size: 15.0,
                        ),
                        width: size.width * 0.85,
                        keyboardType: TextInputType.text,
                        hintText: "Numéro du carte",
                        validator: (value) {
                          if (value!.isEmpty) {
                            return requiredFieldLabel;
                          }
                          return null;
                        },
                        controller: adresse1Controller,
                        focusNode: adresse1FocusNode,
                        onFieldSubmitted: (term) {
                          adresse1FocusNode.unfocus();
                          FocusScope.of(context)
                              .requestFocus(adresse2FocusNode);
                        },
                      ),
                      SizedBox(height: size.height * 0.02),
                      CustomFormField(
                        prefixIcon: Icon(
                          Icons.pin_drop_outlined,
                          color: background_blue,
                          size: 15.0,
                        ),
                        width: size.width * 0.85,
                        keyboardType: TextInputType.text,
                        hintText: "Date d’expiration",
                        validator: (value) {
                          if (value!.isEmpty) {
                            return requiredFieldLabel;
                          }
                          return null;
                        },
                        controller: adresse2Controller,
                        focusNode: adresse2FocusNode,
                        onFieldSubmitted: (term) {
                          adresse2FocusNode.unfocus();
                          FocusScope.of(context).requestFocus(paysFocusNode);
                        },
                      ),
                      SizedBox(height: size.height * 0.02),
                      CustomFormField(
                        prefixIcon: Icon(
                          Icons.flag,
                          color: background_blue,
                          size: 15.0,
                        ),
                        width: size.width * 0.85,
                        keyboardType: TextInputType.text,
                        hintText: "Code CCV",
                        validator: (value) {
                          if (value!.isEmpty) {
                            return requiredFieldLabel;
                          }
                          return null;
                        },
                        controller: paysController,
                        focusNode: paysFocusNode,
                        onFieldSubmitted: (term) {
                          paysFocusNode.unfocus();
                          FocusScope.of(context)
                              .requestFocus(codePostFocusNode);
                        },
                      ),
                      /*  SizedBox(height: size.height * 0.02),
                      CustomFormField(
                        prefixIcon: Icon(
                          Icons.pin_drop_outlined,
                          color: background_blue,
                          size: 15.0,
                        ),
                        width: size.width * 0.85,
                        keyboardType: TextInputType.number,
                        hintText: "Code postal",
                        validator: (value) {
                          if (value!.isEmpty) {
                            return requiredFieldLabel;
                          }
                          return null;
                        },
                        controller: codePosteController,
                        focusNode: codePostFocusNode,
                        onFieldSubmitted: (term) {
                          codePostFocusNode.unfocus();
                          FocusScope.of(context).requestFocus(telFocusNode);
                        },
                      ),
                      SizedBox(height: size.height * 0.02),
                      CustomFormField(
                        prefixIcon: Icon(
                          Icons.call,
                          color: background_blue,
                          size: 15.0,
                        ),
                        width: size.width * 0.85,
                        keyboardType: TextInputType.phone,
                        hintText: "Numéro du téléphone",
                        validator: (value) {
                          if (value!.isEmpty) {
                            return requiredFieldLabel;
                          }
                          return null;
                        },
                        controller: telController,
                        focusNode: telFocusNode,
                        onFieldSubmitted: (term) {
                          telFocusNode.unfocus();
                          if (_formKey.currentState!.validate()) {
                            /* viewModel.createAdresse(
                              context: context,
                              adress1: adresse1Controller.text,
                              adress2: adresse2Controller.text,
                              country: paysController.text,
                              zipCode: codePosteController.text,
                              mobile: telController.text,
                              alias: aliasController.text,
                            );*/
                          }
                        },
                      ),*/
                    ],
                  ),
                ),
              )),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildTextField(
      {required String label,
      required String hint,
      required TextEditingController controller,
      required FocusNode focusNode,
      required Function onSubmit,
      required bool autoFocus,
      Function(String)? onFieldSubmitted,
      required String? Function(String?) validator,
      TextInputType? textInputType}) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(label, style: TextStyle(fontWeight: FontWeight.w600)),
          SizedBox(
            height: 15,
          ),
          TextFormField(
            autofocus: autoFocus,
            controller: controller,
            keyboardType: textInputType,
            focusNode: focusNode,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0)),
                borderSide: BorderSide(
                  color: primary_blue,
                ),
              ),
              filled: true,
              hintStyle: TextStyle(fontSize: 15),
              hintText: hint,
              fillColor: Colors.white70,
            ),
            onFieldSubmitted: onFieldSubmitted,
            validator: validator,
          ),
        ],
      ),
    );
  }
}
