import 'package:applooki/Utils/local_storage.dart';
import 'package:applooki/assets/colors.dart';
import 'package:applooki/auth/sign_in_screen.dart';
import 'package:applooki/landing/landing_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';

import '../assets/images_links.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _PageviewAnimationState();
}

class _PageviewAnimationState extends State<SplashScreen> {
  PageController controller = PageController();
  static dynamic currentPageValue = 0.0;
  // list of pages
  List pageViewItem = [
    page(
      splash_screen1,
    ),
    page(
      splash_screen2,
    ),
    page(
      splash_screen3,
    ),
    page(
      splash_screen4,
    ),
  ];

  List<String> titles = [
    'Prenez une photo et postez',
    'Emballez et envoyez l’article',
    'Recevez votre argent',
    'Achetez,'
  ];
  List<String> body = [
    'Ajoutez des photos, détails et prix de l’article et HOP c’est en ligne',
    'Préparez la commande pour-que le livreur vienne la cherche',
    '24heures après livraison votre argent vous sera versé',
    'Faites vous plaisir et achetez ce qui vous plait a prix bas,'
  ];

  List<int> pagination = [1, 2, 3, 4];
  @override
  void initState() {
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.white, // navigation bar color
      statusBarColor: Colors.white, // status bar color
      systemNavigationBarIconBrightness: Brightness.dark,statusBarIconBrightness: Brightness.dark
    ));
    controller.addListener(() {
      setState(() {
        currentPageValue = controller.page;
      });
    });
  }

  int pageIndex = 0;


  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        actions: [
          TextButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (BuildContext context) {
                  return SignInScreen();
                }));
              },
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: const Text(
                  'Passer',
                  style: TextStyle(color: Color(0xff353535)),
                ),
              ))
        ],
      ),
      // PageView builder builds the page.
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SvgPicture.asset(ic_looki),
            SizedBox(
              height: size.height * 0.44,
              child: PageView.builder(
                  onPageChanged: (i) {
                    //  if (i.toString().split('.').last == '0') {
                    setState(() {
                      pageIndex = i;
                    });

                    // }
                  },
                  itemCount: pageViewItem.length,
                  scrollDirection: Axis.horizontal,
                  controller: controller,
                  itemBuilder: (context, position) {
                    // Transform using for animation
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 22),
                      child: Transform(
                        transform: Matrix4.identity()
                          ..rotateX(-(currentPageValue - position)*1.4),
                        child: pageViewItem[position],
                      ),
                    );
                  }),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 30),
              child: AnimatedSwitcher(
                duration: const Duration(milliseconds: 1000),
                child: titles
                    .map((e) => Text(
                          e,
                          style: TextStyle(
                              color: primary_blue,
                              fontSize: 18,
                              fontWeight: FontWeight.w500),
                          key: UniqueKey(),
                        ))
                    .toList()[pageIndex],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 35),
              child: AnimatedSwitcher(
                duration: const Duration(milliseconds: 1000),
                child: body
                    .map((e) => Text(
                          e,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Color(0xff707070),
                            fontSize: 15,
                            fontWeight: FontWeight.w500,
                          ),
                          key: UniqueKey(),
                        ))
                    .toList()[pageIndex],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20,),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: pagination
                    .map((e) => Pagination(
                          currentIndex: pageIndex + 1,
                          index: e,
                        ))
                    .toList(),
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.only(bottom: 20, right: 20, left: 20),
        child: InkWell(
          onTap: () {
            if (pageIndex != 3) {
              controller.nextPage(
                  duration: const Duration(milliseconds: 1000),
                  curve: Curves.fastOutSlowIn);
            } else {
              Navigator.push(context,
                  MaterialPageRoute(builder: (BuildContext context) {
                return SignInScreen();
              }));
            }
          },
          child: Container(
            height: 40,
            decoration: BoxDecoration(
                color: primary_blue,
                borderRadius: BorderRadius.all(Radius.circular(12))),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Text(
                  'Suivant',
                  style: TextStyle(color: Colors.white),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

// this widget makes the page
Widget page(String image) {
  return SvgPicture.asset(image);
}

class Pagination extends StatelessWidget {
  final int currentIndex;
  final int index;
  const Pagination({Key? key, required this.currentIndex, required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 4),
      child: Container(
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: currentIndex == index ? primary_blue : txt_grey),
        height: 10,
        width: 10,
      ),
    );
  }
}
