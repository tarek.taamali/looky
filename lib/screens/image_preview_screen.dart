import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:photo_view/photo_view.dart';

class ImagePreviewScreen extends StatefulWidget {
  final String url;
  final Map<String, String> headers;
  const ImagePreviewScreen({
    Key? key,
    required this.url,
    required this.headers,
  }) : super(key: key);

  @override
  State<ImagePreviewScreen> createState() => _ImagePreviewScreenState();
}

class _ImagePreviewScreenState extends State<ImagePreviewScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(milliseconds: 300),(){
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
          systemNavigationBarColor: Colors.white, // navigation bar color
          statusBarColor: Colors.black, // status bar color
          systemNavigationBarIconBrightness: Brightness.dark,statusBarIconBrightness: Brightness.light
      ));
    });

  }
@override
  void dispose(){
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.white, // navigation bar color
      statusBarColor: Colors.white, // status bar color
      systemNavigationBarIconBrightness: Brightness.dark,statusBarIconBrightness: Brightness.dark
  ));
    super.dispose();
    //...
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(backgroundColor: Colors.black,),
      body: PhotoView(
        imageProvider: Image.network(
          widget.url,
          headers: widget.headers,
        ).image,
      ),
    );
  }
}
